package com.marlabs.whpicker.database;

import java.util.HashSet;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.ConsolidationItem;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.utility.Validation;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	/*DATABASE NAME*/
	private static final String DATABASE_NAME = "ResourceManager";
	/*TABLES*/
	private static final String TABLE_TASKS = "Tasks";

	/*TABLE COMPONENTS*/
	private final String WHORDER = "whOrder";
	private final String DESTINATIONBIN = "destinationBin";
	private final String HANDLINGUNIT = "handlingUnit";
	private final String QUANTITY = "quantity";
	private final String WAREHOUSETASK = "wareHouseTask";
	private final String RESOURCE = "resource";
	private final String SLOTNO = "slotNo";
	private final String PICKHU = "pickHU";
	private final String ISVALIDTASK = "false";
	private final String CONSOLIDATIONGROUP = "consolidationGroup";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_TASKS_TABLE = "CREATE TABLE " + TABLE_TASKS + "("
				+ WHORDER + " TEXT,"
				+ DESTINATIONBIN + " TEXT," 
				+ QUANTITY + " TEXT," 
				+ WAREHOUSETASK + " TEXT," 
				+ RESOURCE + " TEXT," 
				+ SLOTNO + " TEXT," 
				+ PICKHU + " TEXT," 
				+ CONSOLIDATIONGROUP + " TEXT,"
				+ ISVALIDTASK + " TEXT" + ")";
		db.execSQL(CREATE_TASKS_TABLE);
		//		db.close();
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		try{
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
			onCreate(db);
			db.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void addTask(ConsolidationItem item) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(WHORDER, item.getOrderID()); 
		values.put(DESTINATIONBIN, item.getDestBin()); 
		values.put(QUANTITY, item.getQuantity()); 
		values.put(WAREHOUSETASK, item.getTaskID()); 
		values.put(RESOURCE, item.getResource()); 
		values.put(SLOTNO, item.getSlot()); 
		values.put(PICKHU, item.getTote()); 
		values.put(CONSOLIDATIONGROUP, item.getConsolidationGroup());
		values.put(ISVALIDTASK, item.isConfirmSuccessful());

		db.insert(TABLE_TASKS, null, values);

		db.close(); 
	}

	public Set<ConsolidationItem> getAllTasks(String resource) {
		Set<ConsolidationItem> taskList = new HashSet<ConsolidationItem>();

		String selectQuery = "SELECT  * FROM " + TABLE_TASKS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				ConsolidationItem item = new ConsolidationItem();
				item.setOrderID(cursor.getString(0)); 
				item.setDestBin(cursor.getString(1)); 
				item.setQuantity(Integer.parseInt(cursor.getString(2))); 
				item.setTaskID(cursor.getString(3)); 
				item.setResource(cursor.getString(4)); 
				item.setSlot(cursor.getString(5)); 
				item.setTote(cursor.getString(6)); 
				item.setConsolidationGroup(cursor.getString(7));
				if(!Validation.isNullString(cursor.getString(8))){
					if(cursor.getString(8).equalsIgnoreCase("false"))
						item.setConfirmSuccessful(false); 
					else
						item.setConfirmSuccessful(true);
				}
				if(item.getResource().equalsIgnoreCase(resource))
					taskList.add(item);
			} while (cursor.moveToNext());
		}
		db.close();
		return taskList;
	}

	public void deleteTask(Task task) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_TASKS, HANDLINGUNIT + " = ?", new String[] { task.getHandlingUnit() });
		db.close();
	}

	public void reCreateTable(){
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
		onCreate(db);
		db.close();
	}

	public SQLiteDatabase getDatabase() {
		SQLiteDatabase db = this.getWritableDatabase();
		return db;
	}
}