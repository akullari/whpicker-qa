package com.marlabs.whpicker.scanner;

import java.lang.reflect.Method;
import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;

import com.marlabs.whpickerqa.R;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.ScannerResults;

public class AsyncDataUpdate extends AsyncTask<ScanDataCollection, Void, String> {

	private Method functionName = null;
	private Object receiver = null;

	public AsyncDataUpdate(Context context, Method method) {
		functionName = method;
		receiver = context;
	}

	@Override
	protected String doInBackground(ScanDataCollection... params) {
		ScanDataCollection scanDataCollection = params[0];
		String statusStr = "";
		if (scanDataCollection != null
				&& scanDataCollection.getResult() == ScannerResults.SUCCESS) {

			ArrayList<ScanData> scanData = scanDataCollection.getScanData();

			for (ScanData data : scanData) {
				String barcodeDate = data.getData();
				statusStr = barcodeDate;
			}
		}
		return statusStr;
	}

	@Override
	protected void onPostExecute(String result) {
		
		super.onPostExecute(result);
		try {			
			this.functionName.invoke(receiver, result);
		} catch(Exception exception) {
			exception.printStackTrace();
		}

	}

	@Override
	protected void onPreExecute() {
	}

	@Override
	protected void onProgressUpdate(Void... values) {
	}
}