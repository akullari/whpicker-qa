package com.marlabs.whpicker.scanner;

import com.marlabs.whpickerqa.R;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerException;


public class ScannerUtils{

	private static ScannerUtils instance = new ScannerUtils();

	private ScannerUtils(){}

	public static ScannerUtils getInstance(){
		return instance;
	}

	public void releaseScanner(EMDKManager emdkManager) {
		if (emdkManager != null) {
			emdkManager.release();
//			emdkManager = null;
		}
	}

	public void disableScanner(Scanner scanner) {
		try {
			if (scanner != null) {
				scanner.disable();
				scanner = null;
			}
		} catch (ScannerException e) {
			e.printStackTrace();
		}
	}
}