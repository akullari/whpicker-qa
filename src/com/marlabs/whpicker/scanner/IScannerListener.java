///Om Sri Sai Ram
package com.marlabs.whpicker.scanner;

import com.marlabs.whpickerqa.R;


public interface IScannerListener 
{
	void ScanSuccessful(String scanValue, int sourceIndex);
	void ScanFailed();
}
