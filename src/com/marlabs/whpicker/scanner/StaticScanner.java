///Om Sri Sai Ram
package com.marlabs.whpicker.scanner;

import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.WHPickerApplication;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.ProfileManager;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.DeviceIdentifier;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.Scanner.DataListener;
import com.symbol.emdk.barcode.Scanner.StatusListener;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;
import com.symbol.emdk.barcode.StatusData.ScannerStates;

public class StaticScanner implements EMDKListener, StatusListener, DataListener{


	private static StaticScanner singleTonInstance = null;
	protected StaticScanner() 
	{
		// Exists only to defeat instantiation.
		initDevice();
	}
	public static StaticScanner getInstance() {
		if(singleTonInstance == null) {
			singleTonInstance = new StaticScanner();
		}
		return singleTonInstance;
	}


	public EMDKManager emdkMagr = null;
	public ProfileManager profileMagr = null;
	public BarcodeManager barcodeMagr=null;
	public Scanner scanner = null;
	public boolean isScanning = false;

	public static final String TAG="STATIC SCANNER";

	private void initDevice()
	{
		final Context ctx = WHPickerApplication.getAppContext();
		// The EMDKManager object will be created and returned in the callback.  
		EMDKResults results = EMDKManager.getEMDKManager(ctx,this);  

		// Check the return status of getEMDKManager  
		if (results.statusCode == EMDKResults.STATUS_CODE.SUCCESS) {  
			// EMDKManager object creation success  
			Log.i(TAG, "getEMDKManager => OK");  
		} else {  
			// EMDKManager object creation failed  
			Log.e(TAG, "getEMDKManager => NO");  
		}  
	}

	private IScannerListener listener;
	private int scanningIndex;
	public void scannerRead(IScannerListener listener, int sourceIndex)
	{
		this.listener=listener;
		this.scanningIndex=sourceIndex;
		if(emdkMagr==null)
		{
			System.out.println("EMDK NUll");
			return;
		}
		if(scanner==null)
		{
			initializeScanner();
		}
		
		isScanning=true;
		if(scanner.isReadPending())
			return;
		
			//Enable the scanner
			try
			{
				//Start an asynchronous scan. The method will not turn on the scanner. It will, however, put the scanner in a state which the scanner can be turned ON either by pressing a hardware trigger or can be turned ON automatically
				scanner.read();
			}
			catch(ScannerException e)
			{
//				if(e.getMessage().equalsIgnoreCase("Already scanning. Wait for current scanning to complete."))
//				{
//					scanFailedDue2LastScanNotCompletedRetry=true;
//				}

				Log.e(TAG, "Scanner enable error..." + e.getMessage());
			}
	}


	public void cancelRead()
	{
		if(scanner==null)
		{
			initializeScanner();
		}

		if(scanner!=null)
		{
			//Enable the scanner
			try
			{
				isScanning=false;
				scanner.cancelRead();
			}
			catch(ScannerException e)
			{
				Log.e(TAG, "Scanner enable error..." + e.getMessage());
			}

		}
	}

	//MArk EMDKListener methods
	@Override
	public void onClosed() 
	{
		//		if (emdkManager != null) {
		//			//emdkManager.release();
		//			new AsyncRelease(this, emdkManager).execute();
		//		}
	}

	@Override
	public void onOpened(EMDKManager emdkManager) 
	{
		emdkMagr=emdkManager;  
		initializeScanner();
		profileMagr = (ProfileManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.PROFILE);  
	}
	
	private void initializeScanner() {
		// get the Barcode manager object
		barcodeMagr = (BarcodeManager)emdkMagr.getInstance(FEATURE_TYPE.BARCODE);  

		//Get default scanner defined on the device
		scanner = barcodeMagr.getDevice(DeviceIdentifier.DEFAULT);
		scanner.addDataListener(singleTonInstance);
		scanner.addStatusListener(singleTonInstance);
		//HArd Trigger, When this mode is set, the user has to manually press the trigger on the device after issuing read call
		scanner.triggerType = TriggerType.HARD;

		//Enable the scanner
		try
		{
			scanner.enable();
		}
		catch(ScannerException e)
		{
			Log.e(TAG, "Scanner enable error..." + e.getMessage());
		}
	}
	// End of EMDK Listener

	// MArk StatusListener
	@Override
	public void onStatus(StatusData statusData) 
	{
		//Async Call...
		new AsyncStatusUpdate().execute(statusData); 
		//Sync Call...
//		new AsyncStatusUpdate().doInBackground(statusData);
	}

	//mark DataListener class methods...
	@Override
	public void onData(ScanDataCollection scanDataCollection) 
	{
		new AsyncDataUpdate().execute(scanDataCollection);
		isScanning=false;
//		{ //Sync call...
//		String scanValue=new AsyncDataUpdate().doInBackground(scanDataCollection);
//		listener.ScanSuccessful(scanValue, scanningIndex);
//		}
	}

	private String ExtractScanStatus(StatusData statusData)
	{
		String statusStr = "";  
		// Get the current state of scanner in background  
//		StatusData statusData = params[0];  
		ScannerStates state = statusData.getState();  
		// Different states of Scanner  
		switch (state) {  
		// Scanner is IDLE  
		case IDLE:  
			statusStr = "The scanner enabled and its idle";
            if(isScanning)
            {
              	 try
              	 {
              	 scanner.read();
//              	 isScanning=false;
              	 }
              	 catch(Exception e)
              	 {}
               }
   			break;  
			// Scanner is SCANNING  
		case SCANNING:  
			statusStr = "Scanning..";  
			break;  
			// Scanner is waiting for trigger press  
		case WAITING:  
			statusStr = "Waiting for trigger press..";  
			break;  
			// Scanner is not enabled  
		case DISABLED:  
			statusStr = "Scanner is not enabled";  
			break;  
		default:  
			break;  
		}  

		// Return result to populate on UI thread  
		return statusStr;  
	}
	
	private String ExtractScanString(ScanDataCollection scanDataCollection)
	{
		String scannedString = "";  

		try {  


			if (scanDataCollection != null  
					&& scanDataCollection.getResult() == ScannerResults.SUCCESS) {  

				ArrayList<ScanData> scanData = scanDataCollection.getScanData();  

				// Iterate through scanned data and prepare the statusStr  
				for (ScanData data : scanData) {  
					// Get the scanned data  
					scannedString = data.getData();  
//					// Get the type of label being scanned  
//					LabelType labelType = data.getLabelType();  
//					// Concatenate barcode data and label type  
//					statusStr = barcodeDate + " " + labelType;  
				}  
			}  

			//		         } catch (ScannerException e) {  
		} catch (Exception e) {  
			// TODO Auto-generated catch block  
			e.printStackTrace();  
		}  

		// Return result to populate on UI thread  
		return scannedString;  
	}

	// Update the scan data on UI  
	int dataLength = 0;  

	// AsyncTask that configures the scanned data on background  
	// thread and updated the result on UI thread with scanned data and type of  
	// label  
	private class AsyncDataUpdate extends  
	AsyncTask<ScanDataCollection, Void, String> {  

		@Override  
		protected String doInBackground(ScanDataCollection... params) 
		{  
			return ExtractScanString(params[0]);
		}


		@Override  
		protected void onPostExecute(String result) 
		{  
			// Update the dataView EditText on UI thread with barcode data and  
			// its label type  
			if (dataLength++ > 50) {  
				// Clear the cache after 50 scans  
				//		             dataView.getText().clear();  
				dataLength = 0;  
			}  
			//		         dataView.append(result + "\n");  
			Log.i(TAG, result);
			Toast.makeText(WHPickerApplication.getAppContext(), result, Toast.LENGTH_SHORT).show();
			listener.ScanSuccessful(result, scanningIndex);
		}  

		@Override  
		protected void onPreExecute() {  
		}  

		@Override  
		protected void onProgressUpdate(Void... values) {  
		}  

	}

	// AsyncTask that configures the current state of scanner on background  
	// thread and updates the result on UI thread  
	private class AsyncStatusUpdate extends AsyncTask<StatusData, Void, String> {  

		@Override  
		protected String doInBackground(StatusData... params) {  
			return ExtractScanStatus(params[0]);
		}  

		@Override  
		protected void onPostExecute(String result) {  
			Log.i(TAG, result);
		}  

		@Override  
		protected void onPreExecute() {  
		}  

		@Override  
		protected void onProgressUpdate(Void... values) {  
		}  
	}  
}
