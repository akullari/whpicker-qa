package com.marlabs.whpicker.inbound;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.KeyPadReaderScreen;
import com.marlabs.whpicker.screens.NumericReaderScreen;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Utility;

public class ScanHUScreen extends Activity implements IScannerListener{

	private final int MANUAL_REQUEST_CODE = 1;
	private final int SCANNER_REQUEST_CODE = 2;
	private EditText mEditText;
	private int parentID = 0;
	private HomeMenuItem homeMenuItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scanhu);

		CustomActionBar actionBar = new CustomActionBar(getActionBar(), this, "Queues");
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		scanData();

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			if(bundle.get(AppConstants.SELECTED_MENU) != null){
				HomeMenuItem homeMenuItem = (HomeMenuItem) bundle.get(AppConstants.SELECTED_MENU);
				actionBar.changeHeader(homeMenuItem.getItemName());
				parentID = Integer.parseInt(homeMenuItem.getItemId());
				this.homeMenuItem = homeMenuItem;
			}
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		mEditText = (EditText) findViewById(R.id.scanhu_edittext_code);
		mEditText.setKeyListener(null);
		TextView textView = (TextView) findViewById(R.id.scanhu_textview_manual);
		TextView textViewLabel = (TextView) findViewById(R.id.scanhu_textview_title);
		Button button = (Button) findViewById(R.id.scanhu_button_scan);
		textView.setAnimation(Utility.getInstance().blinkAnimation());

		switch(parentID){
		case 111 : 
			textViewLabel.setText("Delivery"); 
			button.setText("Scan Delivery Code");
			textView.setText("Enter Delivery code manually");
			break;

		case 112 : 
			textViewLabel.setText("PO Number"); 
			button.setText("Scan PO Number");
			textView.setText("Enter PO Number manually");
			break;

		case 113 : 
			textViewLabel.setText("Scan TU"); 
			button.setText("Scan TU Code");
			textView.setText("Enter TU code manually");
			break;

		case 131 : 
			textViewLabel.setText("Enter H.U Code"); 
			button.setText("Scan HU Code");
			textView.setText("Enter HU code manually");
			break;

		case 132 : 
			textViewLabel.setText("Enter W.O Code"); 
			button.setText("Scan WO Code");
			textView.setText("Enter WO code manually");
			break;
		}

		textView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent;
				if(parentID == 112){
					intent = new Intent(ScanHUScreen.this, NumericReaderScreen.class);
				}
				else{
					intent = new Intent(ScanHUScreen.this, KeyPadReaderScreen.class);
				}
				startActivityForResult(intent, MANUAL_REQUEST_CODE);
			}
		});

		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//Utility.getInstance().launchScannerForResult(ScanHUScreen.this, SCANNER_REQUEST_CODE);
			}
		});
	}
	
	private void scanData(){
		Toast.makeText(getApplicationContext(), "Initalizing Scanner", Toast.LENGTH_SHORT).show();
//		try {
//			StaticScanner.getInstance().scannerRead(ScanHUScreen.this, 
//					ScanHUScreen.class.getMethod("onBarCodeFormatted", String.class));
//		} catch (NoSuchMethodException e) {
//			e.printStackTrace();
//		}
		StaticScanner.getInstance().scannerRead(this,1);
	}
	
	public void onBarCodeFormatted(String result){
		//Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SCANNER_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				mEditText.setText(data.getStringExtra("SCAN_RESULT"));
				startChildActivity(data.getStringExtra("SCAN_RESULT"));
			} 
			else if (resultCode == RESULT_CANCELED) {
			}
		}
		else if (requestCode == MANUAL_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				mEditText.setText(data.getStringExtra("INPUT_RESULT"));
				startChildActivity(data.getStringExtra("INPUT_RESULT"));
			} 
			else if (resultCode == RESULT_CANCELED) {
			}
		}
	}

	private void startChildActivity(String code){
		Intent intent = null;

		switch(parentID){
		case 111 : 
			intent = new Intent(ScanHUScreen.this, UnloadingScreen.class); 
			break;
		}

		if(intent != null){
			intent.putExtra(AppConstants.SELECTED_MENU, homeMenuItem);
			startActivity(intent);
			finish();
		}
	}

	//Handing Back Pressed
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		// TODO Auto-generated method stub
		mEditText.setText(scanValue);
		
		Toast.makeText(getApplicationContext(), scanValue, Toast.LENGTH_SHORT).show();
		scanData();
		
	}

	@Override
	public void ScanFailed() {
		// TODO Auto-generated method stub
		
	}
}
	