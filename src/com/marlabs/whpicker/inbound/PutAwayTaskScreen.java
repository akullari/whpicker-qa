package com.marlabs.whpicker.inbound;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.inbound.services.InboundSingleton;
import com.marlabs.whpicker.models.ConfirmResponse;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.networking.ConnectionDetector;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.networking.VolleyRequestHandler;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.KeyPadReaderScreen;
import com.marlabs.whpicker.screens.NumericReaderScreen;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;

public class PutAwayTaskScreen extends Activity implements IScannerListener  {

	private CustomButton mButton1, mButton2, mButton3, mButton4, mButton5;
	private CustomButton mCurrentFocusedButton, mNextFocusedButton;
	private Drawable mButtonDrawable;
	private Button mButtonScan;
	private TextView mTextViewManual;
	private CustomActionBar actionBar;
	private boolean isHWUT = false;
	private int mTasksCount = 0, index = 0;
	private LinearLayout linearLayoutParent;
	private List<Task> mInboundTasks;
	private Task  mCurrentTask;
	private Order mParentOrder;
	private Animation animation1, animation2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom_validations);

		animation1 = AnimationUtils.loadAnimation(this, R.anim.fliptomiddle);
		animation2 = AnimationUtils.loadAnimation(this, R.anim.flipfrommiddle);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		StaticScanner.getInstance().scannerRead(this, 1);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			mParentOrder = (Order) bundle.getSerializable(AppConstants.INBOUND_ORDER);
			actionBar.changeHeader(mParentOrder.getWHOrder());
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		StaticScanner.getInstance().scannerRead(this, 1);
		callVolleyRequest(URLFactory.getPutawayTasks_URL(this, mParentOrder.getWHOrder()));
	}

	private void callVolleyRequest(String url) {
		if(ConnectionDetector.getInstance(this).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(this).callVolleyJSONRequest(this, Method.GET, url, null, Util.getBasicHANAHeaders(this),
						PutAwayTaskScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(PutAwayTaskScreen.this, getString(R.string.no_internet_connectivity));
		}
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			System.out.println("------ * response * -----" + jsonObject.toString());
			mInboundTasks = Parser.parseInboundTasksResult(jsonObject);
			if(mInboundTasks.size() == 0){
				Utility.getInstance().showAlertDialog(this, getString(R.string.no_tasks_in_selected_order));
				return;
			}
			switchViews();
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	private void switchViews(){
		mButton1 = (CustomButton) findViewById(R.id.validation_button1);
		mButton2 = (CustomButton) findViewById(R.id.validation_button2);
		mButton3 = (CustomButton) findViewById(R.id.validation_button3);
		mButton4 = (CustomButton) findViewById(R.id.validation_button4);
		mButton5 = (CustomButton) findViewById(R.id.validation_button5);
		linearLayoutParent = (LinearLayout) findViewById(R.id.linearlayout_validation);
		mTextViewManual = (TextView) findViewById(R.id.bottomlayout_textview_manual);
		mButtonScan = (Button) findViewById(R.id.bottomlayout_button_scan);

		animation1.setAnimationListener(animationListener);
		animation2.setAnimationListener(animationListener);

		mButton1.setOnClickListener(validationButonClickLisener);
		mButton2.setOnClickListener(validationButonClickLisener);
		mButton3.setOnClickListener(validationButonClickLisener);
		mButton4.setOnClickListener(validationButonClickLisener);
		mButton5.setOnClickListener(validationButonClickLisener);

		mTextViewManual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mCurrentFocusedButton == mButton3){
					Intent intent  = new Intent(PutAwayTaskScreen.this, NumericReaderScreen.class);
					intent.putExtra(AppConstants.NUMERIC_KEYPAD_INPUT, mCurrentTask.getTotalQuantity());
					startActivityForResult(intent, 1);
				}
				else{
					String input;
					if(mCurrentFocusedButton == mButton2 && !isHWUT){
						input = mCurrentTask.getBatch();
					}
					else{
						input = "";
					}
					Intent intent  = new Intent(PutAwayTaskScreen.this, KeyPadReaderScreen.class);
					intent.putExtra(AppConstants.ALPHA_KEYPAD_TITLE, mTextViewManual.getText().toString());
					intent.putExtra(AppConstants.ALPHA_KEYPAD_BATCHINPUT, input);
					startActivityForResult(intent, 1);
				}
			}
		});

		mButtonScan.setEnabled(false);
		mButtonScan.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				callConfirmTaskService();
			}
		});

		setDataTovalidation(mTasksCount);
	}

	OnClickListener validationButonClickLisener = new OnClickListener() {
		@SuppressLint("NewApi")
		@Override
		public void onClick(View view) {
			CustomButton button = (CustomButton) view;
			if(mCurrentFocusedButton != null){
				mCurrentFocusedButton.setBackground(mButtonDrawable);
				mCurrentFocusedButton.clearAnimation();
				mCurrentFocusedButton.setAsNotVerified();
			}
			button.setAsNotVerified();
			mCurrentFocusedButton = null;
			mNextFocusedButton = button;
			changeValidationCustomButtonFocus();
			index--;
		}
	};

	AnimationListener animationListener = new AnimationListener() {
		@Override
		public void onAnimationStart(Animation animation) {}

		@Override
		public void onAnimationRepeat(Animation animation) {}

		@Override
		public void onAnimationEnd(Animation animation) {
			if (animation==animation1) {
				(linearLayoutParent).clearAnimation();
				(linearLayoutParent).setAnimation(animation2);
				(linearLayoutParent).startAnimation(animation2);
			} 
		}
	};

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
			if (resultCode== RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				onManualDialogClosed(result);
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.manual_entry_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void onManualDialogClosed(String result){
		if(mCurrentFocusedButton == mButton2 && !isHWUT)
			switchButtons();
		else{
			if(mCurrentFocusedButton.getTag().toString().equalsIgnoreCase(result))
				switchButtons();
			else
				mCurrentFocusedButton.setAsWrongVerified();
		}
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	@SuppressLint("NewApi")
	private void changeValidationCustomButtonFocus(){
		if(mCurrentFocusedButton != null){
			mCurrentFocusedButton.setBackground(mButtonDrawable);
			mCurrentFocusedButton.setAsVerified();
			mCurrentFocusedButton.clearAnimation();
		}

		if(mNextFocusedButton != null){
			mButtonDrawable = mNextFocusedButton.getBackground();
			mNextFocusedButton.setBackgroundResource(R.drawable.header);
			mNextFocusedButton.startAnimation(Utility.getInstance().blinkAnimation());
			mCurrentFocusedButton = mNextFocusedButton;
			mCurrentFocusedButton.removeVerificationForBlink();
			mTextViewManual.setText("Enter "+ mCurrentFocusedButton.getTitle() +" Manually");
		}
		else{
			mCurrentFocusedButton = null;
		}
	}

	private void switchButtons() {
		index = index+1;
		if(index < mValidationButtonsList.size()){
			mNextFocusedButton = mValidationButtonsList.get(index);
			mValidationButtonsList.get(index).setEnabled(true);
		}
		else{
			mNextFocusedButton = null;
			callConfirmTaskService();
		}
		changeValidationCustomButtonFocus();
	}

	private void callConfirmTaskService(){
		mButtonScan.setText("Confirm Task");
		mButtonScan.setBackgroundColor(getResources().getColor(R.color.green));
		mButtonScan.setEnabled(true);
		try {
			InboundSingleton.getInstance().closeTask(this,PutAwayTaskScreen.class.getMethod("onTaskConfirmed", 
					ConfirmResponse.class), mCurrentTask);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public void onTaskConfirmed(ConfirmResponse confirmResponse){
		int respCode = confirmResponse.getResponseCode();
		if(respCode >= 200 && respCode < 300){
			mTasksCount++;
			if(mTasksCount < mInboundTasks.size()){
				setDataTovalidation(mTasksCount);
			}
			else{
				Utility.getInstance().showAlertDialog(this, "Task Confirmed Successfully.!");
			}
		}
		else{
			Util.showMessageDialog(this, confirmResponse.getResponseCode() +" : " + confirmResponse.getResponseLine());
		}
	}

	List<CustomButton> mValidationButtonsList = new ArrayList<CustomButton>();
	private void setDataTovalidation(int taskNumber) {

		actionBar.changeHeader("Task "+ (taskNumber+1) +" of " + mInboundTasks.size());
		Task inboundTask = mInboundTasks.get(taskNumber);
		mCurrentTask = inboundTask;

		if(taskNumber != 0){
			linearLayoutParent.clearAnimation();
			linearLayoutParent.setAnimation(animation1);
			linearLayoutParent.startAnimation(animation1);
		}

		mButton1.setAsVerified();
		mButton2.setAsNotVerified();
		mButton3.setAsNotVerified();
		mButton4.setAsNotVerified();
		mButton5.setAsNotVerified();

		mButton1.setEnabled(true);
		mButton2.setEnabled(false);
		mButton3.setEnabled(false);
		mButton4.setEnabled(false);
		mButton5.setEnabled(false);

		mButton1.setTitleAndText("Source Bin", inboundTask.getSourceBin());
		mButton1.setTag(inboundTask.getSourceBin());
		if(!inboundTask.getSourceBin().isEmpty())
			mValidationButtonsList.add(mButton1);

		if(inboundTask.getHandlingUnitWarehouseTask().equalsIgnoreCase("X")){
			isHWUT = true;

			mButton2.setTitleAndText("HU", Util.removeLeadingZeros(inboundTask.getHandlingUnit()));
			mButton2.setTag(inboundTask.getHandlingUnit());
			if(!inboundTask.getHandlingUnit().isEmpty())
				mValidationButtonsList.add(mButton2);

			mButton3.setTitleAndText("Destination Bin", inboundTask.getDestinationBin());
			mButton3.setTag(inboundTask.getDestinationBin());
			if(!inboundTask.getDestinationBin().isEmpty())
				mValidationButtonsList.add(mButton3);

			mButton4.setVisibility(View.GONE);
			mButton5.setVisibility(View.GONE);
		}
		else{
			isHWUT = false;
			mButton2.setTitleAndText("Product", inboundTask.getProduct());
			mButton2.setTag(inboundTask.getProduct());
			if(!inboundTask.getProduct().isEmpty())
				mValidationButtonsList.add(mButton2);

			mButton3.setTitleAndText("Qunatity", inboundTask.getTotalQuantity()+" "+inboundTask.getQuantityUoM());
			mButton3.setTag(inboundTask.getTotalQuantity());
			mValidationButtonsList.add(mButton3);

			mButton4.setTitleAndText("Serial", inboundTask.getSerial());
			mButton4.setTag(inboundTask.getSerial());
			if(!inboundTask.getSerial().isEmpty())
				mValidationButtonsList.add(mButton4);

			mButton5.setTitleAndText("Destination Bin", inboundTask.getDestinationBin());
			mButton5.setTag(inboundTask.getDestinationBin());
			if(!inboundTask.getDestinationBin().isEmpty())
				mValidationButtonsList.add(mButton5);
		}

		mCurrentFocusedButton = null;
		mNextFocusedButton = mButton1;
		changeValidationCustomButtonFocus();
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		onManualDialogClosed(scanValue);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);	
	}
}