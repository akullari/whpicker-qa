package com.marlabs.whpicker.inbound;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.models.InboundReceiveHU;
import com.marlabs.whpicker.utility.AppConstants;

public class HUDetailView extends Activity{

	private CustomButton mButton1, mButton2, mButton3, mButton4, mButton5;
	private Button mButtonEnter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom_validations);

		InboundReceiveHU inboundReceiveHU = null;
		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			inboundReceiveHU = (InboundReceiveHU) bundle.get(AppConstants.TASK_DETAILS);
		}
		else{
			finish();
		}
		
		CustomActionBar actionBar = new CustomActionBar(getActionBar(), this, inboundReceiveHU.getDocumentID());
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		mButton1 = (CustomButton) findViewById(R.id.validation_button1);
		mButton2 = (CustomButton) findViewById(R.id.validation_button2);
		mButton3 = (CustomButton) findViewById(R.id.validation_button3);
		mButton4 = (CustomButton) findViewById(R.id.validation_button4);
		mButton5 = (CustomButton) findViewById(R.id.validation_button5);
		findViewById(R.id.bottomlayout_textview_manual).setVisibility(View.GONE);
		mButtonEnter = (Button) findViewById(R.id.bottomlayout_button_scan);
		mButtonEnter.setText("Close");
		
		mButton1.removeVerification();
		mButton2.removeVerification();
		mButton3.removeVerification();
		mButton4.removeVerification();
		mButton5.removeVerification();

		mButton1.setTitleAndText("Delivery No.", inboundReceiveHU.getDocumentID());
		mButton2.setTitleAndText("HU No.", inboundReceiveHU.getHandlingUnit());
		mButton3.setTitleAndText("Product No.", inboundReceiveHU.getProduct());
		mButton4.setTitleAndText("Batch", inboundReceiveHU.getBatch());
		mButton5.setTitleAndText("Quantity", inboundReceiveHU.getQunatity() + " "+ inboundReceiveHU.getUOM());
		mButtonEnter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();				
			}
		});
	}
}
