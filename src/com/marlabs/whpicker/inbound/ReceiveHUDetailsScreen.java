package com.marlabs.whpicker.inbound;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.models.InboundReceiveHU;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Utility;

public class ReceiveHUDetailsScreen extends Activity  {

	private CustomActionBar actionBar;
	private boolean unload = false, createWT = false, confirmWT = false, postGR = false;
	private Button buttonCreateWT, buttonUnload, buttonConfirmWT, buttonPostGR;
	private CheckBox mCheckBoxUnloaded, mCheckBoxGRPosted, mCheckBoxWTCreated;
	private List<InboundReceiveHU> mInboundReceiveHUsList;
	private int mTaskCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		InboundReceiveHU inboundReceiveHU = null;
		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			inboundReceiveHU = (InboundReceiveHU) bundle.get(AppConstants.TASK_DETAILS);
			//parentID = bundle.getInt(AppConstants.PARENTID);
			mInboundReceiveHUsList = inboundReceiveHU.getReceiveHUsList();
		}
		else{
			finish();
		}

		switchToDetailView(mTaskCount);
	}

	protected void switchToDetailView(int count) {
		setContentView(R.layout.activity_custom_validations);
		CustomButton button1 = (CustomButton) findViewById(R.id.validation_button1);
		CustomButton button2 = (CustomButton) findViewById(R.id.validation_button2);
		findViewById(R.id.validation_button3).setVisibility(View.GONE);
		findViewById(R.id.validation_button4).setVisibility(View.GONE);
		findViewById(R.id.validation_button5).setVisibility(View.GONE);
		Button buttonEnter = (Button) findViewById(R.id.bottomlayout_button_scan);
		buttonEnter.setText("Enter");

		findViewById(R.id.bottomlayout_textview_manual).setVisibility(View.GONE);
		Button hUListButton = (Button) findViewById(R.id.bottomlayout_button_list);

		hUListButton.setVisibility(View.VISIBLE);
		hUListButton.setText("HU Details");

		button1.removeVerification();
		button2.removeVerification();

		final InboundReceiveHU inboundReceiveHU = mInboundReceiveHUsList.get(count);
		button1.setTitleAndText("HU", inboundReceiveHU.getHandlingUnit());
		button2.setTitleAndText("Proc. HUs", count+" / "+mInboundReceiveHUsList.size());
		hUListButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ReceiveHUDetailsScreen.this, HUDetailView.class); 
				intent.putExtra(AppConstants.TASK_DETAILS, inboundReceiveHU);
				startActivity(intent);
			}
		});

		buttonEnter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switchCreateTaskView(inboundReceiveHU);
			}
		});
	}

	protected void switchCreateTaskView(final InboundReceiveHU inboundReceiveHU) {
		setContentView(R.layout.activity_createtask);
		Button button1 = (Button) findViewById(R.id.createwt_button1);
		Utility.getInstance().removeVerification(this, button1);
		button1.setText("HU No : "+ inboundReceiveHU.getHandlingUnit());
		
		buttonUnload  = (Button) findViewById(R.id.createwt_button_unload);
		buttonCreateWT = (Button) findViewById(R.id.createwt_button_createwt);
		buttonConfirmWT  = (Button) findViewById(R.id.createwt_button_confwt);
		buttonPostGR  = (Button) findViewById(R.id.createwt_button_postgr);
		
		mCheckBoxGRPosted = (CheckBox) findViewById(R.id.createwt_checkbox_grposted);
		mCheckBoxUnloaded = (CheckBox) findViewById(R.id.createwt_checkbox_unloaded);
		mCheckBoxWTCreated = (CheckBox) findViewById(R.id.createwt_checkbox_wtcreated);
		
		buttonUnload.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				buttonUnload.setBackgroundColor(getResources().getColor(R.color.green));
				unload = true;
				isAllValidationsCompleted();
			}
		});
		buttonCreateWT.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				buttonCreateWT.setBackgroundColor(getResources().getColor(R.color.green));
				createWT = true;
				isAllValidationsCompleted();
			}
		});
		buttonConfirmWT.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ReceiveHUDetailsScreen.this, ReceiveHUValidations.class);
				intent.putExtra("key", 2);
				intent.putExtra(AppConstants.TASK_DETAILS, inboundReceiveHU);
				startActivityForResult(intent, 222);
			}
		});
		buttonPostGR.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				buttonPostGR.setBackgroundColor(getResources().getColor(R.color.green));
				postGR = true;
				isAllValidationsCompleted();
			}
		});
	}
	
	protected void isAllValidationsCompleted() {
		mCheckBoxGRPosted.setChecked(postGR);
		mCheckBoxWTCreated.setChecked(createWT);
		mCheckBoxUnloaded.setChecked(unload);
		
		if(unload && createWT && confirmWT && postGR){
			mTaskCount++;
			if(mTaskCount < mInboundReceiveHUsList.size()){
				switchToDetailView(mTaskCount);
				resetButtons();
			}
			else{
				Toast.makeText(getApplicationContext(), "TaskCompleted", Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void resetButtons() {
		unload = false; createWT = false;
		confirmWT = false; postGR = false;
		
		buttonCreateWT.setBackgroundColor(getResources().getColor(R.color.red));
		buttonConfirmWT.setBackgroundColor(getResources().getColor(R.color.red));
		buttonUnload.setBackgroundColor(getResources().getColor(R.color.red));
		buttonPostGR.setBackgroundColor(getResources().getColor(R.color.red));
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 222) {
			if (resultCode == RESULT_OK) {
				//String result = data.getStringExtra("INPUT_RESULT");
				buttonConfirmWT.setBackgroundColor(getResources().getColor(R.color.green));
				confirmWT = true;
				isAllValidationsCompleted();
			}
		} 
	}
}