package com.marlabs.whpicker.inbound;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.inbound.services.InboundBussinessClass;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.MenuDataSorce;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class PutAwayByWOScreen extends Activity implements IScannerListener  {

	private CustomActionBar actionBar;
	private int  mParentID = 0;
	private EditText mEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scanpo);
		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		StaticScanner.getInstance().scannerRead(this, 1);
		init();
	}

	private void init(){
		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			if(bundle.get(AppConstants.SELECTED_MENU) != null){
				HomeMenuItem homeMenuItem = (HomeMenuItem) bundle.get(AppConstants.SELECTED_MENU);
				actionBar.changeHeader(homeMenuItem.getItemName());
				mParentID = Integer.parseInt(homeMenuItem.getItemId());
			}
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		Utility.getInstance().showKeyboard(this);
		TextView textViewLabel = (TextView) findViewById(R.id.scanpo_textview_title);
		if(mParentID == MenuDataSorce.INBOUND_PUTAWAY_HU)
			textViewLabel.setText("*Please Scan or Enter HU");
		else if(mParentID == MenuDataSorce.INBOUND_PUTAWAY_WO)
			textViewLabel.setText("*Please Scan or Enter WO");

		Button buttonSubmit = (Button) findViewById(R.id.scanpo_button_submit);
		Button buttonReset = (Button) findViewById(R.id.scanpo_button_reset);
		mEditText = (EditText) findViewById(R.id.scanpo_edittext_code);

		buttonSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String inputValue = mEditText.getText().toString();
				if(Validation.isNullString(inputValue)){
					if(mParentID == MenuDataSorce.INBOUND_PUTAWAY_HU)
						Toast.makeText(getApplicationContext(), "Please enter HU.!", Toast.LENGTH_SHORT).show();
					else
						Toast.makeText(getApplicationContext(), "Please enter WO.!", Toast.LENGTH_SHORT).show();
					return;
				}
				Utility.getInstance().hideKeyboard(PutAwayByWOScreen.this, mEditText);
				callVolleyRequest(inputValue);
			}
		});

		buttonReset.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mEditText.setText("");
			}
		});
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		callVolleyRequest(scanValue);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	private void callVolleyRequest(String value) {
		if(mParentID == MenuDataSorce.INBOUND_PUTAWAY_WO)
			InboundBussinessClass.getInstance().callPutAwayWHOVolleyRequest(this, value);
		else{
			
		}
		Utility.getInstance().hideKeyboard(PutAwayByWOScreen.this, mEditText);
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	public void onOrdersResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			System.out.println("------ * response * -----" + jsonObject.toString());
			List<Order> orderObjectList = Parser.parseInboundOrdersResult(jsonObject);

			if(orderObjectList.size() == 0){
				StaticScanner.getInstance().scannerRead(this, 1);
				Toast.makeText(getApplicationContext(), "Invalid WO..!", Toast.LENGTH_SHORT).show();
				return;
			}

			Intent intent = new Intent(PutAwayByWOScreen.this, PutAwayResourceScreen.class);
			intent.putExtra(AppConstants.INBOUND_ORDER, orderObjectList.get(0));
			startActivity(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main_picker_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);	
	}
}