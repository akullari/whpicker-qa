package com.marlabs.whpicker.inbound;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.inbound.services.InboundSingleton;
import com.marlabs.whpicker.models.ConfirmResponse;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.ResourceSelectorScreen;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class PutAwayResourceScreen extends Activity implements IScannerListener  {

	private CustomButton mButton1, mButton2, mButton3, mButton4;
	private CustomButton mCurrentFocusedButton, mNextFocusedButton;
	private Drawable mButtonDrawable;
	private TextView mTextViewManual;
	private CustomActionBar actionBar;
	private Order mParentOrder;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom_validations);
		
		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		StaticScanner.getInstance().scannerRead(this, 1);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			mParentOrder = (Order) bundle.getSerializable(AppConstants.INBOUND_ORDER);
			actionBar.changeHeader(mParentOrder.getWHOrder());
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}
		InboundSingleton.getInstance().headersTask(this, mParentOrder);
		init(mParentOrder);
	}

	private void init(Order parentOrder){
		mButton1 = (CustomButton) findViewById(R.id.validation_button1);
		mButton2 = (CustomButton) findViewById(R.id.validation_button2);
		mButton3 = (CustomButton) findViewById(R.id.validation_button3);
		mButton4 = (CustomButton) findViewById(R.id.validation_button4);
		mButton4.setVisibility(View.VISIBLE);
		findViewById(R.id.validation_button5).setVisibility(View.GONE);
		mTextViewManual = (TextView) findViewById(R.id.bottomlayout_textview_manual);

		mButton1.setTitleAndText("Order Number", parentOrder.getWHOrder());
		mButton2.setTitleAndText("Zone", parentOrder.getZone());
		mButton3.setTitleAndText("Weight", "");
		mButton4.setTitleAndText("Resource", "");
		
		if(!Validation.isNullString(parentOrder.getResource())){
			mButton4.setTitleAndText("Resource", parentOrder.getResource());
		}

		mButton1.setAsVerified();
		mButton2.removeVerification();
		mButton3.removeVerification();

		mCurrentFocusedButton = null;
		mNextFocusedButton = mButton4;
		changeValidationCustomButtonFocus();

		mTextViewManual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent  = new Intent(PutAwayResourceScreen.this, ResourceSelectorScreen.class);
				startActivityForResult(intent, 1);
			}
		});
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
			if (resultCode== RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				onResourceScanned(result);
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.manual_entry_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
	}

	@SuppressLint("NewApi")
	private void changeValidationCustomButtonFocus(){
		if(mCurrentFocusedButton != null){
			mCurrentFocusedButton.setBackground(mButtonDrawable);
			mCurrentFocusedButton.setAsVerified();
			mCurrentFocusedButton.clearAnimation();
		}

		if(mNextFocusedButton != null){
			mButtonDrawable = mNextFocusedButton.getBackground();
			mNextFocusedButton.setBackgroundResource(R.drawable.header);
			mNextFocusedButton.startAnimation(Utility.getInstance().blinkAnimation());
			mCurrentFocusedButton = mNextFocusedButton;
			mCurrentFocusedButton.removeVerificationForBlink();
			mTextViewManual.setText("Enter "+ mCurrentFocusedButton.getTitle() +" Manually");
		}
		else{
			mCurrentFocusedButton = null;
		}
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
			onResourceScanned(scanValue);
			StaticScanner.getInstance().scannerRead(this, 1);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	private void onResourceScanned(String resource){
		if(Utility.getInstance().getResources().contains(resource)){
			mParentOrder.setResource(resource);
			mButton4.setTitleAndText("Resource", resource);
			if(mCurrentFocusedButton == mButton4){
				mNextFocusedButton = null;
			}
			callResourceAllocationService(resource);
		}
		else{
			mCurrentFocusedButton.setAsWrongVerified();
			StaticScanner.getInstance().scannerRead(this, 1);
		}
	}
	
	private void callResourceAllocationService(String resource){
		try {
			mParentOrder.setResource(resource);
			InboundSingleton.getInstance().allocateResource(this,PutAwayResourceScreen.class.getMethod("onResourceAllocated", 
					ConfirmResponse.class), mParentOrder);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
	public void onResourceAllocated(ConfirmResponse confirmResponse){
		int respCode = confirmResponse.getResponseCode();
		if(respCode >= 200 && respCode < 300){
			changeValidationCustomButtonFocus();
			
			Intent intent = new Intent(PutAwayResourceScreen.this, PutAwayTaskScreen.class);
			intent.putExtra(AppConstants.INBOUND_ORDER, mParentOrder);
			startActivity(intent);
		}
		else{
			StaticScanner.getInstance().scannerRead(this, 1);
			Util.showMessageDialog(this, confirmResponse.getResponseLine());
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);	
	}
}