package com.marlabs.whpicker.inbound.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.ConfirmResponse;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.models.Unloading;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.networking.UserInfo;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.Util;

public class InboundSingleton {
	private DefaultHttpClient client;
	private static  String receivedToken;
	private static InboundSingleton instance = new InboundSingleton();

	private InboundSingleton(){
		client = new DefaultHttpClient();
	}

	public static InboundSingleton getInstance(){
		return instance;
	}

	public void headersTask(Context context, Order order) {
		new HeadersAsyncTask(context, order).execute("");
	}
	
	public void closeTask(Context context, Method method, Task inboundTask) {
		new ConfirmAsyncTaskInbound(context, method, inboundTask).execute("");
	}
	
	public void allocateResource(Context context, Method method, Order order) {
		new ResourceAllocationAsyncTask(context, method, order).execute("");
	}

	public void closeUnloadingTask(Context context, Method method, Unloading unloading) {
		new ConfirmUnloadingAsyncTaskInbound(context, method, unloading).execute("");
	}
	
	public void unloadingHeadersTask(Context context, Unloading unloading) {
		new UnloadingHeadersAsyncTask(context, unloading).execute("");
	}

	public class HeadersAsyncTask extends AsyncTask<String, Void, String> {
		private ProgressDialog mProgressDialog;
		private Order mOrder;
		private Context context;
		public HeadersAsyncTask(Context context, Order inboundOrder) {
			this.mOrder = inboundOrder;
			this.context = context;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			fetchHeaders(context, mOrder);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			mProgressDialog.dismiss();
		}
	}
	
	public void fetchHeaders(Context context, Order order){

		String url = URLFactory.getBaseGateWayURL(context)+"odata/sap/ZEWM_WAREHOUSE_TASK_READ_SRV/WHOpenTaskSet" +
				"(Warehouse='W001')?$format=json";
		HttpGet get = new HttpGet(url);

		get.setHeader("x-csrf-token", "fetch");
		//get.setHeader("Authorization", UserInfo.getInstance().getAuthorizationHeader());
		get.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));

		HttpResponse response = null;
		try {
			response = client.execute(get);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		List<Cookie> cookies = client.getCookieStore().getCookies();
		if (cookies.isEmpty()) {
			System.out.println("None");
		} 
		else {
			for (int i = 0; i < cookies.size(); i++) {
				System.out.println("- " + cookies.get(i).toString());
			}
		}

		Header[] headerFields = response.getAllHeaders();
		for (Header header : headerFields) {
			if (header.getName().equalsIgnoreCase("x-csrf-token")) {
				InboundSingleton.receivedToken = header.getValue();
				System.out.println("Token GET request "+receivedToken);
			}
		}
	}
	
	public class ConfirmAsyncTaskInbound extends AsyncTask<String, Void, ConfirmResponse> {
		private ProgressDialog mProgressDialog;
		private Context context;
		private Method method;
		private Task inboundTask;
		public ConfirmAsyncTaskInbound(Context context, Method method, Task inboundTask) {
			this.context = context;
			this.inboundTask = inboundTask;
			this.method = method;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected ConfirmResponse doInBackground(String... params) {
			return closeSingleTask(context, inboundTask);
		}

		@Override
		protected void onPostExecute(ConfirmResponse confirmResponse) {
			mProgressDialog.dismiss();
			try {
				method.invoke(context, confirmResponse);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	public ConfirmResponse closeSingleTask(Context context, Task inboundTask){
		ConfirmResponse confirmResponse = new ConfirmResponse();
		String url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_WAREHOUSE_TASK_READ_SRV/WHOpenTaskConfirmSet(WHTask='"+
				inboundTask.getWareHouseTask()+"',Warehouse='W001')";
		HttpPut put = new HttpPut(url);

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("WHTask", inboundTask.getWareHouseTask());
		params.put("Warehouse", UserInfo.getInstance().getWareHouse(context));
		//params.put("Quantity", inboundOrder.getQuantity());
		//params.put("Unit",  inboundOrder.getQuantityUoM());
		//params.put("DestBin", inboundOrder.getDestinationBin());
		params.put("Queue", inboundTask.getQueue() != null? inboundTask.getQueue():"");
		params.put("DestRsrc", inboundTask.getDestinationBin());
		JSONObject jsonObject = new JSONObject(params);
		System.out.println(jsonObject.toString());

		try {
			System.out.println("Received token "+receivedToken);
			put.setHeader("x-csrf-token", receivedToken);
			//put.setHeader("Authorization", UserInfo.getInstance().getAuthorizationHeader());
			put.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));
			put.setHeader("Content-Type", "application/json; charset=utf-8");
			put.setHeader("Accept", "application/json");
			put.setHeader("charset", "utf-8");
			put.setHeader("Accept-Language","en-US" );

			List<Cookie> cookies = client.getCookieStore().getCookies();

			String cookieString = "";
			for (Cookie cookie : cookies) {
				cookieString+=cookie.getName()+"="+cookie.getValue()+";";

			}
			put.setHeader("Cookie", cookieString);

			HttpEntity entity;
			StringEntity stringEntity = new StringEntity(jsonObject.toString());
			stringEntity.setContentEncoding((Header) new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			entity = stringEntity;
			put.setEntity(entity);

			HttpResponse response = client.execute(put);
			int responseCode = response.getStatusLine().getStatusCode();
			confirmResponse.setResponseCode(responseCode);
			System.out.println("Response Code "+responseCode);
			System.out.println("Response "+response);
			if(responseCode > 200 && responseCode < 300){
				confirmResponse.setResponseLine("Success");
				return confirmResponse;
			}
			else{
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				while ((line = rd.readLine()) != null) {
					System.out.println(line);
					confirmResponse.setResponseLine(Parser.parseError(line));
					return confirmResponse;
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
	
	public class ResourceAllocationAsyncTask extends AsyncTask<String, Void, ConfirmResponse> {
		private ProgressDialog mProgressDialog;
		private Context context;
		private Method method;
		private Order mOrder;
		public ResourceAllocationAsyncTask(Context context, Method method, Order order) {
			this.context = context;
			this.mOrder = order;
			this.method = method;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected ConfirmResponse doInBackground(String... params) {
			return allocateResource(context, mOrder);
		}

		@Override
		protected void onPostExecute(ConfirmResponse confirmResponse) {
			mProgressDialog.dismiss();
			try {
				method.invoke(context, confirmResponse);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}
	
	public ConfirmResponse allocateResource(Context context, Order order){
		ConfirmResponse confirmResponse = new ConfirmResponse();
		
		String url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_WHO_STATUS_CHANGE_SRV/WhOrderSet(" +
				"IvLgnum='"+ UserInfo.getInstance().getWareHouse(context)+"',IvWhoid='"+order.getWHOrder()+"')";
		HttpPut put = new HttpPut(url);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("IvLgnum", UserInfo.getInstance().getWareHouse(context));
		params.put("IvWhoid", order.getWHOrder());
		params.put("Rsrc", order.getResource());
		JSONObject jsonObject = new JSONObject(params);
		System.out.println("resource----" + jsonObject.toString());

		try {
			System.out.println("Received token "+receivedToken);
			put.setHeader("x-csrf-token", receivedToken);
			//put.setHeader("Authorization", UserInfo.getInstance().getAuthorizationHeader());
			put.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));
			put.setHeader("Content-Type", "application/json; charset=utf-8");
			put.setHeader("Accept", "application/json");
			put.setHeader("charset", "utf-8");
			put.setHeader("Accept-Language","en-US" );

			List<Cookie> cookies = client.getCookieStore().getCookies();

			String cookieString = "";
			for (Cookie cookie : cookies) {
				cookieString+=cookie.getName()+"="+cookie.getValue()+";";

			}
			put.setHeader("Cookie", cookieString);

			HttpEntity entity;
			StringEntity stringEntity = new StringEntity(jsonObject.toString());
			stringEntity.setContentEncoding((Header) new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			entity = stringEntity;
			put.setEntity(entity);

			HttpResponse response = client.execute(put);
			int responseCode = response.getStatusLine().getStatusCode();
			confirmResponse.setResponseCode(responseCode);
			System.out.println("Response Code "+responseCode);
			System.out.println("Response "+response);
			if(responseCode > 200 && responseCode < 300){
				confirmResponse.setResponseLine("Success");
				return confirmResponse;
			}
			else{
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				while ((line = rd.readLine()) != null) {
					System.out.println(line);
					confirmResponse.setResponseLine(Util.getDataInBraces(Parser.parseError(line)));
					return confirmResponse;
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
	
	public class ConfirmUnloadingAsyncTaskInbound extends AsyncTask<String, Void, ConfirmResponse> {
		private ProgressDialog mProgressDialog;
		private Context context;
		private Method method;
		private Unloading unloading;
		public ConfirmUnloadingAsyncTaskInbound(Context context, Method method, Unloading unloading) {
			this.context = context;
			this.unloading = unloading;
			this.method = method;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected ConfirmResponse doInBackground(String... params) {
			return closeUnloadingSingleTask(context, unloading);
		}

		@Override
		protected void onPostExecute(ConfirmResponse confirmResponse) {
			mProgressDialog.dismiss();
			try {
				method.invoke(context, confirmResponse);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	public ConfirmResponse closeUnloadingSingleTask(Context context, Unloading unloading){
		ConfirmResponse confirmResponse = new ConfirmResponse();
		String url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_WAREHOUSE_TASK_READ_SRV/WHOpenTaskConfirmSet(WHTask='"+
				unloading.getWareHouseTask()+"',Warehouse='"+ UserInfo.getInstance().getWareHouse(context)+"')";
		HttpPut put = new HttpPut(url);

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("WHTask", unloading.getWareHouseTask());
		params.put("Warehouse", UserInfo.getInstance().getWareHouse(context));
		params.put("Queue", unloading.getQueue() != null? unloading.getQueue():"");
		JSONObject jsonObject = new JSONObject(params);
		System.out.println(jsonObject.toString());

		try {
			System.out.println("Received token "+receivedToken);
			put.setHeader("x-csrf-token", receivedToken);
			//put.setHeader("Authorization", UserInfo.getInstance().getAuthorizationHeader());
			put.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));
			put.setHeader("Content-Type", "application/json; charset=utf-8");
			put.setHeader("Accept", "application/json");
			put.setHeader("charset", "utf-8");
			put.setHeader("Accept-Language","en-US" );

			List<Cookie> cookies = client.getCookieStore().getCookies();

			String cookieString = "";
			for (Cookie cookie : cookies) {
				cookieString+=cookie.getName()+"="+cookie.getValue()+";";

			}
			put.setHeader("Cookie", cookieString);

			HttpEntity entity;
			StringEntity stringEntity = new StringEntity(jsonObject.toString());
			stringEntity.setContentEncoding((Header) new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			entity = stringEntity;
			put.setEntity(entity);

			HttpResponse response = client.execute(put);
			int responseCode = response.getStatusLine().getStatusCode();
			confirmResponse.setResponseCode(responseCode);
			System.out.println("Response Code "+responseCode);
			System.out.println("Response "+response);
			if(responseCode > 200 && responseCode < 300){
				confirmResponse.setResponseLine("Success");
				return confirmResponse;
			}
			else{
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				while ((line = rd.readLine()) != null) {
					System.out.println(line);
					confirmResponse.setResponseLine(Parser.parseError(line));
					return confirmResponse;
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
	
	public class UnloadingHeadersAsyncTask extends AsyncTask<String, Void, String> {
		private ProgressDialog mProgressDialog;
		private Unloading unloading;
		private Context context;
		public UnloadingHeadersAsyncTask(Context context, Unloading unloading) {
			this.unloading = unloading;
			this.context = context;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			fetchUnloadingHeaders(context, unloading);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			mProgressDialog.dismiss();
		}
	}
	
	public void fetchUnloadingHeaders(Context context, Unloading unloading){

		String url = URLFactory.getBaseGateWayURL(context) +"odata/sap/ZEWM_WAREHOUSE_TASK_READ_SRV/WHOpenTaskSet(Warehouse='" +
				UserInfo.getInstance().getWareHouse(context)+"')?$format=json";
		HttpGet get = new HttpGet(url);

		get.setHeader("x-csrf-token", "fetch");
		//get.setHeader("Authorization", UserInfo.getInstance().getAuthorizationHeader());
		get.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));

		HttpResponse response = null;
		try {
			response = client.execute(get);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		List<Cookie> cookies = client.getCookieStore().getCookies();
		if (cookies.isEmpty()) {
			System.out.println("None");
		} 
		else {
			for (int i = 0; i < cookies.size(); i++) {
				System.out.println("- " + cookies.get(i).toString());
			}
		}

		Header[] headerFields = response.getAllHeaders();
		for (Header header : headerFields) {
			if (header.getName().equalsIgnoreCase("x-csrf-token")) {
				InboundSingleton.receivedToken = header.getValue();
				System.out.println("Token GET request "+receivedToken);
			}
		}
	}
}