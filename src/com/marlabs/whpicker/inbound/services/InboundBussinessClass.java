package com.marlabs.whpicker.inbound.services;

import org.json.JSONObject;

import android.content.Context;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.inbound.InboundProcessorScreen;
import com.marlabs.whpicker.inbound.PutAwayByWOScreen;
import com.marlabs.whpicker.inbound.ReceiveHUDetailsScreen;
import com.marlabs.whpicker.inbound.ReceiveHUScreen;
import com.marlabs.whpicker.inbound.UnloadingScreen;
import com.marlabs.whpicker.networking.ConnectionDetector;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.networking.VolleyRequestHandler;
import com.marlabs.whpicker.outbound.ByQueuesScreen;
import com.marlabs.whpicker.outbound.OrdersScreen;
import com.marlabs.whpicker.utility.MenuDataSorce;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;

public class InboundBussinessClass {

	private static InboundBussinessClass instance = new InboundBussinessClass();

	private InboundBussinessClass(){}

	public static InboundBussinessClass getInstance(){
		return instance;
	}

	/*****************Receive By HU API Call**************/
	public void callReceiveHUByDeliveryVolleyRequest(Context context, String inputValue) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, 
						URLFactory.getReceiveHUDelivery_URL(context, inputValue), null, Util.getBasicHANAHeaders(context),
						ReceiveHUScreen.class.getMethod("onDocumentIDResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	public void callReceiveHUByTUVolleyRequest(Context context, String inputValue) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, 
						URLFactory.getReceiveHUByTU_URL(context, inputValue), null, Util.getBasicHANAHeaders(context),
						ReceiveHUScreen.class.getMethod("onDocumentIDResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	public void callReceiveHUByASNVolleyRequest(Context context, String inputValue) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, 
						URLFactory.getReceiveHUASN_URL(context, inputValue), null, Util.getBasicHANAHeaders(context),
						ReceiveHUScreen.class.getMethod("onDocumentIDResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	public void callReceiveHUDetailsVolleyRequest(Context context,	String documentID) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, 
						URLFactory.getReceiveHUDetails_URL(context, documentID), null, Util.getBasicHANAHeaders(context),
						ReceiveHUScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}

	}

	public void callReceiveHUConfirmTaskDetails(Context context, String documentID) {

		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, 
						URLFactory.getReceiveHUDetails_URL(context, documentID), null, Util.getBasicHANAHeaders(context),
						ReceiveHUDetailsScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	/*****************Processor API Call**************/
	public void callProcessorOrdersVolleyRequest(Context context) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				String url = URLFactory.getProcessorOrders_URL(context);
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						InboundProcessorScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	/*****************PutAway API Call**************/
	public void callQueuesVolleyRequest(Context context) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				String url = URLFactory.getPutAwayQueues_URL(context);
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						ByQueuesScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	public void callOrdersVolleyRequest(Context context, String inputValue) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				String url = URLFactory.getInboundOrders_URL(context, inputValue);
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						OrdersScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	//	public void callAllOrdersVolleyRequest(Context context) {
	//		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
	//			try {
	//				String url = URLFactory.getAllInboundOrders_URL(context);
	//				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, Util.getBasicHANAHeaders(context),
	//						ByQueuesScreen.class.getMethod("onOrdersResponse", JSONObject.class, VolleyError.class));
	//			} catch (NoSuchMethodException e) {
	//				e.printStackTrace();
	//			}
	//		}
	//		else{
	//			Utility.getInstance().showAlertDialog(context, AppConstants.NO_CONNECTIVITY);
	//		}
	//	}

	public void callPutAwayWHOVolleyRequest(Context context, String inputValue) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				String url = URLFactory.getPutawayByWO_URL(context, inputValue);
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						PutAwayByWOScreen.class.getMethod("onOrdersResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	/*****************Unloading API Call**************/
	public void callUnloadingVolleyRequest(Context context, String inputValue, int parentID) {
		String url = "";
		if(parentID == MenuDataSorce.INBOUND_UNLOADING_DELIVERY)
			url = URLFactory.getUnloadingByDelivery_URL(context, inputValue);
		else if(parentID == MenuDataSorce.INBOUND_UNLOADING_TU)
			url = URLFactory.getUnloadingByTU_URL(context, inputValue);
		else if(parentID == MenuDataSorce.INBOUND_UNLOADING_HU)
			url = URLFactory.getUnloadingByHU_URL(context, inputValue);
		else
			url = URLFactory.getUnloadingByASN_URL(context, inputValue);

		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						UnloadingScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}
}