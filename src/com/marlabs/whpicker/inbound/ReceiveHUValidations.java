package com.marlabs.whpicker.inbound;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.models.InboundReceiveHU;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.KeyPadReaderScreen;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class ReceiveHUValidations extends Activity implements IScannerListener  {

	private CustomActionBar actionBar;
	private CustomButton mButton1, mButton2, mButton3;
	private TextView mTextViewManual;
	private int key = 0;
	private InboundReceiveHU inboundReceiveHU = null;
	List<CustomButton> mValidationButtonsList = new ArrayList<CustomButton>();
	public CustomButton mCurrentFocusedButton, mNextFocusedButton;
	public Drawable mButtonDrawable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		actionBar = new CustomActionBar(getActionBar(), this, "Receive HU by Delivery");
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			key = bundle.getInt("key");
		}

		StaticScanner.getInstance().scannerRead(this, 1);
		if(key == 1)
			init();
		else{
			actionBar.changeHeader(getString(R.string.selected_delivery));
			switchToValidationView("");
		}
	}

	private void init() {
		setContentView(R.layout.activity_scanpo);
		TextView textViewLabel = (TextView) findViewById(R.id.scanpo_textview_title);
		textViewLabel.setText("*Please Scan or Enter Delivery");

		Button buttonSubmit = (Button) findViewById(R.id.scanpo_button_submit);
		Button buttonReset = (Button) findViewById(R.id.scanpo_button_reset);
		final EditText editText = (EditText) findViewById(R.id.scanpo_edittext_code);

		Utility.getInstance().showKeyboard(this);
		buttonSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String value = editText.getText().toString();
				if(Validation.isNullString(value)){
					Toast.makeText(getApplicationContext(), "Please enter Delivery Number.!", Toast.LENGTH_SHORT).show();
					return;
				}
				Utility.getInstance().hideKeyboard(ReceiveHUValidations.this, editText);
				//switchToDeliveryView(value);
				returnIntentresult(value);
			}
		});

		buttonReset.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				editText.setText("");
			}
		});
	}

	private void switchToValidationView(String deliveryNumber){
		setContentView(R.layout.activity_custom_validations);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			inboundReceiveHU = (InboundReceiveHU) bundle.get(AppConstants.TASK_DETAILS);
		}
		else{
			finish();
		}

		mButton1 = (CustomButton) findViewById(R.id.validation_button1);
		mButton2 = (CustomButton) findViewById(R.id.validation_button2);
		mButton3 = (CustomButton) findViewById(R.id.validation_button3);
		findViewById(R.id.validation_button4).setVisibility(View.GONE);
		findViewById(R.id.validation_button5).setVisibility(View.GONE);
		mTextViewManual = (TextView) findViewById(R.id.bottomlayout_textview_manual);
		mTextViewManual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent  = new Intent(ReceiveHUValidations.this, KeyPadReaderScreen.class);
				intent.putExtra(AppConstants.ALPHA_KEYPAD_TITLE, mTextViewManual.getText().toString());
				intent.putExtra(AppConstants.ALPHA_KEYPAD_BATCHINPUT, "");
				startActivityForResult(intent, 1);
			}
		});

		mButton1.setTitleAndText("Source HU", inboundReceiveHU.getSourceHU());
		mButton2.setTitleAndText("Dest HU", inboundReceiveHU.getDestinationHU());
		mButton3.setTitleAndText("Dest BIN", inboundReceiveHU.getDestinationStorageBin());

		mButton1.setOnClickListener(validationButonClickLisener);
		mButton2.setOnClickListener(validationButonClickLisener);
		mButton3.setOnClickListener(validationButonClickLisener);

		mButton1.setEnabled(true);
		mButton2.setEnabled(false);
		mButton3.setEnabled(false);

		mValidationButtonsList.add(mButton1);
		mValidationButtonsList.add(mButton2);
		mValidationButtonsList.add(mButton3);

		mCurrentFocusedButton = null;
		mNextFocusedButton = mButton1;
		changeValidationCustomButtonFocus();
	}

	OnClickListener validationButonClickLisener = new OnClickListener() {
		@SuppressLint("NewApi")
		@Override
		public void onClick(View view) {
			CustomButton button = (CustomButton) view;
			if(mCurrentFocusedButton != null){
				mCurrentFocusedButton.setBackground(mButtonDrawable);
				mCurrentFocusedButton.clearAnimation();
				mCurrentFocusedButton.setAsNotVerified();
			}
			button.setAsNotVerified();
			mCurrentFocusedButton = null;
			mNextFocusedButton = button;
			changeValidationCustomButtonFocus();
			index--;
		}
	};

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
			if (resultCode== RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				onManualDialogClosed(result);
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.manual_entry_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void onManualDialogClosed(String result){
		if(mCurrentFocusedButton.getTag().toString().equalsIgnoreCase(result))
			switchButtons();
		else
			mCurrentFocusedButton.setAsWrongVerified();
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	int index = 0;
	private void switchButtons() {
		index = index+1;
		if(index < mValidationButtonsList.size()){
			mNextFocusedButton = mValidationButtonsList.get(index);
			mValidationButtonsList.get(index).setEnabled(true);
		}
		else{
			mNextFocusedButton = null;
			Intent returnIntent = new Intent();
			returnIntent.putExtra("INPUT_RESULT", "Scan Completed");
			setResult(RESULT_OK, returnIntent);
			finish();
		}
		changeValidationCustomButtonFocus();
	}

	@SuppressLint("NewApi")
	public void changeValidationCustomButtonFocus(){
		if(mCurrentFocusedButton != null){
			mCurrentFocusedButton.setBackground(mButtonDrawable);
			mCurrentFocusedButton.setAsVerified();
			mCurrentFocusedButton.clearAnimation();
		}

		if(mNextFocusedButton != null){
			mButtonDrawable = mNextFocusedButton.getBackground();
			mNextFocusedButton.setBackgroundResource(R.drawable.header);
			mNextFocusedButton.startAnimation(Utility.getInstance().blinkAnimation());
			mCurrentFocusedButton = mNextFocusedButton;
			mCurrentFocusedButton.removeVerificationForBlink();
			mTextViewManual.setText("Enter "+ mCurrentFocusedButton.getTitle() +" Manually");
		}
		else{
			mCurrentFocusedButton = null;
		}
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex){
		returnIntentresult(scanValue);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	private void returnIntentresult(String result) {
		if(key == 1){
			Intent returnIntent = new Intent();
			returnIntent.putExtra("INPUT_RESULT", result);
			setResult(RESULT_OK, returnIntent);
			finish();
		}
		else{
			onManualDialogClosed(result);
		}
	}
}