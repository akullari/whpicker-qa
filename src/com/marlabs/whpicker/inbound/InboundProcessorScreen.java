package com.marlabs.whpicker.inbound;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.adapters.OrderAdapter;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.inbound.services.InboundBussinessClass;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.networking.UserInfo;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Utility;

public class InboundProcessorScreen extends Activity{
	private CustomActionBar actionBar;
	private ListView mListView;
	private LinearLayout mLinearLayout;
	private TextView mTextViewUser, mTextViewWarehouse;
	private List<Order> mInboundOrdersList;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			if(bundle.get(AppConstants.SELECTED_MENU) != null){
				HomeMenuItem homeMenuItem = (HomeMenuItem) bundle.get(AppConstants.SELECTED_MENU);
				actionBar.changeHeader(homeMenuItem.getItemName());
			}
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		mLinearLayout = (LinearLayout) findViewById(R.id.queueslist_headers);
		mLinearLayout.setVisibility(View.GONE);
		mListView = (ListView) findViewById(R.id.pulltorefresh_listview);
		mTextViewUser = (TextView) findViewById(R.id.queueslist_textview_user);
		mTextViewWarehouse = (TextView) findViewById(R.id.queueslist_textview_warehouse);

		mTextViewUser.setText(UserInfo.getInstance().getUserID(this));
		mTextViewWarehouse.setText(UserInfo.getInstance().getWareHouse(this));

		InboundBussinessClass.getInstance().callProcessorOrdersVolleyRequest(this);

		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(InboundProcessorScreen.this, PutAwayTaskScreen.class);
				intent.putExtra(AppConstants.INBOUND_ORDER, mInboundOrdersList.get(position));
				startActivity(intent);
			}
		});
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			System.out.println("------ * response * -----" + jsonObject.toString());
			mInboundOrdersList = Parser.parseInboundOrdersResult(jsonObject);
			if(mInboundOrdersList.size() == 0){
				Utility.getInstance().showAlertDialog(InboundProcessorScreen.this, "No Orders associated with processor..!");
			}
			else{
				mListView.setAdapter(new OrderAdapter(InboundProcessorScreen.this, mInboundOrdersList));
			}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(InboundProcessorScreen.this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

}
