package com.marlabs.whpicker.inbound;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.inbound.services.InboundBussinessClass;
import com.marlabs.whpicker.inbound.services.InboundSingleton;
import com.marlabs.whpicker.models.ConfirmResponse;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.models.Unloading;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.KeyPadReaderScreen;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.MenuDataSorce;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class UnloadingScreen extends Activity implements IScannerListener  {

	private CustomButton mButton1, mButton2, mButton3, mButton4, mButton5;
	private CustomButton mCurrentFocusedButton, mNextFocusedButton;
	private Drawable mButtonDrawable;
	private Button mButtonScan;
	private TextView mTextViewManual;
	private LinearLayout linearLayoutParent;
	private int isDialog = 0, taskCount = 0;
	private List<Unloading> mUnloadingList;
	private CustomActionBar actionBar;
	private int mParentID = 0;
	protected Animation animation1, animation2;
	private Unloading mCurrentTask;
	private EditText mEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scanpo);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			if(bundle.get(AppConstants.SELECTED_MENU) != null){
				HomeMenuItem homeMenuItem = (HomeMenuItem) bundle.get(AppConstants.SELECTED_MENU);
				actionBar.changeHeader(homeMenuItem.getItemName());
				mParentID = Integer.parseInt(homeMenuItem.getItemId());
			}
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}
		
		StaticScanner.getInstance().scannerRead(this, 1);		
		animation1 = AnimationUtils.loadAnimation(this, R.anim.fliptomiddle);
		animation2 = AnimationUtils.loadAnimation(this, R.anim.flipfrommiddle);

		TextView textViewLabel = (TextView) findViewById(R.id.scanpo_textview_title);
		if(mParentID == MenuDataSorce.INBOUND_UNLOADING_DELIVERY)
			textViewLabel.setText("*Please Scan or Enter Delivery");
		else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_TU)
			textViewLabel.setText("*Please Scan or Enter TU");
		else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_HU)
			textViewLabel.setText("*Please Scan or Enter HU");
		else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_ASN)
			textViewLabel.setText("*Please Scan or Enter ASN");

		Button buttonSubmit = (Button) findViewById(R.id.scanpo_button_submit);
		Button buttonReset = (Button) findViewById(R.id.scanpo_button_reset);
		mEditText = (EditText) findViewById(R.id.scanpo_edittext_code);
		Utility.getInstance().showKeyboard(this);
		buttonSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String value = mEditText.getText().toString();
				if(Validation.isNullString(value)){
					if(mParentID == MenuDataSorce.INBOUND_UNLOADING_DELIVERY)
						Toast.makeText(getApplicationContext(), "Please enter Delivery Number.!", Toast.LENGTH_SHORT).show();
					else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_TU)
						Toast.makeText(getApplicationContext(), "Please enter TU.!", Toast.LENGTH_SHORT).show();
					else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_HU)
						Toast.makeText(getApplicationContext(), "Please enter HU.!", Toast.LENGTH_SHORT).show();
					else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_ASN)
						Toast.makeText(getApplicationContext(), "Please enter ASN.!", Toast.LENGTH_SHORT).show();
					return;
				}

				Utility.getInstance().hideKeyboard(UnloadingScreen.this, mEditText);

				if(mParentID == MenuDataSorce.INBOUND_UNLOADING_DELIVERY)
					value = Util.addLeadingZeros(value, AppConstants.DELIVERY_LENGTH);
				else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_TU)
					value = Util.addLeadingZeros(value, AppConstants.TU_LENGTH);
				else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_HU)
					value = Util.addLeadingZeros(value, AppConstants.HU_LENGTH);
				else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_ASN)
					value = Util.addLeadingZeros(value, AppConstants.TU_LENGTH);

				switchToValidationView(value);
			}
		});

		buttonReset.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mEditText.setText("");
			}
		});
	}

	private void switchToValidationView(String inputValue){
		StaticScanner.getInstance().scannerRead(this, 1);
		Utility.getInstance().hideKeyboard(this, mEditText);
		isDialog = 1;
		setContentView(R.layout.activity_custom_validations);

		mButton1 = (CustomButton) findViewById(R.id.validation_button1);
		mButton2 = (CustomButton) findViewById(R.id.validation_button2);
		mButton3 = (CustomButton) findViewById(R.id.validation_button3);
		mButton4 = (CustomButton) findViewById(R.id.validation_button4);
		mButton5 = (CustomButton) findViewById(R.id.validation_button5);
		linearLayoutParent = (LinearLayout) findViewById(R.id.linearlayout_validation);
		mButtonScan = (Button) findViewById(R.id.bottomlayout_button_scan);
		mTextViewManual = (TextView) findViewById(R.id.bottomlayout_textview_manual);

		animation1.setAnimationListener(animationListener);
		animation2.setAnimationListener(animationListener);

		mButton1.removeVerification();
		mButton2.removeVerification();
		mButton3.removeVerification();
		mButton5.removeVerification();

		mButton4.setEnabled(true);
		mButton4.setOnClickListener(validationButonClickLisener);

		mTextViewManual.setText("Validate HU manually");
		mButtonScan.setText("Scan HU");

		InboundBussinessClass.getInstance().callUnloadingVolleyRequest(this, inputValue, mParentID);

		mTextViewManual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent  = new Intent(UnloadingScreen.this, KeyPadReaderScreen.class);
				intent.putExtra(AppConstants.ALPHA_KEYPAD_TITLE, mTextViewManual.getText().toString());
				intent.putExtra(AppConstants.ALPHA_KEYPAD_BATCHINPUT, "");
				startActivityForResult(intent, 1);
			}
		});
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
			if (resultCode== RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				onManualDialogClosed(result);
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.manual_entry_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void onManualDialogClosed(String result){
		if(mCurrentFocusedButton.getTag().toString().equalsIgnoreCase(result))
			switchButtons();
		else{
			mCurrentFocusedButton.setAsWrongVerified();
			return;
		}
	}

	AnimationListener animationListener = new AnimationListener() {
		@Override
		public void onAnimationStart(Animation animation) {}

		@Override
		public void onAnimationRepeat(Animation animation) {}

		@Override
		public void onAnimationEnd(Animation animation) {
			if (animation==animation1) {
				(linearLayoutParent).clearAnimation();
				(linearLayoutParent).setAnimation(animation2);
				(linearLayoutParent).startAnimation(animation2);
			} 
		}
	};

	OnClickListener validationButonClickLisener = new OnClickListener() {
		@SuppressLint("NewApi")
		@Override
		public void onClick(View view) {
			CustomButton button = (CustomButton) view;
			if(mCurrentFocusedButton != null){
				mCurrentFocusedButton.setBackground(mButtonDrawable);
				mCurrentFocusedButton.clearAnimation();
				mCurrentFocusedButton.setAsNotVerified();
			}
			button.setAsNotVerified();
			mCurrentFocusedButton = null;
			mNextFocusedButton = button;
			changeValidationCustomButtonFocus();
			index--;
		}
	};

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			if(mParentID == MenuDataSorce.INBOUND_UNLOADING_DELIVERY)
				mUnloadingList = Parser.parseUnloadingByDeliveryResult(jsonObject);
			else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_TU)
				mUnloadingList = Parser.parseUnloadingByTUResult(jsonObject);
			else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_HU)
				mUnloadingList = Parser.parseUnloadingByHUResult(jsonObject);
			else
				mUnloadingList = Parser.parseUnloadingByTUResult(jsonObject);

			if(mUnloadingList.size() == 0){
				Utility.getInstance().showAlertDialog(UnloadingScreen.this, "No Tasks in Selected Delivery..!");
				return;
			}
			setDataToValidate(0);

			InboundSingleton.getInstance().unloadingHeadersTask(this, mCurrentTask);
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(UnloadingScreen.this, getString(R.string.unable_to_connect_server_please_try_again));
		}
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	int index = 0;
	private void switchButtons() {
		index = index+1;
		if(index < mValidationButtonsList.size()){
			mNextFocusedButton = mValidationButtonsList.get(index);
			mValidationButtonsList.get(index).setEnabled(true);
		}
		else{
			mNextFocusedButton = null;
			callConfirmTaskService();
		}
		changeValidationCustomButtonFocus();
	}
	
	@SuppressLint("NewApi")
	public void changeValidationCustomButtonFocus(){
		if(mCurrentFocusedButton != null){
			mCurrentFocusedButton.setBackground(mButtonDrawable);
			mCurrentFocusedButton.setAsVerified();
			mCurrentFocusedButton.clearAnimation();
		}

		if(mNextFocusedButton != null){
			mButtonDrawable = mNextFocusedButton.getBackground();
			mNextFocusedButton.setBackgroundResource(R.drawable.header);
			mNextFocusedButton.startAnimation(Utility.getInstance().blinkAnimation());
			mCurrentFocusedButton = mNextFocusedButton;
			mCurrentFocusedButton.removeVerificationForBlink();
			mTextViewManual.setText("Enter "+ mCurrentFocusedButton.getTitle() +" Manually");
		}
		else{
			mCurrentFocusedButton = null;
		}
	}

	private void callConfirmTaskService(){
		try {
			InboundSingleton.getInstance().closeUnloadingTask(this, UnloadingScreen.class.getMethod("onTaskConfirmed", 
					ConfirmResponse.class), mCurrentTask);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public void onTaskConfirmed(ConfirmResponse confirmResponse){
		int respCode = confirmResponse.getResponseCode();
		if(respCode >= 200 && respCode < 300){
			if(taskCount < mUnloadingList.size()){
				mCurrentFocusedButton.setBackground(mButtonDrawable);
				mCurrentFocusedButton.setAsVerified();
				mCurrentFocusedButton.clearAnimation();
				mCurrentFocusedButton = null;
				mNextFocusedButton = mButton4;
				changeValidationCustomButtonFocus();

				mButton4.setAsNotVerified();
				setDataToValidate(taskCount);
			}
			else{
				Utility.getInstance().showAlertDialog(this, "Task Confirmed Successfully.!");
			}
		}
		else{
			Util.showMessageDialog(this, confirmResponse.getResponseCode() +" : " + confirmResponse.getResponseLine());
		}
	}

	List<CustomButton> mValidationButtonsList = new ArrayList<CustomButton>();
	private void setDataToValidate(int taskNumber) {

		actionBar.changeHeader("Task "+ (taskNumber+1) +" of " + mUnloadingList.size());

		if(taskNumber != 0){
			linearLayoutParent.clearAnimation();
			linearLayoutParent.setAnimation(animation1);
			linearLayoutParent.startAnimation(animation1);
		}

		Unloading unloading = mUnloadingList.get(taskNumber);
		mCurrentTask = unloading;

		if(mParentID == MenuDataSorce.INBOUND_UNLOADING_DELIVERY)
			mButton1.setTitleAndText("DeliveryNumber",  Util.removeLeadingZeros(unloading.getDeliveryNumber()));
		else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_TU)
			mButton1.setTitleAndText("TU", Util.removeLeadingZeros(unloading.getTU()));
		else if(mParentID == MenuDataSorce.INBOUND_UNLOADING_HU)
			mButton1.setVisibility(View.GONE);
		else
			mButton1.setTitleAndText("ASN", Util.removeLeadingZeros(unloading.getASN()));

		mButton2.setTitleAndText("No.of HUs", unloading.getTotalHUCount());

		mButton3.setTitleAndText("Vendor", unloading.getVendor());

		mButton4.setTitleAndText("HU", Util.removeLeadingZeros(unloading.getHU()));
		mButton4.setTag(unloading.getHU());
		if(!unloading.getHU().isEmpty())
			mValidationButtonsList.add(mButton4);

		mButton5.setTitleAndText("Processed HUs", taskNumber +"/"+unloading.getTotalHUCount());

		taskCount++;

		mCurrentFocusedButton = null;
		mNextFocusedButton = mButton4;
		changeValidationCustomButtonFocus();
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		if(isDialog == 0){
			switchToValidationView(scanValue);
		}
		else{
			if(mCurrentFocusedButton.getTag().toString().equalsIgnoreCase(scanValue) || 
					(mCurrentFocusedButton.getTag().toString().isEmpty()))
				switchButtons();
			else{
				mCurrentFocusedButton.setAsWrongVerified();
			}
			StaticScanner.getInstance().scannerRead(this, 1);
		}
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);	
	}
}