package com.marlabs.whpicker.inbound;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.inbound.services.InboundBussinessClass;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.models.InboundReceiveHU;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.MenuDataSorce;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class ReceiveHUScreen extends Activity  {

	private CustomActionBar actionBar;
	private CustomButton mButton1, mButton2, mButton3, mButton4;
	private Button mButtonEnter;
	private List<InboundReceiveHU> mInboundReceiveHUsList;
	private int parentID = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			if(bundle.get(AppConstants.SELECTED_MENU) != null){
				HomeMenuItem homeMenuItem = (HomeMenuItem) bundle.get(AppConstants.SELECTED_MENU);
				actionBar.changeHeader(homeMenuItem.getItemName());
				parentID = Integer.parseInt(homeMenuItem.getItemId());
			}
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		Intent intent = new Intent(this, ReceiveHUValidations.class);
		intent.putExtra("key", 1);
		startActivityForResult(intent, 111);
	}

	private void init(String inputValue) {
		setContentView(R.layout.activity_custom_validations);
		mButton1 = (CustomButton) findViewById(R.id.validation_button1);
		mButton2 = (CustomButton) findViewById(R.id.validation_button2);
		mButton3 = (CustomButton) findViewById(R.id.validation_button3);
		mButton4 = (CustomButton) findViewById(R.id.validation_button4);
		findViewById(R.id.validation_button5).setVisibility(View.GONE);
		mButtonEnter = (Button) findViewById(R.id.bottomlayout_button_scan);
		mButtonEnter.setText("Enter");
		findViewById(R.id.bottomlayout_textview_manual).setVisibility(View.GONE);
		Button hUListButton = (Button) findViewById(R.id.bottomlayout_button_list);
		hUListButton.setVisibility(View.VISIBLE);
		hUListButton.setText("HU List");

		mButton1.removeVerification();
		mButton2.removeVerification();
		mButton3.removeVerification();
		mButton4.removeVerification();

		if(parentID == MenuDataSorce.INBOUND_RECEIVEBYHU_DELIVERY)
			InboundBussinessClass.getInstance().callReceiveHUByDeliveryVolleyRequest(this, inputValue);
		else if(parentID == MenuDataSorce.INBOUND_RECEIVEBYHU_TU)
			InboundBussinessClass.getInstance().callReceiveHUByTUVolleyRequest(this, inputValue);
		else if(parentID == MenuDataSorce.INBOUND_RECEIVEBYHU_ASN)
			InboundBussinessClass.getInstance().callReceiveHUByASNVolleyRequest(this, inputValue);

		hUListButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
			}
		});

		mButtonEnter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				InboundReceiveHU inboundReceiveHU = new InboundReceiveHU();
				inboundReceiveHU.setReceiveHUsList(mInboundReceiveHUsList);
				Intent intent = new Intent(ReceiveHUScreen.this, ReceiveHUDetailsScreen.class);
				intent.putExtra(AppConstants.PARENTID, parentID);
				intent.putExtra(AppConstants.TASK_DETAILS, inboundReceiveHU);
				startActivity(intent);
			}
		});
	}

	public void onDocumentIDResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			System.out.println("------ *documentID response * -----" + jsonObject.toString());
			String documnetID = Parser.parseReceiveByHUDocimentID(jsonObject);
			if(!Validation.isNullString(documnetID)){
				InboundBussinessClass.getInstance().callReceiveHUDetailsVolleyRequest(this, documnetID);
				System.out.println(documnetID);
			}
			else{
				Utility.getInstance().showAlertDialog(this, "Invalid Document ID..!");
			}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			System.out.println("------ * response * -----" + jsonObject.toString());
			mInboundReceiveHUsList = new ArrayList<InboundReceiveHU>();
			mInboundReceiveHUsList = Parser.parseReceiveByHUTasks(jsonObject);
			if(mInboundReceiveHUsList.size() == 0){
				Utility.getInstance().showAlertDialog(this, "No Tasks in Selected Delivery.!");
			}
			else{
				InboundReceiveHU inboundReceiveHU = mInboundReceiveHUsList.get(0);
				mButton1.setTitleAndText("Partner No", inboundReceiveHU.getPartnerNumber());
				mButton2.setTitleAndText("Name", inboundReceiveHU.getPartnerName());
				mButton3.setTitleAndText("WHS Door", inboundReceiveHU.getDoor());
				mButton4.setTitleAndText("No. of HUs", mInboundReceiveHUsList.size()+"");
			}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 111) {
			if (resultCode == RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				//Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
				init(result);
			}
			else{
				finish();
			}
		} 
	}
}