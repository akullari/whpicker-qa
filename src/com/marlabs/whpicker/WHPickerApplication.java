package com.marlabs.whpicker;

import android.app.Application;
import android.content.Context;

import com.marlabs.whpicker.scanner.StaticScanner;

public class WHPickerApplication extends Application {
	 public static Context mContext;
	 
	@Override
	public void onCreate() {
		super.onCreate();
		mContext = getApplicationContext();
		StaticScanner.getInstance();
	}
	
	public static Context getAppContext(){
		return WHPickerApplication.mContext;
	}
}