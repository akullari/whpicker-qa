package com.marlabs.whpicker.outbound;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.customviews.VoiceMessage;
import com.marlabs.whpicker.models.ConfirmResponse;
import com.marlabs.whpicker.models.ConsolidationItem;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.models.SplitTask;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.outbound.services.OutboundBussinessClass;
import com.marlabs.whpicker.outbound.services.OutboundSingleton;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.HomeActivity;
import com.marlabs.whpicker.screens.KeyPadReaderScreen;
import com.marlabs.whpicker.screens.NumericReaderScreen;
import com.marlabs.whpicker.tparsers.OutboundParser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class TaskScreen extends Activity implements IScannerListener {

	private CustomButton mButtonSourceBin, mButtonProduct, mButtonQuantity, mButtonSlot, mButtonPickHU, 
	mButtonSourceHU, mCurrentFocusedButton;
	private Button mButtonScan, mParkButton;
	private TextView mTextViewManual, mTextViewWarehouse, mTextViewPSHUCount, mTextViewPSHU, mTextViewRemainingQunatity;
	private CustomActionBar actionBar;
	private int mTasksCount = 0, currentSplitIndex = 0, availableSlotForTask = 0;
	private LinearLayout linearLayoutParent, linearLayoutSplitTask;
	private Task mCurrentTask;
	private Order mParentOrder;
	private Animation animation1, animation2;
	private VoiceMessage mVoiceMessage;
	private List<Task> mOutboundTasks, parkingTasks, batchTasks, processedTasksList;
	private List<CustomButton> mValidationButtonsList;
	private boolean customStyle = false;
	private ScrollView mScrollView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (android.os.Build.VERSION.SDK_INT > 9){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}

		init();
	}

	private void init() {
		mVoiceMessage = new VoiceMessage(this);

		animation1 = AnimationUtils.loadAnimation(this, R.anim.fliptomiddle);
		animation2 = AnimationUtils.loadAnimation(this, R.anim.flipfrommiddle);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		parkingTasks = new ArrayList<Task>();
		batchTasks = new ArrayList<Task>();
		processedTasksList = new ArrayList<Task>();
		mValidationButtonsList = new ArrayList<CustomButton>();

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			mParentOrder = (Order) bundle.getSerializable(AppConstants.OUTBOUND_ORDERS);
			actionBar.changeHeader(Util.removeLeadingZeros(mParentOrder.getWHOrder()));
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		StaticScanner.getInstance().scannerRead(this, 1);
		try {
			OutboundBussinessClass.getInstance().callOutboundTasksVolleyRequest(this, mParentOrder.getWHOrder(), 
					TaskScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			//System.out.println("------ * response * -----" + jsonObject.toString());
			mOutboundTasks = OutboundParser.parseOutboundTasksResult(jsonObject);
			if(mOutboundTasks.size() == 0){
				Utility.getInstance().showAlertDialog(this, getString(R.string.no_tasks_in_selected_order));
				return;
			}
			switchViews();
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	@SuppressLint("ResourceAsColor")
	private void switchViews(){
		setContentView(R.layout.activity_task_validations);
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.taskdetails_headers);
		mButtonSourceBin = (CustomButton) findViewById(R.id.validation_button1);
		mButtonSourceHU = (CustomButton) findViewById(R.id.validation_buttonsourcehu);
		mButtonProduct = (CustomButton) findViewById(R.id.validation_button2);
		mButtonQuantity = (CustomButton) findViewById(R.id.validation_button3);
		mButtonSlot = (CustomButton) findViewById(R.id.validation_button4);
		mButtonPickHU = (CustomButton) findViewById(R.id.validation_button5);

		TextView textViewUser = (TextView) findViewById(R.id.taskdetails_textview_user); 
		mTextViewWarehouse = (TextView) findViewById(R.id.taskdetails_textview_warehouse);
		mTextViewRemainingQunatity = (TextView) findViewById(R.id.taskdetails_textview_remaainingqunatity);
		linearLayoutParent = (LinearLayout) findViewById(R.id.linearlayout_validation);
		linearLayoutSplitTask = (LinearLayout) findViewById(R.id.linearlayout_splittask);
		mScrollView = (ScrollView) findViewById(R.id.scrollview_validations);

		mTextViewManual = (TextView) findViewById(R.id.bottomlayout_textview_manual);
		mTextViewPSHUCount = (TextView) findViewById(R.id.taskdetails_pshucount);
		mTextViewPSHU = (TextView) findViewById(R.id.taskdetails_pshu);
		mButtonScan = (Button) findViewById(R.id.bottomlayout_button_scan);
		mParkButton = (Button) findViewById(R.id.bottomlayout_button_parkbutton);
		mParkButton.setVisibility(View.VISIBLE);
		mTextViewRemainingQunatity.setVisibility(View.VISIBLE);

		animation1.setAnimationListener(animationListener);
		animation2.setAnimationListener(animationListener);

		mButtonSourceBin.setOnClickListener(validationButonClickLisener);
		mButtonSourceHU.setOnClickListener(validationButonClickLisener);
		mButtonProduct.setOnClickListener(validationButonClickLisener);
		mButtonQuantity.setOnClickListener(validationButonClickLisener);
		mButtonSlot.setOnClickListener(validationButonClickLisener);
		mButtonPickHU.setOnClickListener(validationButonClickLisener);
		mParkButton.setOnClickListener(parkButonClickListener);

		linearLayout.setVisibility(View.VISIBLE);
		textViewUser.setText(Util.removeLeadingZeros(mParentOrder.getWHOrder()));
		mTextViewManual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mCurrentFocusedButton != null)
					manualAction();
			}
		});

		mButtonScan.setEnabled(false);
		mButtonScan.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				switchButtons();
			}
		});

		setDataTovalidation(mTasksCount);
	}

	OnClickListener parkButonClickListener = new OnClickListener() {
		@Override
		public void onClick(View view){
			AlertDialog confirmDialog=new AlertDialog.Builder(TaskScreen.this).create();
			confirmDialog.setMessage(getString(R.string.would_you_like_to_park_this_pshu_tasks));
			confirmDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					parkingTasks.clear();
					for(Task iteratingTask : mOutboundTasks)
					{
						if(mCurrentTask.getWareHouseTask().equalsIgnoreCase(iteratingTask.getWareHouseTask()) || 
								(!Validation.isNullString(mCurrentTask.getConsolidationGroup()) && !Validation.isNullString(iteratingTask.getConsolidationGroup()) && mCurrentTask.getConsolidationGroup().equalsIgnoreCase(iteratingTask.getConsolidationGroup())) )
						{
							if(Validation.isNullString(iteratingTask.getStatus()) || AppConstants.STATUS_CLIENTSIDE_CONFIRM_STRING.equalsIgnoreCase(iteratingTask.getStatus()) || AppConstants.STATUS_BATCH_ERROR.equalsIgnoreCase(iteratingTask.getStatus()) )
							{
								try {
									parkingTasks.add(iteratingTask);
									OutboundSingleton.getInstance().parkTask(TaskScreen.this, TaskScreen.class.getMethod("onTaskParked", ConfirmResponse.class), iteratingTask);
								} catch (NoSuchMethodException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			});

			confirmDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.no), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					//
				}
			});

			confirmDialog.show();
		}
	};

	OnClickListener validationButonClickLisener = new OnClickListener() {
		@SuppressLint("NewApi")
		@Override
		public void onClick(View view) {
			CustomButton selectedCustomButton = (CustomButton)view;
			if(mCurrentFocusedButton != null){
				if((mValidationButtonsList.indexOf(mCurrentFocusedButton)) < (mValidationButtonsList.indexOf(selectedCustomButton))){
					Toast.makeText(getApplicationContext(), "Validate "+mCurrentFocusedButton.getTitle()+" first", Toast.LENGTH_SHORT).show();
					return;
				}
				mCurrentFocusedButton.setButtonValidation(false);
				mCurrentFocusedButton.setAsNotVerified();
			}
			selectedCustomButton.setButtonValidation(false);
			switchButtons();
		}
	};

	AnimationListener animationListener = new AnimationListener() {
		@Override
		public void onAnimationStart(Animation animation) {}
		@Override
		public void onAnimationRepeat(Animation animation) {}
		@Override
		public void onAnimationEnd(Animation animation) {
			if (animation==animation1) {
				(linearLayoutParent).clearAnimation();
				(linearLayoutParent).setAnimation(animation2);
				(linearLayoutParent).startAnimation(animation2);
			} 
		}
	};

	private void manualAction(){
		if((mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.quantity)))){
			Intent intent  = new Intent(TaskScreen.this, NumericReaderScreen.class);
			intent.putExtra(AppConstants.NUMERIC_KEYPAD_ISFORRESOURCE, false);
			intent.putExtra(AppConstants.NUMERIC_KEYPAD_INPUT, mCurrentTask.getTotalQuantity());
			intent.putExtra(AppConstants.NUMERIC_KEYPAD_INPUT_UNITS, mCurrentTask.getQuantityUoM());
			intent.putExtra(AppConstants.NUMERIC_KEYPAD_SCANVALUE, mCurrentTask.getProduct_Scanned());
			intent.putExtra(AppConstants.NUMERIC_KEYPAD_REMAINING_INPUT, mCurrentTask.getSuppliedQuantity());
			intent.putExtra(AppConstants.NUMERIC_KEYPAD_ENTERED_VALUE, mCurrentTask.getSplitTasksList().get(mCurrentFocusedButton.getButtonTagValue()).getQuantity());
			startActivityForResult(intent, 1);
		}
		else{
			String input;
			if((mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.product)))){
				input = mCurrentTask.getBatch();
			}
			else{
				input = "";
			}
			Intent intent  = new Intent(TaskScreen.this, KeyPadReaderScreen.class);
			intent.putExtra(AppConstants.ALPHA_KEYPAD_TITLE, mTextViewManual.getText().toString());
			intent.putExtra(AppConstants.ALPHA_KEYPAD_BATCHINPUT, input);
			if(mCurrentFocusedButton.getTitle().contains(getString(R.string.slot_id)))
				intent.putExtra(AppConstants.ALPHA_KEYPAD_ISNUMERIC, true);
			else
				intent.putExtra(AppConstants.ALPHA_KEYPAD_ISNUMERIC, false);
			startActivityForResult(intent, 1);
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
			if (resultCode== RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				onDataScanned(result);
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, getString(R.string.manual_entry_cancelled), Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void onDataScanned(String scanResult) {
		if(mCurrentFocusedButton == null)
			return;

		if((mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.slot_id))))
		{
			mCurrentFocusedButton.setTitleAndText(getString(R.string.slot_id), scanResult);
			mCurrentTask.getSplitTasksList().get(currentSplitIndex).setSlot(scanResult);
			if(Resource.RPT_MANUAL_PARING == Resource.getSelectedResource().getResourceType() || 
					Resource.RPT_AUTO_PARING == Resource.getSelectedResource().getResourceType())
			{
				boolean isSlotValid=true;
				List<ConsolidationItem> accomodatedTasks = Resource.getSelectedResource().getAccomadatedTasks();
				for(ConsolidationItem iteratingItem:accomodatedTasks)
				{
					if(!Validation.isNullString(scanResult) && !Validation.isNullString(iteratingItem.getSlot()) 
							&& iteratingItem.getSlot().equalsIgnoreCase(scanResult) 
							&& !iteratingItem.getConsolidationGroup().equalsIgnoreCase(mCurrentTask.getConsolidationGroup()))
					{
						isSlotValid=false;
						Util.showMessageDialog(TaskScreen.this, "This sliot alredy occupied by Consolidation group "+ 
								iteratingItem.getConsolidationGroup());
						break;
					}
				}
				mCurrentFocusedButton.setButtonValidation(isSlotValid);
				if(isSlotValid){
					mCurrentFocusedButton.setAsVerified();
				}
				else
					return;
			}
			else{
				mCurrentFocusedButton.setAsVerified();
			}
			switchButtons();
		}

		//Source Handling
		else if((mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.source_hu)))){
			mCurrentFocusedButton.setTitleAndText(getString(R.string.source_hu), scanResult);
			mCurrentTask.setSourceHU(scanResult);
			mCurrentFocusedButton.setButtonValidation(true);
			mCurrentFocusedButton.setAsVerified();
			switchButtons();
		}

		//Product button handling
		else if((mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.product)))){
			mCurrentTask.setProduct_Scanned(scanResult);
			if(Validation.isNullString(scanResult)){
				mCurrentFocusedButton.setAsWrongVerified();
				mCurrentFocusedButton.setButtonValidation(true);
			}

			Boolean isProductValid=false;
			if(!Validation.isNullString(mCurrentTask.getBatch()))
				isProductValid=scanResult.startsWith(mCurrentTask.getProduct()) && scanResult.endsWith(mCurrentTask.getBatch());
			if(!isProductValid)
				isProductValid=mCurrentTask.getBatch().endsWith(scanResult);
			if(!isProductValid)
				isProductValid=mCurrentTask.getProduct().endsWith(scanResult);

			if(isProductValid){
				mCurrentFocusedButton.setAsVerified();
				mCurrentFocusedButton.setButtonValidation(true);

				if(mCurrentTask.getSuppliedQuantity()==0)
				{
					if(mCurrentTask.getSplitTasksList().size()>0)
					{
						mCurrentTask.getSplitTasksList().get(0).setQuantity(1);
					}
				}
				switchButtons();
			}
			else{
				mCurrentFocusedButton.setAsWrongVerified();
				mCurrentFocusedButton.setButtonValidation(true);
				mVoiceMessage.speak(getString(R.string.wrong)+ " " + mCurrentFocusedButton.getTitle() + ", "+ getString(R.string.please_repeat));
			}
		}

		else if((mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.pick_hu)))){
			boolean isToatValid=true;
			//Validate the Tote

			List<ConsolidationItem> accomodatedTasks = Resource.getSelectedResource().getAccomadatedTasks();
			//Check 1:1 mapping between tote and Slot
			//			for(ConsolidationItem iteratingItem : accomodatedTasks)
			//			{
			//				if(isToatValid)
			//				{
			//					String iteratingTote=iteratingItem.getTote();
			//					String iteratingSlot=iteratingItem.getSlot();
			//					if(!Validation.isNullString(iteratingTote) && !Validation.isNullString(iteratingSlot))
			//					{
			//						for(ConsolidationItem subItem:accomodatedTasks)
			//						{
			//							if(isToatValid)
			//							{
			//								String subTote=subItem.getTote();
			//								String subSlot=subItem.getSlot();
			//								if(iteratingTote.equalsIgnoreCase(subTote) && !iteratingSlot.equalsIgnoreCase(subSlot))
			//								{
			//									Util.showMessageDialog(this, getString(R.string.please_select_different_slot_for_this_pick_hu));
			//									isToatValid = false;
			//									break;
			//								}
			//								if(iteratingSlot.equalsIgnoreCase(subSlot) && !iteratingTote.equalsIgnoreCase(subTote))
			//								{
			//									Util.showMessageDialog(this, getString(R.string.please_select_different_slot_for_this_pick_hu));
			//									isToatValid = false;
			//									break;
			//								}
			//							}
			//						}
			//
			//					}
			//				}
			//			}
			//
			//			if(!isToatValid)
			//				return;

			String errorMessage=null;
			for(ConsolidationItem iteratingTask:accomodatedTasks){
				if(!mCurrentTask.equals(iteratingTask))
				{
					if(scanResult.equalsIgnoreCase(iteratingTask.getTote()) && 
							(Validation.isNullString(mCurrentTask.getConsolidationGroup()) || 
									Validation.isNullString(iteratingTask.getConsolidationGroup()) || 
									!(mCurrentTask.getConsolidationGroup().equalsIgnoreCase(iteratingTask.getConsolidationGroup()) )))  
					{
						mCurrentTask.getSplitTasksList().get(currentSplitIndex).setTote("");
						mCurrentTask.getSplitTasksList().get(currentSplitIndex).setSlot("");
						errorMessage = getString(R.string.this_pick_hu_is_used_by_task)+" "+ iteratingTask.getConsolidationGroup();
						isToatValid=false;
						mCurrentFocusedButton.setAsWrongVerified();
						mCurrentFocusedButton.setButtonValidation(true);
					}

					if(!Validation.isNullString(mCurrentTask.getSplitTasksList().get(0).getSlot()) &&
							! Validation.isNullString(iteratingTask.getSlot()) &&
							mCurrentTask.getSplitTasksList().get(0).getSlot().equalsIgnoreCase(iteratingTask.getSlot()) &&
							! Validation.isNullString(scanResult) && //mCurrentTask.getPickHU()
							! Validation.isNullString(iteratingTask.getTote()) &&
							! scanResult.equalsIgnoreCase(iteratingTask.getTote()))
					{
						mCurrentTask.getSplitTasksList().get(0).setSlot("");
						errorMessage = getString(R.string.please_select_different_slot_for_this_pick_hu);
						isToatValid=false;
					}
				}
			}

			if(Validation.isNullString(errorMessage)){
				mCurrentFocusedButton.setTitleAndText(getString(R.string.pick_hu), scanResult);
				mCurrentTask.getSplitTasksList().get(currentSplitIndex).setTote(scanResult);
			}
			else{
				Util.showMessageDialog(this, errorMessage);
				mCurrentFocusedButton.setTitleAndText(getString(R.string.pick_hu), "");
			}

			if(isToatValid){
				mCurrentFocusedButton.setAsVerified();
				mCurrentFocusedButton.setButtonValidation(true);
				if(Validation.isNullString(mCurrentTask.getPickHU_Suggested()) || 
						!(mCurrentTask.getPickHU_Suggested().equalsIgnoreCase(mCurrentTask.getSplitTasksList().get(currentSplitIndex).getTote())))
				{
					switchButtons();

				}
				else{
					mCurrentTask.getSplitTasksList().get(currentSplitIndex).setTote(scanResult);
					switchButtons();
				}
			}
		}

		//Qunatity button handling
		else if((mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.quantity)))){ 
			currentSplitIndex = mCurrentFocusedButton.getButtonTagValue();
			if(!Validation.isNullString(scanResult))
				mCurrentTask.getSplitTasksList().get(currentSplitIndex).setQuantity(Integer.parseInt(scanResult));

			SplitTask lastSplitItem=mCurrentTask.getSplitTasksList().get(mCurrentTask.getSplitTasksList().size()-1);
			boolean isLastIndexQuantityEntered=!(lastSplitItem.getQuantity() == 0);
			if(mCurrentTask.getRemainingQuantity()>0 && isLastIndexQuantityEntered)
			{
				showSplitConfirmationDialog(scanResult);
				return;
			}
			//mCurrentTask.getSplitTasksList().get(currentSplitIndex).setQuantity(result);
			mTextViewRemainingQunatity.setText(getString(R.string.remaining_quanity) + " : " + (mCurrentTask.getRemainingQuantity())+" "+mCurrentTask.getQuantityUoM());
			mCurrentFocusedButton.setTitleAndText(getString(R.string.quantity), scanResult+ " "+ mCurrentTask.getQuantityUoM());
			mCurrentFocusedButton.setAsVerified();
			mCurrentFocusedButton.setButtonValidation(true);
			switchButtons();
		}
		else if(mCurrentFocusedButton.getTag().toString().equalsIgnoreCase(scanResult)){
			mCurrentFocusedButton.setAsVerified();
			mCurrentFocusedButton.setButtonValidation(true);
			switchButtons();
		}
		else{
			mCurrentFocusedButton.setAsWrongVerified();
			mCurrentFocusedButton.setButtonValidation(true);
			mVoiceMessage.speak(getString(R.string.wrong) + " " + mCurrentFocusedButton.getTitle() + ", " + getString(R.string.please_repeat));
		}

		if(mCurrentFocusedButton != null && 
				(mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.quantity)))){ //Quantity
			this.manualAction();
		}
		else{
			StaticScanner.getInstance().scannerRead(this, 1);
		}
	}

	public void switchButtons(){
		if(mCurrentFocusedButton != null){
			Util.clearAnimation(mCurrentFocusedButton);
		}

		mCurrentFocusedButton = null;
		for(CustomButton customButton : mValidationButtonsList){
			if(!customButton.isButtonValidated()){
				mCurrentFocusedButton = customButton;
				break;
			}
		}

		if(mCurrentFocusedButton != null){
			Util.setAnimation(mCurrentFocusedButton);
			mTextViewManual.setText((getString(R.string.enter)) + " " + mCurrentFocusedButton.getTitle() +" " + getString(R.string.manually));
			if(mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.product))){
				scrollBottom();
			}
			if(mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.quantity))){
				if(mCurrentTask.getRemainingQuantity() > 1)
					mVoiceMessage.speak(getString(R.string.now_enter) + " " + mCurrentFocusedButton.getTitle());
			}
			else if(mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.pick_hu))){
				if(!Validation.isNullString(mCurrentTask.getSplitTasksList().get(currentSplitIndex).getSlot())){
					//					List<String> totesOfSameConsolidations = new ArrayList<String>();
					//					for(Task tasktemp : mOutboundTasks){
					//						if(mCurrentTask.getConsolidationGroup().equalsIgnoreCase(tasktemp.getConsolidationGroup())){
					//							for(SplitTask splitTask : tasktemp.getSplitTasksList()){
					//								totesOfSameConsolidations.add(splitTask.getSlot());
					//							}
					//						}
					//					}
					//					if(totesOfSameConsolidations.size() > 1)
					//						showSlotsDialog(totesOfSameConsolidations);
					mVoiceMessage.speak(getString(R.string.go_to_slot)+" "+ mCurrentTask.getSplitTasksList().get(currentSplitIndex).getSlot() +" "+getString(R.string.and_scan_pick_hu));
				}
				else{
					mVoiceMessage.speak(getString(R.string.now_scan_pick_hu));
				}
			}
			else{
				mVoiceMessage.speak(getString(R.string.now_scan) + " " + mCurrentFocusedButton.getTitle());
			}
		}
		else{
			mButtonScan.setEnabled(true);
			mButtonScan.setBackgroundColor(getResources().getColor(R.color.green));
			if(mTasksCount < mOutboundTasks.size())
				mButtonScan.setText("Go to Next Task");
			else
				mButtonScan.setText("Go to Order Screen");
			batchTasks.clear();
			batchTasks.add(mCurrentTask);
			if(mCurrentTask.getSplitTasksList().size() == 0){
				Util.showMessageDialog(this, "Task Count is zero. Unable to Validate");
			}

			if(mCurrentTask.getSplitTasksList().size() == 1){
				callConfirmTaskService(batchTasks);
			}
			else{
				try {
					OutboundSingleton.getInstance().callBatchUpdate(mCurrentTask.getConsolidationGroup(),this, 
							TaskScreen.class.getMethod("onBatchUpdated", ConfirmResponse.class), batchTasks);
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		}
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	String itemsArray[];
	private void showSlotsDialog(List<String> items) {
		itemsArray = new String[items.size()];
		itemsArray = items.toArray(itemsArray);
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle("What do you Like ?");
		ad.setSingleChoiceItems(itemsArray, 0,  new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				Toast.makeText(getApplicationContext(), "You Choose : " + itemsArray[arg1], Toast.LENGTH_LONG).show();
			}
		});
		ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Toast.makeText(getApplicationContext(), "You Have Cancel the Dialog box", Toast.LENGTH_LONG).show();
			}
		});
	}

	private void callConfirmTaskService(List<Task> tasksList){
		try {
			OutboundSingleton.getInstance().ConfirmTask(this,TaskScreen.class.getMethod("onTaskConfirmed", ConfirmResponse.class), tasksList);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}		
	}

	private void currentTaskActionSuccessfully() {
		processedTasksList.add(mCurrentTask);
		//Determine next open task
		mTasksCount=-1;
		for(int taskIteratorIndex=0;taskIteratorIndex<mOutboundTasks.size(); taskIteratorIndex++)
		{
			Task iteratingTask=mOutboundTasks.get(taskIteratorIndex);
			//if(AppConstants.EMPTY_STRING==iteratingTask.getStatus() || AppConstants.STATUS_BATCH_ERROR == iteratingTask.getStatus()) //or add batch error
			if(Validation.isNullString(iteratingTask.getStatus()) || 
					AppConstants.EMPTY_STRING.equalsIgnoreCase(iteratingTask.getStatus()) || 
					AppConstants.STATUS_BATCH_ERROR.equalsIgnoreCase(iteratingTask.getStatus())) { //or add batch error
				mTasksCount = taskIteratorIndex;
				break;
			}
		}
		if(mTasksCount>=0){
			if(!Validation.isNullString(mCurrentTask.getErrorMessage())){
				Util.showMessageDialog(this, mCurrentTask.getErrorMessage());
				//switchButtons();
			}
			else{
				setDataTovalidation(mTasksCount);
				//switchButtons();
				mCurrentTask.setErrorMessage("");
			}
		}
		else{
			mParentOrder.setTasksList(processedTasksList);
			Intent intent = null;
			if(Validation.isNullString(HomeActivity.sSelectedQueue)){
				intent = new Intent(TaskScreen.this, ResourcesScreen.class);
			}
			else{
				intent = new Intent(TaskScreen.this, OrdersScreen.class);
			}
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
	}

	public void onTaskParked(ConfirmResponse confirmResponse){
		int respCode = confirmResponse.getResponseCode();
		Task matchingTask = null;
		for(Task iteratingTask:mOutboundTasks){
			if(iteratingTask.getWareHouseTask().equalsIgnoreCase(confirmResponse.getStrTag())){
				matchingTask=iteratingTask;
			}
		}

		if(confirmResponse==null || (respCode >= 200 && respCode < 300)){
			matchingTask.setValidTask(false);
			matchingTask.setStatus(AppConstants.STATUS_WAITING_STRING);
			Resource.getSelectedResource().addTask(matchingTask);
		}
		else{
			matchingTask.setValidTask(false);
			matchingTask.setErrorMessage(confirmResponse.getResponseLine());
			Util.showMessageDialog(this, confirmResponse.getResponseLine());
		}

		parkingTasks.remove(matchingTask);
		if(0==parkingTasks.size())
			currentTaskActionSuccessfully();
	}

	public void onTaskConfirmed(ConfirmResponse confirmResponse){
		int respCode = confirmResponse==null?0:confirmResponse.getResponseCode();
		Task task = mCurrentTask;
		if(respCode >= 200 && respCode < 300){
			processedTasksList.add(task);
			task.setValidTask(true);
			//if(confirmResponse!=null)
			task.setStatus(AppConstants.STATUS_CONFIRM_STRING);
			Resource.getSelectedResource().addTask(mCurrentTask);
			//Utility.getInstance().showAlertDialog(this, "Task Confirmed Successfully.!");
			currentTaskActionSuccessfully();
		}
		else{
			task.setValidTask(false);
			//task.setErrorMessage(confirmResponse.getResponseLine());
			Util.showMessageDialog(this, confirmResponse.getResponseLine());
		}
	}

	public void onBatchUpdated(ConfirmResponse confirmResponse){
		if(confirmResponse.getBatchResponses() == null || confirmResponse.getBatchResponses().size() == 0){
			Util.showMessageDialog(this, confirmResponse.getResponseLine());
			return;
		}

		List<ConfirmResponse> confirmResponseList = confirmResponse.getBatchResponses();
		int j = 0;
		for(Task task : batchTasks){
			for(SplitTask splitTask : task.getSplitTasksList()){
				System.out.println(splitTask.getQuantity());
				System.out.println("Index ------ " + j);
				System.out.println("confirmResponseList ------ " + confirmResponseList.size());
				if(j < confirmResponseList.size()){
					ConfirmResponse response  =  confirmResponseList.get(j);
					if(response.isBatchError()){
						task.setValidTask(false);
						task.setErrorMessage(response.getBatchErrorMessage());
						task.setStatus(AppConstants.STATUS_BATCH_ERROR);
						//isActionFailed=true;
						//Util.showMessageDialog(this, response.getBatchErrorMessage());
					}
					else{
						task.setValidTask(true);
						task.setStatus(AppConstants.STATUS_CONFIRM_STRING);
					}
				}
				j++;
			}
			Resource.getSelectedResource().addTask(task);
			processedTasksList.add(task);
		}
		//if(!isActionFailed)
		currentTaskActionSuccessfully();
	}

	private void setDataTovalidation(int taskNumber) {
		currentSplitIndex = 0;
		availableSlotForTask = Resource.getSelectedResource().getLastOccupiedSlotNumber();
		actionBar.changeHeader((getString(R.string.task))+ " " + (taskNumber+1) +" "+(getString(R.string.of))+" " + mOutboundTasks.size());
		Task task = mOutboundTasks.get(taskNumber);
		mCurrentTask = task;
		mTextViewWarehouse.setText(Util.removeLeadingZeros(mCurrentTask.getWareHouseTask()));

		//PSHU task number
		if(!Validation.isNullString(mCurrentTask.getConsolidationGroup())){
			int totalPSHUTasksCount=0;
			int processedPSHUTasksCount=0;
			for(int index=0;index<mOutboundTasks.size(); index++){
				Task iteratingTask=mOutboundTasks.get(index);
				if(mCurrentTask.getConsolidationGroup().equalsIgnoreCase(iteratingTask.getConsolidationGroup())){
					totalPSHUTasksCount++;
					if(AppConstants.STATUS_CLIENTSIDE_CONFIRM_STRING.equals(iteratingTask.getStatus()) || AppConstants.STATUS_CONFIRM_STRING.equals(iteratingTask.getStatus()) || AppConstants.STATUS_WAITING_STRING.equals(iteratingTask.getStatus()))
						processedPSHUTasksCount++;
				}
			}
			mTextViewPSHU.setText(mCurrentTask.getConsolidationGroup());
			mTextViewPSHUCount.setText((processedPSHUTasksCount+1)+ " "+ getString(R.string.of)+" " + totalPSHUTasksCount);
		}
		else{
			mTextViewPSHU.setText(mCurrentTask.getConsolidationGroup());
			mTextViewPSHUCount.setText(" 1 of 1");
		}

		//Set pick hu, if consolidation group is same
		if(!Validation.isNullString(mCurrentTask.getConsolidationGroup()) && Validation.isNullString(mCurrentTask.getSplitTasksList().get(currentSplitIndex).getTote()))
		{
			for(int index=taskNumber-1;index>=0;index--)
			{
				Task iteratingTask=mOutboundTasks.get(index);
				if(iteratingTask.getConsolidationGroup().equalsIgnoreCase(mCurrentTask.getConsolidationGroup()) && 
						!Validation.isNullString(iteratingTask.getSplitTasksList().get(currentSplitIndex).getTote()))
				{
					mCurrentTask.setPickHU_Suggested(iteratingTask.getSplitTasksList().get(currentSplitIndex).getTote());
					mCurrentTask.getSplitTasksList().get(currentSplitIndex).setSlot(iteratingTask.getSplitTasksList().get(currentSplitIndex).getSlot());
					break;
				}
			}
		}

		//Set the slot number, if the resource paring type is automatic
		if(Resource.RPT_AUTO_PARING==Resource.getSelectedResource().getResourceType() && 
				Validation.isNullString(mCurrentTask.getSplitTasksList().get(currentSplitIndex).getSlot())){
			mCurrentTask.getSplitTasksList().get(0).setSlot((availableSlotForTask+1) + "");
		}

		if(taskNumber != 0){
			linearLayoutParent.clearAnimation();
			linearLayoutParent.setAnimation(animation1);
			linearLayoutParent.startAnimation(animation1);
		}

		mScrollView.fullScroll(View.FOCUS_UP);
		mButtonSourceBin.setAsVerified();
		mButtonProduct.setAsNotVerified();
		mButtonSourceHU.setAsNotVerified();
		mButtonQuantity.setAsNotVerified();
		mButtonSlot.setAsNotVerified();
		mButtonPickHU.setAsNotVerified();

		mButtonQuantity.removeIndex();
		mButtonSlot.removeIndex();
		mButtonPickHU.removeIndex();

		mButtonScan.setEnabled(false);
		mButtonScan.setBackgroundColor(getResources().getColor(R.color.red));
		mButtonScan.setText(getString(R.string.scan));

		linearLayoutSplitTask.removeAllViews();

		mButtonSourceBin.setTitleAndText(getString(R.string.source_bin), task.getSourceBin());
		mButtonSourceBin.setTag(task.getSourceBin());
		if(!task.getSourceBin().isEmpty()){
			mButtonSourceBin.setButtonValidation(false);
			mValidationButtonsList.add(mButtonSourceBin);
		}

		mButtonSourceHU.setTitleAndText(getString(R.string.source_hu), "");
		mButtonSourceHU.setButtonValidation(true);
		mValidationButtonsList.add(mButtonSourceHU);

		mButtonProduct.setTitleAndText(getString(R.string.product), task.getProduct());
		mButtonProduct.setTag(task.getBatch());
		if(!task.getProduct().isEmpty()){
			mButtonProduct.setButtonValidation(false);
			mValidationButtonsList.add(mButtonProduct);
		}

		mTextViewRemainingQunatity.setText(getString(R.string.remaining_quanity)+" : " + task.getTotalQuantity()+" "+task.getQuantityUoM());
		mButtonQuantity.setTitleAndText(getString(R.string.quantity), task.getTotalQuantity()+" "+task.getQuantityUoM());
		mButtonQuantity.setTag(task.getTotalQuantity());
		mButtonQuantity.setButtonTagValue(0);
		//if(!task.getTotalQuantity().isEmpty()){
		mButtonQuantity.setButtonValidation(false);
		mValidationButtonsList.add(mButtonQuantity);
		//}

		mButtonSlot.setButtonTagValue(0);
		if(Resource.RPT_NO_PARING == Resource.getSelectedResource().getResourceType()){
			mButtonSlot.setVisibility(View.GONE);
			mButtonPickHU.setBackgroundColor(this.getResources().getColor(R.color.grey_dark));
		}
		else if(Resource.RPT_AUTO_PARING == Resource.getSelectedResource().getResourceType()){
			mButtonSlot.setTitleAndText(getString(R.string.slot_id), 
					mCurrentTask.getSplitTasksList().get(currentSplitIndex).getSlot());
			mButtonSlot.setButtonValidation(true);
			mValidationButtonsList.add(mButtonSlot);
		}
		else{
			String slot = mCurrentTask.getSplitTasksList().get(currentSplitIndex).getSlot();
			mButtonSlot.setTitleAndText(getString(R.string.slot_id), slot==null?"":slot);
			mButtonSlot.setButtonValidation(false);
			mValidationButtonsList.add(mButtonSlot);
		}

		mButtonPickHU.setTitleAndText(getString(R.string.pick_hu), mCurrentTask.getPickHU_Suggested());
		mButtonPickHU.setTag(task.getPickHU_Suggested());
		mButtonPickHU.setButtonValidation(false);
		mButtonPickHU.setButtonTagValue(0);
		mValidationButtonsList.add(mButtonPickHU);

		mCurrentFocusedButton = null;
		switchButtons();
		mTasksCount++;
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		onDataScanned(scanValue);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;

		case R.id.action_settings_item:
			Utility.getInstance().showSettingsDialog(TaskScreen.this);
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mVoiceMessage.destroySpeechEngine();
	}

	public void showSplitConfirmationDialog(final String result){
		new AlertDialog.Builder(TaskScreen.this).setTitle(getString(R.string.partial_quantity))
		.setMessage(getString(R.string.do_you_want_to_split))
		.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				//if(mCurrentTask.getSplitTasksList().size() <= 1)
				{
					SplitTask splitTask = new SplitTask();
					int splitedTasksCount=mCurrentTask.getSplitTasksList().size();
					splitedTasksCount++;
					if(Resource.RPT_AUTO_PARING==Resource.getSelectedResource().getResourceType()){
						splitTask.setSlot((availableSlotForTask+splitedTasksCount)+"");
					}
					mCurrentTask.getSplitTasksList().add(splitTask);

					//mCurrentTask.getSplitTasksList().get(currentSplitIndex).setQuantity(result);
					mTextViewRemainingQunatity.setText(getString(R.string.remaining_quanity)+" : " + (mCurrentTask.getRemainingQuantity())+" "+mCurrentTask.getQuantityUoM());
					mCurrentFocusedButton.setTitleAndText(getString(R.string.quantity), result+ " "+ mCurrentTask.getQuantityUoM());
					mCurrentFocusedButton.setAsVerified();
					mCurrentFocusedButton.setButtonValidation(true);
					switchButtons();
					createSplitTasksUIForValidation();
				}
			}
		})
		.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				manualAction();
			}
		}).show();
	}

	private void createSplitTasksUIForValidation() {
		if(currentSplitIndex == 0){
			mButtonQuantity.setIndex(1);
			mButtonSlot.setIndex(1);
			mButtonPickHU.setIndex(1);
		}

		int index = currentSplitIndex + 2;
		CustomButton customButtonQuantity = new CustomButton(this);
		customButtonQuantity.setTitleAndText(getString(R.string.quantity), mCurrentTask.getRemainingQuantity()+" "+ mCurrentTask.getQuantityUoM());
		customButtonQuantity.setIndex(index);
		customButtonQuantity.setOnClickListener(validationButonClickLisener);
		customButtonQuantity.setButtonTagValue(mCurrentTask.getSplitTasksList().size()-1);

		CustomButton customButtonSlot = new CustomButton(this);
		customButtonSlot.setOnClickListener(validationButonClickLisener);
		customButtonSlot.setButtonTagValue(mCurrentTask.getSplitTasksList().size()-1);
		customButtonSlot.setIndex(index);
		customButtonSlot.setTitleAndText(getString(R.string.slot_id), 
				mCurrentTask.getSplitTasksList().get(currentSplitIndex+1).getSlot());

		CustomButton customButtonPickHU = new CustomButton(this);
		customButtonPickHU.setIndex(index);
		customButtonPickHU.setTitleAndText(getString(R.string.pick_hu), "");
		customButtonPickHU.setButtonTagValue(mCurrentTask.getSplitTasksList().size()-1);

		if(customStyle){
			customButtonQuantity.setLightBackgroundStyle(customButtonQuantity);
			customButtonSlot.setDarkBackgroundStyle(customButtonSlot);
			customButtonPickHU.setLightBackgroundStyle(customButtonPickHU);
			customStyle = false;
		}
		else{
			customButtonQuantity.setDarkBackgroundStyle(customButtonQuantity);
			customButtonSlot.setLightBackgroundStyle(customButtonSlot);
			customButtonPickHU.setDarkBackgroundStyle(customButtonPickHU);
			customStyle = true;
		}

		linearLayoutSplitTask.addView(customButtonQuantity);
		mValidationButtonsList.add(customButtonQuantity);
		customButtonQuantity.setButtonValidation(false);

		if(Resource.RPT_NO_PARING == Resource.getSelectedResource().getResourceType()){
			if(customStyle)
				customButtonPickHU.setLightBackgroundStyle(customButtonPickHU);
			else
				customButtonPickHU.setDarkBackgroundStyle(customButtonPickHU);
		}
		else if(Resource.RPT_AUTO_PARING == Resource.getSelectedResource().getResourceType()){
			linearLayoutSplitTask.addView(customButtonSlot);
			mValidationButtonsList.add(customButtonSlot);
			customButtonSlot.setButtonValidation(true);
		}
		else{
			linearLayoutSplitTask.addView(customButtonSlot);
			mValidationButtonsList.add(customButtonSlot);
			customButtonSlot.setButtonValidation(false);
		}

		linearLayoutSplitTask.addView(customButtonPickHU);
		mValidationButtonsList.add(customButtonPickHU);
		customButtonPickHU.setButtonValidation(false);

		scrollBottom();
		switchButtons();

		if((mCurrentFocusedButton.getTitle().equalsIgnoreCase(getString(R.string.quantity)))){ //Quantity
			this.manualAction();
		}
		else{
			StaticScanner.getInstance().scannerRead(this, 1);
		}
	}

	private void scrollBottom(){
		ValueAnimator realSmoothScrollAnimation = ValueAnimator.ofInt(0, mScrollView.getBottom());
		realSmoothScrollAnimation.setDuration(500);
		realSmoothScrollAnimation.addUpdateListener(new AnimatorUpdateListener(){
			@Override
			public void onAnimationUpdate(ValueAnimator animation){
				int scrollTo = (Integer) animation.getAnimatedValue();
				mScrollView.scrollTo(0, scrollTo);
			}
		});
		realSmoothScrollAnimation.start();
	}
}