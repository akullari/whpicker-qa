package com.marlabs.whpicker.outbound;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.models.ConfirmResponse;
import com.marlabs.whpicker.models.ErrorDetails;
import com.marlabs.whpicker.models.ErrorResponse;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.outbound.services.OutboundBussinessClass;
import com.marlabs.whpicker.outbound.services.OutboundSingleton;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.KeyPadReaderScreen;
import com.marlabs.whpicker.screens.LoginWebviewScreen;
import com.marlabs.whpicker.tparsers.OutboundParser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class ConsolidationScreen extends Activity implements IScannerListener {
	private LinearLayout mLinearLayout;
	private Button mButtonReleaseResource;
	private boolean isLogoutCalled = false;
	private TextView mManualTextView;
	private List<Task> packingTasks;
	private CustomButton mCurrentFocusedButton = null;
	private Drawable mButtonDrawable;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_consolidation);
		if (android.os.Build.VERSION.SDK_INT > 9){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}

		isLogoutCalled = false;
		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			isLogoutCalled = true;
		}

		CustomActionBar actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.packing_station));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		mLinearLayout  = (LinearLayout) findViewById(R.id.linearlayout_consolidation);
		mButtonReleaseResource = (Button) findViewById(R.id.button_releaseresource);
		mManualTextView = (TextView) findViewById(R.id.textview_consolidation_manual);
		TextView textViewresourceName = (TextView) findViewById(R.id.textview_consolidation_resourcename);

		textViewresourceName.setText(getString(R.string.resource)+": " + Resource.getSelectedResource().getResourceName());
		mManualTextView.setText(getString(R.string.enter)+" "+getString(R.string.destination_bin)+" "+getString(R.string.manually));
		mManualTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				onManualAction();
			}
		});

		try {
			OutboundBussinessClass.getInstance().callOutboundConsolidationTasksVolleyRequest(this,
					ConsolidationScreen.class.getMethod("onTasksResponse", JSONObject.class, VolleyError.class));
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		mButtonReleaseResource.setOnClickListener(releaseResourceListener);
	}

	public void onTasksResponse(JSONObject jsonObject, VolleyError volleyError) {
		//System.out.println("Tasks Fetched"+ jsonObject.toString());
		try {
			OutboundSingleton.getInstance().headersTask(this, null, 
					ConsolidationScreen.class.getMethod("onHeadersFetched", String.class), false);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		if(volleyError == null){
			packingTasks = OutboundParser.parseOutboundTasksResult(jsonObject);
			if(packingTasks.size() == 0){
				//Utility.getInstance().showAlertDialog(this, getString(R.string.no_tasks_in_selected_order));
				return;
			}
			setDataToViews();
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	public void onHeadersFetched(String result) {
		System.out.println("Headers fetched");
	}

	private void setDataToViews() {
		mLinearLayout.removeAllViews();
		for(Task task : packingTasks){
			int position = packingTasks.indexOf(task);
			CustomButton customButton = new CustomButton(this);
			boolean isEmptySlot = Validation.isNullString(task.getLogicalPosition());
			boolean isResporceNoParing = Resource.getSelectedResource().getResourceType() == Resource.RPT_NO_PARING;
			if(isEmptySlot || isResporceNoParing)
				customButton.setTitleAndText(task.getHandlingUnit(), task.getDestinationBinValue());
			else
				customButton.setTitleAndText(task.getHandlingUnit()+"("+task.getLogicalPosition()+")", task.getDestinationBinValue());

			customButton.setInnerPadding();
			customButton.setPositionIndex(position);
			customButton.setOnClickListener(validationButonClickLisener);
			if(task.isValidTask())
				customButton.setAsVerified();
			else
				customButton.setAsNotVerified();
			
			String errorMessage =task.getErrorMessage();
			if(!(Validation.isNullString(errorMessage))){
				customButton.setAsWrongVerified();
			}
			
			mLinearLayout.addView(customButton);
		}
	}

	OnClickListener validationButonClickLisener = new OnClickListener() {
		@SuppressLint("NewApi")
		@Override
		public void onClick(View view) {
			mManualTextView.setVisibility(View.VISIBLE);
			if(mCurrentFocusedButton != null){
				mCurrentFocusedButton.setBackground(mButtonDrawable);
				mCurrentFocusedButton.setDarkTextColor();
				mCurrentFocusedButton.setAsNotVerified();
				mCurrentFocusedButton.clearAnimation();
			}

			if(mCurrentFocusedButton == (CustomButton) view){
				mManualTextView.setVisibility(View.GONE);
				mCurrentFocusedButton = null;
				return;
			}

			StaticScanner.getInstance().scannerRead(ConsolidationScreen.this, 1);
			mCurrentFocusedButton = (CustomButton) view;
			String errorMessage = packingTasks.get(mCurrentFocusedButton.getPositionIndex()).getErrorMessage();
			if(!(Validation.isNullString(errorMessage))){
				Util.showMessageDialog(ConsolidationScreen.this, errorMessage);
				packingTasks.get(mCurrentFocusedButton.getPositionIndex()).setErrorMessage("");
			}
			
			mButtonDrawable = mCurrentFocusedButton.getBackground();
			mCurrentFocusedButton.setAsNotVerified();
			mCurrentFocusedButton.setBackgroundResource(R.drawable.header);
			mCurrentFocusedButton.startAnimation(Utility.getInstance().blinkAnimation());
			mCurrentFocusedButton.removeVerificationForBlink();
			//Toast.makeText(ConsolidationScreen.this, mCurrentFocusedButton.getPositionIndex()+"", Toast.LENGTH_SHORT).show();
		}
	};

	private void onManualAction(){
		Intent intent  = new Intent(this, KeyPadReaderScreen.class);
		String title = getString(R.string.enter)+ " " + getString(R.string.destination_bin);
		intent.putExtra(AppConstants.ALPHA_KEYPAD_TITLE, title);
		intent.putExtra(AppConstants.ALPHA_KEYPAD_BATCHINPUT, "");
		startActivityForResult(intent, 1);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
			if (resultCode== RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				onDataScanned(result);
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, getString(R.string.manual_entry_cancelled), Toast.LENGTH_SHORT).show();
			}
		}
	}

	OnClickListener releaseResourceListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			confirmTasks();
		}
	};

	int numberOfPipelineRequests=0;
	private void confirmTasks() {
		boolean isAllTasksValid = true;
		for(Task task : packingTasks){
			isAllTasksValid = isAllTasksValid && task.isValidTask();
		}

		if(packingTasks.size() == 0 || isAllTasksValid){
			try {
				OutboundSingleton.getInstance().releaseAllocatedResource(ConsolidationScreen.this, 
						ConsolidationScreen.class.getMethod("onResourceReleased", ConfirmResponse.class), false);
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
			return;
		}

		numberOfPipelineRequests=0;
		
		for(Task task : packingTasks)
		{
			int index = packingTasks.indexOf(task);
			try {
				if(!task.isValidTask())
				{
					OutboundSingleton.getInstance().confirmPackingTask(this, ConsolidationScreen.class.getMethod("onTaskConfirmed", 
							ConfirmResponse.class), task, index);
					numberOfPipelineRequests++;
				}
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}	
		}
	}

	public void onTaskConfirmed(ConfirmResponse confirmResponse)
	{
		numberOfPipelineRequests--;
		int respCode = confirmResponse==null?0:confirmResponse.getResponseCode();
		if(confirmResponse==null || (respCode >= 200 && respCode < 300))
		{
			packingTasks.get(confirmResponse.getTaskIndex()).setValidTask(true);
			setDataToViews();
		}
		else
		{
			packingTasks.get(confirmResponse.getTaskIndex()).setValidTask(false);
			packingTasks.get(confirmResponse.getTaskIndex()).setErrorMessage(confirmResponse.getResponseLine());
		}
		
		if(numberOfPipelineRequests<=0)
		{
			int errorTasksCount=0;
			String errorMessage=null; 
			for(int index=0; index<packingTasks.size(); index++)
			{
				Task task = packingTasks.get(index);
				if(!task.isValidTask())
				{
					errorTasksCount++;
					errorMessage=task.getErrorMessage();
				}
			}

			if(errorTasksCount==0)
			{
				try {
					OutboundSingleton.getInstance().releaseAllocatedResource(ConsolidationScreen.this, 
							ConsolidationScreen.class.getMethod("onResourceReleased", ConfirmResponse.class), false);
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
			else if(errorTasksCount==1)
			{
				Util.showMessageDialog(this, errorMessage);
			}
			else
			{
				Util.showMessageDialog(this, getString(R.string.some_tasks_end_up_with_error_please_fix));
			}
		}
		setDataToViews();
	}

	public void onResourceReleased(ConfirmResponse confirmResponse) {
		int respCode = confirmResponse.getResponseCode();
		if (respCode >= 200 && respCode < 300) {
			releaseResource();
		}
		else {
			if(Validation.isNullString(confirmResponse.getResponseLine())){
				Util.showMessageDialog(this, getString(R.string.error_while_releasing_resource_please_try_again));
			}
			else{
				boolean isHUIssue = false;
				ErrorResponse errorResponse = confirmResponse.getErrorResponse();
				System.out.println(errorResponse.getErrorCode());
				if(errorResponse.getErrorCode().equalsIgnoreCase("SY/530")){
					System.out.println(isHUIssue);
					isHUIssue = true;
					for(ErrorDetails details : errorResponse.getErrorDetailsList()){
						if(details.getErrorDetailsCode().equalsIgnoreCase("/IWBEP/CX_SD_GEN_DPC_BUSINS")){
							isHUIssue = true;
						}
					}
				}
				System.out.println(isHUIssue);
				if(isHUIssue){
					//Util.showMessageDialog(this, "Unable to release Resource. Please try again.");
					try {
						OutboundSingleton.getInstance().releaseAllocatedResource(ConsolidationScreen.this, 
								ConsolidationScreen.class.getMethod("onResourceReleasedWithFlag", ConfirmResponse.class), true);
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					}
				}
				else{
					Util.showMessageDialog(this, confirmResponse.getResponseLine());
				}
			}
		}
	}

	public void onResourceReleasedWithFlag(ConfirmResponse confirmResponse) {
		int respCode = confirmResponse.getResponseCode();
		if (respCode >= 200 && respCode < 300) {
			releaseResource();
		}
		else {
			if(Validation.isNullString(confirmResponse.getResponseLine())){
				Util.showMessageDialog(this, getString(R.string.error_while_releasing_resource_please_try_again));
			}
			else{
				Util.showMessageDialog(this, confirmResponse.getResponseLine());
			}
		}
	}

	private void releaseResource() {
		Resource.releaseSelectedResource();
		if(isLogoutCalled){
			Intent intent = new Intent(ConsolidationScreen.this, LoginWebviewScreen.class);
			intent.putExtra(AppConstants.LOGOUT, AppConstants.LOGOUT);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
			finish();
		}
		else{
			Resource.releaseSelectedResource();
			Intent intent = new Intent(ConsolidationScreen.this, OrdersScreen.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;

		case R.id.action_settings_item:
			Utility.getInstance().showSettingsDialog(ConsolidationScreen.this);
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	private void onDataScanned(String scanValue) {
		if(mCurrentFocusedButton != null){
			mCurrentFocusedButton.setBackground(mButtonDrawable);
			mCurrentFocusedButton.setDarkTextColor();
			mCurrentFocusedButton.clearAnimation();

			mManualTextView.setVisibility(View.GONE);
			//packingTasks.get(mCurrentFocusedButton.getPositionIndex()).setModifiedDestinationBin(scanValue);
			showpackStationDialog(scanValue, mCurrentFocusedButton.getPositionIndex());
			//Task taskTemp = packingTasks.get(mCurrentFocusedButton.getPositionIndex());
			//mCurrentFocusedButton.setTitleAndText(taskTemp.getHandlingUnit(), taskTemp.getDestinationBinValue());
		}
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		onDataScanned(scanValue);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	private void showpackStationDialog(final String scannedValue, final int selectedButtonIndex){
		String[] itemsArray ;
		final boolean[] checkedItems;
		// Filter out the non edited Dest bins...
		{
			List<String> totes = new ArrayList<String>();
			List<Boolean> checks = new ArrayList<Boolean>();
			for(int index=0;index<packingTasks.size(); index++)
			{
				Task task = packingTasks.get(index);
				if(Validation.isNullString(task.getModifiedDestinationBin()))
				{
					totes.add(task.getHandlingUnit());
					if(index==selectedButtonIndex)
						checks.add(true);
					else
						checks.add(false);
				}
			}
			itemsArray=new String[totes.size()];
			itemsArray = totes.toArray(itemsArray);
			
			checkedItems=new boolean[itemsArray.length];
			for(int index=0;index<checks.size(); index++)
			{
				checkedItems[index]=(checks.get(index).equals(true));
			}
		}
		
		if(itemsArray.length <= 1)
		{
			packingTasks.get(selectedButtonIndex).setModifiedDestinationBin(scannedValue);
			setDataToViews();
			return;
		}
		
		AlertDialog dialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select Totes to change Packing Station");
		builder.setMultiChoiceItems(itemsArray, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
				if (isChecked) {
					//packingTasks.get(indexSelected).setModifiedDestinationBin(scannedValue);
					checkedItems[indexSelected]=true;
				} else{
					//packingTasks.get(indexSelected).setModifiedDestinationBin("");
					checkedItems[indexSelected]=false;
				}
			}
		})
		// Set the action buttons
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				
				int displayedIndex=-1;
				for(int index=0;index<packingTasks.size(); index++)
				{
					Task task = packingTasks.get(index);
					if(Validation.isNullString(task.getModifiedDestinationBin()))
					{
						displayedIndex++;
						if(checkedItems[displayedIndex])
							packingTasks.get(index).setModifiedDestinationBin(scannedValue);
					}
				}
				
				dialog.dismiss();
				setDataToViews();
			}
		})
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		dialog = builder.create();//AlertDialog dialog; create like this outside onClick
		dialog.show();
	}
}