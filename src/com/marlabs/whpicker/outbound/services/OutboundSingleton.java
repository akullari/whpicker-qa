package com.marlabs.whpicker.outbound.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.CookieManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.marlabs.whpicker.models.ConfirmResponse;
import com.marlabs.whpicker.models.ErrorResponse;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.models.SplitTask;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.models.TaskObject;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.networking.UserInfo;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Validation;
import com.marlabs.whpickerqa.R;

public class OutboundSingleton {

	public static final String TAG = OutboundSingleton.class.getSimpleName();
	private static String receivedToken;
	private static String cookiesFetched;
	private DefaultHttpClient client;
	private static OutboundSingleton mInstance;

	private OutboundSingleton(){
		client = new DefaultHttpClient();
	}

	public static OutboundSingleton getInstance(){
		if (mInstance == null) {
			mInstance = new OutboundSingleton();
		}
		return mInstance;
	}

	public void headersTask(Context context, Order orderObject, Method method, boolean isResource) {
		new HeadersAsyncTask(context, orderObject, method, isResource).execute("");
	}

	public void allocateResource(Context context, Method method, Order orderObject) {
		Toast.makeText(context, context.getString(R.string.allocating_resource), Toast.LENGTH_SHORT).show();
		new ResourceAllocationAsyncTask(context, method, orderObject).execute("");
	}

	public void releaseAllocatedResource(Context context, Method method, boolean booleanInput) {
		new ReleaseResourceAsyncTask(context, method, booleanInput).execute("");
	}

	public void ConfirmTask(Context context, Method method, List<Task> tasksList) {
		Toast.makeText(context, context.getString(R.string.confirming_task), Toast.LENGTH_SHORT).show();
		new ConfirmTaskAsyncTask(context, method, tasksList).execute("");
	}

	public void confirmPackingTask(Context context, Method method, Task task, int index) {
		new ConfirmPackingAsyncTask(context, method, task, index).execute("");
	}

	public void parkTask(Context context, Method method, Task task) {
		Toast.makeText(context,  context.getString(R.string.parking_task), Toast.LENGTH_SHORT).show();
		new parkAsyncTaskOutbound(context, method, task).execute("");
	}

	public void createTote(Context context, Method method, Task task, SplitTask splitTask) {
		Toast.makeText(context,  context.getString(R.string.creating_tote), Toast.LENGTH_SHORT).show();
		new createToteAsync(context, method, task, splitTask).execute("");
	}

	public void callBatchUpdate(String consGroup, Context context, Method method, List<Task> taskList) {
		Toast.makeText(context,  context.getString(R.string.confirming_batch_task_), Toast.LENGTH_SHORT).show();
		new BatchRequest(consGroup, context, method, taskList).execute();
	}

	public class HeadersAsyncTask extends AsyncTask<String, Void, String> {
		private ProgressDialog mProgressDialog;
		private Order orderObject;
		private Context context;
		private Method method;
		private boolean isResource;
		public HeadersAsyncTask(Context context, Order orderObject, Method method, boolean isResource) {
			this.orderObject = orderObject;
			this.context = context;
			this.method = method;
			this.isResource = isResource;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
			mProgressDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			return fetchHeaders(context, orderObject, method, isResource);
		}

		@Override
		protected void onPostExecute(String result) {
			mProgressDialog.dismiss();
		}
	}

	public String fetchHeaders(Context context, Order orderObject, Method method, boolean isResource){
		String url;
		if(isResource){
			url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_WHO_STATUS_CHANGE_SRV/WhOrderSet(IvLgnum='"+
					UserInfo.getInstance().getWareHouse(context)+"',IvWhoid='"+orderObject.getWHOrder()+"')";
		}
		else{
			url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_WHO_STATUS_CHANGE_SRV/WhOrderSet(IvLgnum='"+
					UserInfo.getInstance().getWareHouse(context)+"')";
		}
		System.out.println("URL--" + url);
		HttpGet get = new HttpGet(url);

		get.setHeader("x-csrf-token", "fetch");
		get.setHeader("Content-Type", "application/json; charset=utf-8");
		get.setHeader("Accept", "application/json");
		get.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));
		//get.setHeader("Authorization", UserInfo.getInstance().getAuthorizationHeader());

		HttpResponse response = null;
		String jsonResponse = "";
		try {
			response = client.execute(get);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			while ((jsonResponse = rd.readLine()) != null) {
				try {
					method.invoke(context, jsonResponse);
				}  catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		List<Cookie> cookies = client.getCookieStore().getCookies();
		if (cookies.isEmpty()) {
			System.out.println("None");
		} 
		else {
			for (int i = 0; i < cookies.size(); i++) {
				System.out.println("- " + cookies.get(i).toString());
			}
			cookiesFetched = CookieManager.getInstance().getCookie(url);
		}

		if(response!=null)
		{
			Header[] headerFields = response.getAllHeaders();
			for (Header header : headerFields) {
				if (header.getName().equalsIgnoreCase("x-csrf-token")) {
					OutboundSingleton.receivedToken = header.getValue();
					System.out.println("Token GET request "+receivedToken);
				}
			}
		}
		return jsonResponse;
	}

	public class ConfirmTaskAsyncTask extends AsyncTask<String, Void, ConfirmResponse> {
		private ProgressDialog mProgressDialog;
		private Context context;
		private Method method;
		private List<Task> tasksList;
		public ConfirmTaskAsyncTask(Context context, Method method, List<Task> tasksList) {
			this.context = context;
			this.tasksList = tasksList;
			this.method = method;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
			mProgressDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected ConfirmResponse doInBackground(String... params) {
			return closeSingleTask(context, tasksList);
		}

		@Override
		protected void onPostExecute(ConfirmResponse confirmResponse) {
			mProgressDialog.dismiss();
			try {
				method.invoke(context, confirmResponse);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	public ConfirmResponse closeSingleTask(Context context, List<Task> tasksList){
		ConfirmResponse confirmResponse = new ConfirmResponse();
		String url = null;
		HashMap<String, String> params = new HashMap<String, String>();
		for (Task task : tasksList){
			url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_CREATE_HU_AND_WTCONFIRM_SRV/WHOpenTaskConfirmSet" +
					"(WHTask='"+ task.getWareHouseTask() +"',Warehouse='"+ UserInfo.getInstance().getWareHouse(context) +"')";
			for(SplitTask splitTask : task.getSplitTasksList())
			{
				params.put("WHTask", task.getWareHouseTask());
				params.put("Warehouse", UserInfo.getInstance().getWareHouse(context));
				params.put("Queue", task.getQueue() != null? task.getQueue():"");
				params.put("Huident", splitTask.getTote());
				params.put("Pmat", "PICKHU");
				params.put("Rsrc", Resource.getSelectedResource().getResourceName());

				//Setting Logical Position
				boolean isEmptySlot = Validation.isNullString(splitTask.getSlot());
				boolean isResporceNoParing = Resource.getSelectedResource().getResourceType() != Resource.RPT_NO_PARING;
				if(!isEmptySlot && isResporceNoParing){
					params.put("Logpos", splitTask.getSlot()); 
				}

				//Setting SourceHU
				if(!Validation.isNullString(task.getSourceHU())){
					params.put("SrcHU", task.getSourceHU());
				}

				if(task.getSplitTasksList().size() > 1){
					params.put("Quantity", splitTask.getQuantity()+"");
					params.put("Unit", task.getQuantityUoM());
					params.put("Split", "X");
				}
			}
		}
		HttpPut put = new HttpPut(url);
		JSONObject jsonObject = new JSONObject(params);
		System.out.println(jsonObject.toString());
		try {
			System.out.println("Received token "+receivedToken);
			put.setHeader("x-csrf-token", receivedToken);
			//put.setHeader("Authorization", UserInfo.getInstance().getAuthorizationHeader());
			//put.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));
			put.setHeader("Cookie", cookiesFetched);
			put.setHeader("Content-Type", "application/json; charset=utf-8");
			put.setHeader("Accept", "application/json");
			put.setHeader("charset", "utf-8");
			put.setHeader("Accept-Language","en-US" );

			List<Cookie> cookies = client.getCookieStore().getCookies();

			String cookieString = "";
			for (Cookie cookie : cookies) {
				cookieString+=cookie.getName()+"="+cookie.getValue()+";";

			}
			put.setHeader("Cookie", cookieString);

			HttpEntity entity;
			StringEntity stringEntity = new StringEntity(jsonObject.toString());
			stringEntity.setContentEncoding((Header) new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			entity = stringEntity;
			put.setEntity(entity);

			HttpResponse response = client.execute(put);
			int responseCode = response.getStatusLine().getStatusCode();
			confirmResponse.setResponseCode(responseCode);
			System.out.println("Response Code "+responseCode);
			System.out.println("Response "+response);
			if(responseCode > 200 && responseCode < 300){
				confirmResponse.setResponseLine("Success");
				return confirmResponse;
			}
			else if(responseCode == 401){
				Util.userLogout(context);
			}
			else{
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				while ((line = rd.readLine()) != null) {
					System.out.println(line);
					confirmResponse.setResponseLine(Parser.parseError(line));
					return confirmResponse;
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	public class parkAsyncTaskOutbound extends AsyncTask<String, Void, ConfirmResponse> {
		private ProgressDialog mProgressDialog;
		private Context context;
		private Method method;
		private Task outboundTask;
		public parkAsyncTaskOutbound(Context context, Method method, Task outboundTask){
			this.context = context;
			this.outboundTask = outboundTask;
			this.method = method;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
			mProgressDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected ConfirmResponse doInBackground(String... params) {
			return parkTask(context, outboundTask);
		}

		@Override
		protected void onPostExecute(ConfirmResponse confirmResponse) {
			mProgressDialog.dismiss();
			try {
				method.invoke(context, confirmResponse);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	private ConfirmResponse parkTask(Context context, Task task){
		ConfirmResponse confirmResponse = new ConfirmResponse();
		confirmResponse.setStrTag(task.getWareHouseTask());

		String url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_UNASSIGN_WT_FROM_ORDER_SRV/TaskFromOrderSet(IvWho='"+task.getWHOrder()+"',Tanum='"+
				task.getWareHouseTask()+"',IvLgnum='"+ UserInfo.getInstance().getWareHouse(context) +"')";

		HttpPut put = new HttpPut(url);

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("Tanum", task.getWareHouseTask());
		params.put("IvLgnum", UserInfo.getInstance().getWareHouse(context));
		params.put("IvWho", task.getWHOrder());
		JSONObject jsonObject = new JSONObject(params);
		System.out.println(jsonObject.toString());

		try {
			System.out.println("Received token "+receivedToken);
			put.setHeader("x-csrf-token", receivedToken);
			//put.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));
			put.setHeader("Cookie", cookiesFetched);
			put.setHeader("Content-Type", "application/json; charset=utf-8");
			put.setHeader("Accept", "application/json");
			put.setHeader("charset", "utf-8");
			put.setHeader("Accept-Language","en-US" );

			List<Cookie> cookies = client.getCookieStore().getCookies();
			String cookieString = "";
			for (Cookie cookie : cookies) {
				cookieString+=cookie.getName()+"="+cookie.getValue()+";";
			}
			put.setHeader("Cookie", cookieString);

			HttpEntity entity;
			StringEntity stringEntity = new StringEntity(jsonObject.toString());
			stringEntity.setContentEncoding((Header) new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			entity = stringEntity;
			put.setEntity(entity);
			HttpResponse response = client.execute(put);
			int responseCode = response.getStatusLine().getStatusCode();
			confirmResponse.setResponseCode(responseCode);
			System.out.println("Response Code "+responseCode);
			System.out.println("Response "+response);
			if(responseCode > 200 && responseCode < 300){
				confirmResponse.setResponseLine("Success");
				return confirmResponse;
			}
			else if(responseCode == 401){
				Util.userLogout(context);
			}
			else{
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				while ((line = rd.readLine()) != null) {
					System.out.println(line);
					confirmResponse.setResponseLine(Parser.parseError(line));
					return confirmResponse;
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	public class ResourceAllocationAsyncTask extends AsyncTask<String, Void, ConfirmResponse> {
		private ProgressDialog mProgressDialog;
		private Context context;
		private Method method;
		private Order inboundTask;
		public ResourceAllocationAsyncTask(Context context, Method method, Order inboundTask) {
			this.context = context;
			this.inboundTask = inboundTask;
			this.method = method;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
			mProgressDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected ConfirmResponse doInBackground(String... params) {
			return allocateResource(context, inboundTask);
		}

		@Override
		protected void onPostExecute(ConfirmResponse confirmResponse) {
			mProgressDialog.dismiss();
			try {
				method.invoke(context, confirmResponse);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	public ConfirmResponse allocateResource(Context context, Order orderObject){
		ConfirmResponse confirmResponse = new ConfirmResponse();

		String resourceName = Resource.getSelectedResource().getResourceName();

		String url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_WHO_STATUS_CHANGE_SRV/WhOrderSet(" +
				"IvLgnum='"+ UserInfo.getInstance().getWareHouse(context)+"',IvWhoid='"+orderObject.getWHOrder()+"')";
		System.out.println("URL ---" + url);
		HttpPut put = new HttpPut(url);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("IvLgnum", UserInfo.getInstance().getWareHouse(context));
		params.put("IvWhoid", orderObject.getWHOrder());
		params.put("Rsrc", resourceName);
		JSONObject jsonObject = new JSONObject(params);
		System.out.println("resource----" + jsonObject.toString());

		try {
			System.out.println("Received token "+ receivedToken);
			put.setHeader("x-csrf-token", receivedToken);
			//put.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));
			put.setHeader("Cookie", cookiesFetched);
			put.setHeader("Content-Type", "application/json; charset=utf-8");
			put.setHeader("Accept", "application/json");
			put.setHeader("charset", "utf-8");
			put.setHeader("Accept-Language","en-US" );

			HttpEntity entity;
			StringEntity stringEntity = new StringEntity(jsonObject.toString());
			stringEntity.setContentEncoding((Header) new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			entity = stringEntity;
			put.setEntity(entity);

			HttpResponse response = client.execute(put);
			int responseCode = response.getStatusLine().getStatusCode();
			confirmResponse.setResponseCode(responseCode);
			System.out.println("Response Code "+responseCode);
			System.out.println("Response "+response);
			if(responseCode > 200 && responseCode < 300){
				confirmResponse.setResponseLine("Success");
				return confirmResponse;
			}
			else if(responseCode == 401){
				Util.userLogout(context);
			}
			else{
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				while ((line = rd.readLine()) != null) {
					System.out.println(line);
					confirmResponse.setResponseLine(Util.getDataInBraces(Parser.parseError(line)));
					return confirmResponse;
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			return confirmResponse;
		}
		return null;
	}

	public class ReleaseResourceAsyncTask extends AsyncTask<String, Void, ConfirmResponse> {
		private ProgressDialog mProgressDialog;
		private Context context;
		private Method method;
		private boolean booleanInput;
		public ReleaseResourceAsyncTask(Context context, Method method, boolean booleanInput) {
			this.context = context;
			this.method = method;
			this.booleanInput = booleanInput;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
			mProgressDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected ConfirmResponse doInBackground(String... params) {
			return releaseResource(context, booleanInput);
		}

		@Override
		protected void onPostExecute(ConfirmResponse confirmResponse) {
			mProgressDialog.dismiss();
			try {
				method.invoke(context, confirmResponse);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	public ConfirmResponse releaseResource(Context context, boolean booleanInput){
		ConfirmResponse confirmResponse = new ConfirmResponse();

		String resourceName = Resource.getSelectedResource().getResourceName();
		String url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_WHO_STATUS_CHANGE_SRV/WORsrcSet(IvLgnum='"+ 
				UserInfo.getInstance().getWareHouse(context) +"',IvWhoid='')";
		System.out.println("URL ---------" + url);

		HttpPut put = new HttpPut(url);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("IvLgnum", UserInfo.getInstance().getWareHouse(context));
		params.put("Rsrc", resourceName);
		System.out.println("booleanInput ---------" + booleanInput);
		if(booleanInput)
			params.put("Flgrsrc", "X");
		JSONObject jsonObject = new JSONObject(params);

		System.out.println("resource----" + jsonObject.toString());
		try {
			System.out.println("Received token "+ receivedToken);
			put.setHeader("x-csrf-token", receivedToken);
			//put.setHeader("Authorization", UserInfo.getInstance().getAuthorizationHeader());
			//put.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));
			put.setHeader("Cookie", cookiesFetched);
			put.setHeader("Content-Type", "application/json; charset=utf-8");
			put.setHeader("Accept", "application/json");
			put.setHeader("charset", "utf-8");
			put.setHeader("Accept-Language","en-US" );

			HttpEntity entity;
			StringEntity stringEntity = new StringEntity(jsonObject.toString());
			stringEntity.setContentEncoding((Header) new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			entity = stringEntity;
			put.setEntity(entity);

			HttpResponse response = client.execute(put);
			int responseCode = response.getStatusLine().getStatusCode();
			confirmResponse.setResponseCode(responseCode);
			System.out.println("Response Code "+responseCode);
			if(responseCode > 200 && responseCode < 300){
				confirmResponse.setResponseLine("Success");
				return confirmResponse;
			}
			else if(responseCode == 401){
				Util.userLogout(context);
			}
			else{
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				while ((line = rd.readLine()) != null) {
					System.out.println(line);
					ErrorResponse errorResponse = Parser.parseErrorDetails(line);
					confirmResponse.setErrorResponse(errorResponse);
					confirmResponse.setResponseLine(Util.getDataInBraces(errorResponse.getErrorMessage()));
					//System.out.println(confirmResponse.getErrorResponse().getErrorCode());
					return confirmResponse;
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	public class BatchRequest extends AsyncTask<Void, Void, Void>{
		private HttpResponse httpResponse;
		private HttpPost httpPostRequest;
		private List<Task> mTaskList;
		private Context mContext;
		private Method mMethod;
		private ProgressDialog mProgressDialog;
		public BatchRequest(String consGroup, Context context, Method method, List<Task> taskList){
			mTaskList = taskList;
			mContext = context;
			mMethod = method;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
			mProgressDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute(){
			mProgressDialog.show();
			String boundary = "batch";
			String changeset = 	"changeset";
			String newLineChar = 	"\r\n";
			StringBuilder builder = new StringBuilder();

			for (Task task : mTaskList){
				for(SplitTask splitTask : task.getSplitTasksList()){
					System.out.println("batch Service Quantity " + splitTask.getQuantity());
					System.out.println("batch Service Tote " + splitTask.getTote());

					builder.append("--"+boundary);
					builder.append(newLineChar);
					builder.append("Content-Type: multipart/mixed; boundary="+changeset);
					builder.append(newLineChar+newLineChar);


					builder.append("--"+changeset);
					builder.append(newLineChar);
					builder.append("Content-Type: application/http");
					builder.append(newLineChar);
					builder.append("Content-Transfer-Encoding: binary");
					builder.append(newLineChar+newLineChar);

					TaskObject object = new TaskObject();
					object.setWHTask(task.getWareHouseTask());
					object.setWarehouse(UserInfo.getInstance().getWareHouse(mContext));
					object.setQueue(task.getQueue() != null? task.getQueue():"");
					object.setHuident(splitTask.getTote());
					object.setPmat("PICKHU");
					object.setRsrc(Resource.getSelectedResource().getResourceName());

					boolean isEmptySlot = Validation.isNullString(splitTask.getSlot());
					boolean isResporceNoParing = Resource.getSelectedResource().getResourceType() != Resource.RPT_NO_PARING;
					if(!isEmptySlot && isResporceNoParing){
						object.setLogpos(Integer.parseInt(splitTask.getSlot()));
					}
					if(!Validation.isNullString(task.getSourceHU())){
						object.setSrcHU(task.getSourceHU());
					}

					if(task.getSplitTasksList().size() > 1){
						object.setQuantity(splitTask.getQuantity()+"");
						object.setUnit(task.getQuantityUoM());
						object.setSplit("X");
					}

					Gson gson = new Gson();
					String json = gson.toJson(object);
					String batchInput = "PUT WHOpenTaskConfirmSet(WHTask='"+ task.getWareHouseTask() +"',Warehouse='"+ UserInfo.getInstance().getWareHouse(mContext) +"') HTTP/1.1 ";
					System.out.println("batchRequest----------" + batchInput);
					System.out.println("Imputs----------" + json);

					builder.append(batchInput);
					builder.append(newLineChar);
					builder.append("Content-Type:application/json; charset=utf-8");
					builder.append(newLineChar);
					builder.append("Accept:application/json");
					builder.append(newLineChar);
					builder.append("Content-Length:"+String.valueOf(json.length()));
					builder.append(newLineChar+newLineChar);

					builder.append(json);
					builder.append(newLineChar+newLineChar);

					builder.append("--"+changeset+"--");
					builder.append(newLineChar);
				}
			}

			builder.append("--"+boundary+"--");
			System.out.println("BATCH REQUEST BODY : \n");
			//System.out.println(builder);
			System.out.println("BODY  END ");//ZEWM_WAREHOUSE_TASK_READ_SRV ZEWM_CREATE_HU_AND_WTCONFIRM_SRV/$batch
			httpPostRequest = new HttpPost(URLFactory.getBaseGateWayURL(mContext) + "odata/sap/ZEWM_CREATE_HU_AND_WTCONFIRM_SRV/$batch");
			try{
				httpPostRequest.setEntity(new StringEntity(builder.toString(), "UTF8"));
			}
			catch(Exception e){
				e.printStackTrace();
			}

			httpPostRequest.setHeader("x-csrf-token", receivedToken);
			httpPostRequest.setHeader("Content-Type", "multipart/mixed;boundary=batch");
			httpPostRequest.setHeader("Accept", "application/json");
			httpPostRequest.setHeader("Cookie", UserInfo.getInstance().getUserCookies(mContext));

			Log.e("COOKIE STORE ", client.getCookieStore().toString());
			List<Cookie> cookies = client.getCookieStore().getCookies();

			String cookieString = "";
			for (Cookie cookie : cookies) {
				cookieString+=cookie.getName()+"="+cookie.getValue()+";";
			}
			cookieString = cookieString.replace(cookieString.charAt(cookieString.length()-1), ' ').trim();

			httpPostRequest.setHeader("Cookie", cookieString);
			Header[] requestHeaders = httpPostRequest.getAllHeaders();
			for (Header header : requestHeaders) {
				System.out.println("Header name : "+ header.getName() + " Value "+ header.getValue());
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			try {
				httpResponse = client.execute(httpPostRequest);
			} 
			catch (ClientProtocolException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				e.printStackTrace();
			}	
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			mProgressDialog.dismiss();
			try {
				int responseCode = httpResponse.getStatusLine().getStatusCode();
				System.out.println("Response Code:"+ responseCode);

				if(responseCode == 401){
					Util.userLogout(mContext);
				}

				else if(responseCode > 200 && responseCode < 300){
					HttpEntity entity = httpResponse.getEntity();
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent()));
					List<String> responsesList = new ArrayList<String>();
					String responseLine;
					while((responseLine = bufferedReader.readLine()) != null){
						System.out.println(responseLine);
						responsesList.add(responseLine);
					}

					List<List<String>> batchesList = new ArrayList<List<String>>();
					List<String> currentBatchResponse = null;
					boolean isbatchStarted = false;
					for(String response : responsesList){
						if(Util.getPatternIndex(response) == 1){
							isbatchStarted = true;
							currentBatchResponse = new ArrayList<String>();
						}
						else if(Util.getPatternIndex(response) == 3){
							if(isbatchStarted){
								batchesList.add(currentBatchResponse);
								isbatchStarted = false;
							}
						}
						else{
							currentBatchResponse.add(response);
						}
					}
					List<ConfirmResponse> confirmResponsesList = new ArrayList<ConfirmResponse>();
					for(List<String> list : batchesList){
						boolean isError = false;
						ConfirmResponse confirmResponse = new ConfirmResponse();
						String msg = "";
						for(String string : list){
							if(string.contains("HTTP/1.1")){
								String batchCode = string.replaceFirst(".*?(\\d+\\d).*", "$1");
								int responseBatchCode = Integer.parseInt(batchCode);
								if(responseBatchCode >= 200 && responseBatchCode < 300){
									isError = false;
								}
								else{
									isError = true;
								}
							}
							if(isError){
								msg = string;
							}
						}

						if(!Validation.isNullString(msg))
							msg = Parser.parseError(msg);

						confirmResponse.setBatchError(isError);
						confirmResponse.setBatchErrorMessage(msg);
						confirmResponsesList.add(confirmResponse);
					}

					ConfirmResponse confirmResponse = new ConfirmResponse();
					confirmResponse.setBatchResponses(confirmResponsesList);
					bufferedReader.close();
					mMethod.invoke(mContext, confirmResponse);
				}
				else{
					BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
					String line = "";
					while ((line = rd.readLine()) != null) {
						System.out.println(line);
						ConfirmResponse confirmResponse = new ConfirmResponse();
						ErrorResponse errorResponse = Parser.parseErrorDetails(line);
						confirmResponse.setResponseCode(responseCode);
						confirmResponse.setErrorResponse(errorResponse);
						confirmResponse.setResponseLine(Util.getDataInBraces(errorResponse.getErrorMessage()));
						//System.out.println(confirmResponse.getErrorResponse().getErrorCode());
						mMethod.invoke(mContext, confirmResponse);
					}
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public class createToteAsync extends AsyncTask<String, Void, ConfirmResponse> {
		private ProgressDialog mProgressDialog;
		private Context context;
		private Method method;
		private Task task;
		private SplitTask splitTask;
		public createToteAsync(Context context, Method method, Task task, SplitTask splitTask) {
			this.context = context;
			this.task = task;
			this.method = method;
			this.splitTask = splitTask;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
			mProgressDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected ConfirmResponse doInBackground(String... params) {
			return createToatServiceCall(context, task, splitTask);
		}

		@Override
		protected void onPostExecute(ConfirmResponse confirmResponse) {
			mProgressDialog.dismiss();
			try {
				method.invoke(context, confirmResponse);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	public ConfirmResponse createToatServiceCall(Context context, Task task, SplitTask splitTask){
		ConfirmResponse confirmResponse = new ConfirmResponse();
		String url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_WAREHOUSE_TASK_READ_SRV/PickHUSet";
		HttpPost httpPost = new HttpPost(url);

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("Huident", splitTask.getTote());
		params.put("Lgnum", UserInfo.getInstance().getWareHouse(context));
		params.put("Pmat", "PICKHU");
		params.put("Who", task.getWHOrder());
		params.put("Lgpla", task.getDestinationBin());
		JSONObject jsonObject = new JSONObject(params);
		System.out.println(jsonObject.toString());

		try {
			System.out.println("Received token "+ receivedToken);
			httpPost.setHeader("x-csrf-token", receivedToken);
			//httpPost.setHeader("Authorization", UserInfo.getInstance().getAuthorizationHeader());
			httpPost.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));
			httpPost.setHeader("Content-Type", "application/json; charset=utf-8");
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("charset", "utf-8");
			httpPost.setHeader("Accept-Language","en-US" );

			List<Cookie> cookies = client.getCookieStore().getCookies();

			String cookieString = "";
			for (Cookie cookie : cookies) {
				cookieString+=cookie.getName()+"="+cookie.getValue()+";";
			}
			httpPost.setHeader("Cookie", cookieString);

			HttpEntity entity;
			StringEntity stringEntity = new StringEntity(jsonObject.toString());
			stringEntity.setContentEncoding((Header) new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			entity = stringEntity;
			httpPost.setEntity(entity);

			HttpResponse response = client.execute(httpPost);
			int responseCode = response.getStatusLine().getStatusCode();
			confirmResponse.setResponseCode(responseCode);
			System.out.println("Response Code "+responseCode);
			System.out.println("Response "+response);
			if(responseCode > 200 && responseCode < 300){
				confirmResponse.setResponseLine("Success");
				return confirmResponse;
			}
			else if(responseCode == 401){
				Util.userLogout(context);
			}
			else{
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				while ((line = rd.readLine()) != null) {
					System.out.println(line);
					confirmResponse.setResponseLine(Util.getDataInBraces(Parser.parseError(line)));
					return confirmResponse;
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	public class ConfirmPackingAsyncTask extends AsyncTask<String, Void, ConfirmResponse> {
		private ProgressDialog mProgressDialog;
		private Context context;
		private Method method;
		private Task task;
		private int index;
		public ConfirmPackingAsyncTask(Context context, Method method, Task task, int index) {
			this.context = context;
			this.task = task;
			this.method = method;
			this.index = index;
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getString(R.string.please_wait));
			mProgressDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
		}

		@Override
		protected ConfirmResponse doInBackground(String... params) {
			return confirmPackingTask(context, task);
		}

		@Override
		protected void onPostExecute(ConfirmResponse confirmResponse) {
			mProgressDialog.dismiss();
			try {
				confirmResponse.setTaskIndex(index);
				method.invoke(context, confirmResponse);
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	public ConfirmResponse confirmPackingTask(Context context, Task task){
		ConfirmResponse confirmResponse = new ConfirmResponse();

		HttpPut put = new HttpPut(URLFactory.getOutboundPickingConfirmTasks_URL(context, task));
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("WHTask", task.getWareHouseTask());
		params.put("Warehouse", UserInfo.getInstance().getWareHouse(context));
		params.put("Queue", task.getQueue() != null? task.getQueue():"");
		params.put("Huident", task.getHandlingUnit());
		if(!Validation.isNullString(task.getModifiedDestinationBin()))
			params.put("DestBin", task.getModifiedDestinationBin());
		params.put("Pmat", "PICKHU");
		params.put("Rsrc", Resource.getSelectedResource().getResourceName());
		params.put("FlgHuTo", "X");
		boolean isEmptySlot = Validation.isNullString(task.getLogicalPosition());
		boolean isResporceNoParing = Resource.getSelectedResource().getResourceType() != Resource.RPT_NO_PARING;
		if(!isEmptySlot && isResporceNoParing){
			params.put("Logpos", task.getLogicalPosition()); 
		}

		JSONObject jsonObject = new JSONObject(params);
		System.out.println(jsonObject.toString());

		try {
			System.out.println("Received token "+receivedToken);
			put.setHeader("x-csrf-token", receivedToken);
			//put.setHeader("Cookie", UserInfo.getInstance().getUserCookies(context));
			put.setHeader("Cookie", cookiesFetched);
			put.setHeader("Content-Type", "application/json; charset=utf-8");
			put.setHeader("Accept", "application/json");
			put.setHeader("charset", "utf-8");
			put.setHeader("Accept-Language","en-US" );

			HttpEntity entity;
			StringEntity stringEntity = new StringEntity(jsonObject.toString());
			stringEntity.setContentEncoding((Header) new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			entity = stringEntity;
			put.setEntity(entity);

			HttpResponse response = client.execute(put);
			int responseCode = response.getStatusLine().getStatusCode();
			confirmResponse.setResponseCode(responseCode);
			System.out.println("Response Code "+responseCode);
			System.out.println("Response "+response);
			if(responseCode > 200 && responseCode < 300){
				confirmResponse.setResponseLine("Success");
				return confirmResponse;
			}
			else if(responseCode == 401){
				Util.userLogout(context);
			}
			else{
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				while ((line = rd.readLine()) != null) {
					System.out.println(line);
					confirmResponse.setResponseLine(Parser.parseError(line));
					return confirmResponse;
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
}