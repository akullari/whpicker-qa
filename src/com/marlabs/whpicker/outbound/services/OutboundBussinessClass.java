package com.marlabs.whpicker.outbound.services;

import org.json.JSONObject;

import android.content.Context;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.networking.ConnectionDetector;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.networking.VolleyRequestHandler;
import com.marlabs.whpicker.outbound.ByProcessorScreen;
import com.marlabs.whpicker.outbound.ByQueuesScreen;
import com.marlabs.whpicker.outbound.ByWHOScreen;
import com.marlabs.whpicker.outbound.OrdersScreen;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;

public class OutboundBussinessClass {

	private static OutboundBussinessClass instance = new OutboundBussinessClass();

	private OutboundBussinessClass(){}

	public static OutboundBussinessClass getInstance(){
		return instance;
	}

	public void callQueuesVolleyRequest(Context context) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				String url = URLFactory.getOutboundQueues_URL(context);
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						ByQueuesScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	public void callWHOorHUVolleyRequest(Context context, String url) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						ByWHOScreen.class.getMethod("onOrdersResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	public void callWHODetailsVolleyRequest(Context context, String whOrder) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				String url = URLFactory.getOutboundWHODetails_URL(context, whOrder);
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						ByWHOScreen.class.getMethod("onOrdersDetailResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	//	public void callAllOutboundOrdersVolleyRequest(Context context) {
	//		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
	//			try {
	//				String url = URLFactory.getAllOutboundOrders_URL(context);
	//				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, Util.getBasicHANAHeaders(context),
	//						ByQueuesScreen.class.getMethod("onOrdersResponse", JSONObject.class, VolleyError.class));
	//			} catch (NoSuchMethodException e) {
	//				e.printStackTrace();
	//			}
	//		}
	//		else{
	//			Utility.getInstance().showAlertDialog(context, AppConstants.NO_CONNECTIVITY);
	//		}
	//	}

	public void callOutboundOrdersVolleyRequest(Context context, String inputvalue) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				String url = URLFactory.getOutboundOrdersByQueue_URL(context, inputvalue);
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						OrdersScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	public void callOutboundOrdersDetailsVolleyRequest(Context context, String inputvalue) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				String url = URLFactory.getOutboundOrderDetailsByQueue_URL(context, inputvalue);
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						OrdersScreen.class.getMethod("onDetailResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	public void callOutboundProcessorOrdersVolleyRequest(Context context, String url) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						ByProcessorScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

	public void callOutboundTasksVolleyRequest(Context context, String wHOrder, java.lang.reflect.Method callBackMethod) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			String url = URLFactory.getOutboundPickingTasks_URL(context, wHOrder);
			new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context), callBackMethod);
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}
	
	public void callOutboundConsolidationTasksVolleyRequest(Context context, java.lang.reflect.Method callBackMethod) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			String url = URLFactory.getOutboundConfirmTasks_URL(context, Resource.getSelectedResource());
			new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context), callBackMethod);
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}

}
