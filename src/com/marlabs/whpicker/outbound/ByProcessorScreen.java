package com.marlabs.whpicker.outbound;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.adapters.OrderAdapter;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.outbound.services.OutboundBussinessClass;
import com.marlabs.whpicker.tparsers.OutboundParser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Utility;

public class ByProcessorScreen extends Activity implements OnRefreshListener  {

	private CustomActionBar actionBar;
	private ListView mListView;
	private LinearLayout mLinearLayout;
	private List<Order> mInboundOrdersList;
	private SwipeRefreshLayout swipeLayout;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		setContentView(R.layout.activity_list);
		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			if(bundle.get(AppConstants.SELECTED_MENU) != null){
				HomeMenuItem homeMenuItem = (HomeMenuItem) bundle.get(AppConstants.SELECTED_MENU);
				actionBar.changeHeader(homeMenuItem.getItemName());
			}
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}
		mLinearLayout = (LinearLayout) findViewById(R.id.queueslist_headers);
		mLinearLayout.setVisibility(View.GONE);
		swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeLayout.setOnRefreshListener(this);
		swipeLayout.setColorScheme(android.R.color.holo_blue_bright, 
				android.R.color.holo_green_light, 
				android.R.color.holo_orange_light, 
				android.R.color.holo_red_light);
		mListView = (ListView) findViewById(R.id.pulltorefresh_listview);
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(ByProcessorScreen.this, ResourcesScreen.class);
				intent.putExtra(AppConstants.OUTBOUND_ORDERS, mInboundOrdersList.get(position));
				startActivity(intent);
			}
		});
	}
	
	@Override 
	public void onRefresh() {
		swipeLayout.setRefreshing(false);
		callVolleyRequest();
	}

	private void callVolleyRequest() {
		String url = URLFactory.getOutboundOrdersByProcessor_URL(this);
		OutboundBussinessClass.getInstance().callOutboundProcessorOrdersVolleyRequest(this, url);
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			System.out.println("------ * response * -----" + jsonObject.toString());
			mInboundOrdersList = OutboundParser.parseOutboundOrdersResult(jsonObject);
			if(mInboundOrdersList.size() == 0){
				Utility.getInstance().showAlertDialog(ByProcessorScreen.this, getString(R.string.no_orders_details_in_selected_queue));
			}
			else{
				mListView.setAdapter(new OrderAdapter(ByProcessorScreen.this, mInboundOrdersList));
			}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(ByProcessorScreen.this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main_picker_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;
		case R.id.action_settings_item:
			Utility.getInstance().showSettingsDialog(ByProcessorScreen.this);
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_right_out, R.anim.right_left_out);
		super.onBackPressed();
	}


}