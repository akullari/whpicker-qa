package com.marlabs.whpicker.outbound;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.adapters.QueueAdapter;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Queue;
import com.marlabs.whpicker.networking.UserInfo;
import com.marlabs.whpicker.outbound.services.OutboundBussinessClass;
import com.marlabs.whpicker.screens.HomeActivity;
import com.marlabs.whpicker.tparsers.OutboundParser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;

public class ByQueuesScreen extends Activity implements OnRefreshListener{
	private CustomActionBar actionBar;
	private ListView mListView;
	private SwipeRefreshLayout swipeLayout;
	private TextView mTextViewUser, mTextViewWarehouse;
	private int mParentID;
	private List<Queue> mQueuesList;
	private List<Order> mOrderList;
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			if(bundle.get(AppConstants.SELECTED_MENU) != null){
				HomeMenuItem homeMenuItem = (HomeMenuItem) bundle.get(AppConstants.SELECTED_MENU);
				actionBar.changeHeader(homeMenuItem.getItemName());
				mParentID = Integer.parseInt(homeMenuItem.getItemId());
			}
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeLayout.setOnRefreshListener(this);
		swipeLayout.setColorScheme(R.color.green, R.color.red, R.color.green, R.color.red);
		mListView = (ListView) findViewById(R.id.pulltorefresh_listview);
		mTextViewUser = (TextView) findViewById(R.id.queueslist_textview_user);
		mTextViewWarehouse = (TextView) findViewById(R.id.queueslist_textview_warehouse);

		mTextViewUser.setText("USER: " + UserInfo.getInstance().getUserID(this));
		mTextViewWarehouse.setText("WH NO. " + UserInfo.getInstance().getWareHouse(this));

		OutboundBussinessClass.getInstance().callQueuesVolleyRequest(this);

		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				TextView textView = (TextView) view.findViewById(R.id.queue);
				String queue = textView.getText().toString();
				Intent intent= new Intent(ByQueuesScreen.this, OrdersScreen.class);
				System.out.println(queue);
				intent.putExtra(AppConstants.OUTBOUND_ORDER_INPUT, queue);
				intent.putExtra(AppConstants.PARENTID, mParentID);
				HomeActivity.sSelectedQueue = queue;
				HomeActivity.sSelectedParentID = mParentID;
				startActivity(intent);
			}
		});
	}


	@Override 
	public void onRefresh() {
		swipeLayout.setRefreshing(false);
		OutboundBussinessClass.getInstance().callQueuesVolleyRequest(this);
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			System.out.println(jsonObject);
			mQueuesList = OutboundParser.parseOutboundQueuesResult(jsonObject);

			/*List Validations*/
			if(mQueuesList.size() == 0){//"No Queues in current WareHouse..!"
				Utility.getInstance().showAlertDialog(ByQueuesScreen.this, getString(R.string.no_queues_in_current_warehouse));
				return;
			}
			else{
				//				if(mParentID == OUTBOUND_QUEUE_ID)
				//					OutboundBussinessClass.getInstance().callAllOutboundOrdersVolleyRequest(this);
				//				else
				//					InboundBussinessClass.getInstance().callAllOrdersVolleyRequest(this);
				mListView.setAdapter(new QueueAdapter(ByQueuesScreen.this, mQueuesList));
			}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(ByQueuesScreen.this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	public void onOrdersResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			//System.out.println("------ * response * -----" + jsonObject.toString());
			mOrderList = OutboundParser.parseOutboundOrdersResult(jsonObject);

			/*List Validations*/
			if(mOrderList.size() == 0){//"No Orders associated with User..!"
				Utility.getInstance().showAlertDialog(ByQueuesScreen.this, getString(R.string.no_orders_associated_with_user));
			}
			else{
				mQueuesList = Util.getWHOCountInQueue(mQueuesList, mOrderList);
				mListView.setAdapter(new QueueAdapter(ByQueuesScreen.this, mQueuesList));
			}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(ByQueuesScreen.this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;

		case R.id.action_settings_item:
			Utility.getInstance().showSettingsDialog(ByQueuesScreen.this);
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_right_out, R.anim.right_left_out);
		super.onBackPressed();
	}
}