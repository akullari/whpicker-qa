package com.marlabs.whpicker.outbound;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.customviews.VoiceMessage;
import com.marlabs.whpicker.models.ConfirmResponse;
import com.marlabs.whpicker.models.ConsolidationItem;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.outbound.services.OutboundSingleton;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.NumericReaderScreen;
import com.marlabs.whpicker.screens.ResourceSelectorScreen;
import com.marlabs.whpicker.tparsers.OutboundParser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class ResourcesScreen extends Activity implements IScannerListener {

	private CustomButton mButton1, mButton2, mButton3, mButton4, mButton5;
	public CustomButton mCurrentFocusedButton, mNextFocusedButton;
	public Drawable mButtonDrawable;
	private TextView mTextViewManual;
	private CustomActionBar actionBar;
	private Order mParentOrder;
	private int slotCapacity = 0;
	//mPSHUCount = 0
	private Button mScanButton;
	private Resource mSelectedResource;
	private VoiceMessage mVoiceMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom_validations);

		mVoiceMessage = new VoiceMessage(this);
		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		StaticScanner.getInstance().scannerRead(this, 1);
		Bundle bundle = getIntent().getExtras();
		if (bundle != null){
			if((bundle.get(AppConstants.OUTBOUND_ORDERS)!=null)){
				mParentOrder = (Order) bundle.getSerializable(AppConstants.OUTBOUND_ORDERS);
				actionBar.changeHeader(Util.removeLeadingZeros(mParentOrder.getWHOrder()));
			}
		} 
		else {
			actionBar.setUpTitle(getString(R.string.home));
			finish();
		}

		mButton1 = (CustomButton) findViewById(R.id.validation_button1);
		mButton2 = (CustomButton) findViewById(R.id.validation_button2);
		mButton3 = (CustomButton) findViewById(R.id.validation_button3);
		mButton4 = (CustomButton) findViewById(R.id.validation_button4);
		mButton5 = (CustomButton) findViewById(R.id.validation_button5);
		mScanButton = (Button) findViewById(R.id.bottomlayout_button_scan);
		mTextViewManual = (TextView) findViewById(R.id.bottomlayout_textview_manual);

		if(mParentOrder != null){
			init();
		}
		else{
			finish();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(mSelectedResource==null)
			mSelectedResource = Resource.getSelectedResource();
		if(null == mSelectedResource || !Validation.isNullString(mSelectedResource.getResourceName())){
			setDataToResourceField();
		}
		else{
			mButton5.setTitleAndText(getString(R.string.resource), mSelectedResource.getResourceName());
		}
	}
	private void init() {
		mScanButton.setText(getString(R.string.scan) +" " + getString(R.string.resource));
		mButton1.setTitleAndText(getString(R.string.order) +" " + getString(R.string.number), Util.removeLeadingZeros(mParentOrder.getWHOrder()));
		mButton2.setTitleAndText(getString(R.string.zone), "");
		mButton3.setTitleAndText(getString(R.string.weight), mParentOrder.getWeight());
		mButton4.setTitleAndText("#PSHU", mParentOrder.getPSHUCount()+"");

		//mPSHUCount = mParentOrder.getPSHUCount();

		mButton1.removeVerification();
		mButton2.removeVerification();
		mButton3.removeVerification();
		mButton4.removeVerification();
		mScanButton.setEnabled(false);
		mTextViewManual.setText(getString(R.string.pick_resource_manually));

		//		mCurrentFocusedButton = null;
		//		mNextFocusedButton = mButton5;
		//		changeValidationCustomButtonFocus();

		mTextViewManual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent  = new Intent(ResourcesScreen.this, ResourceSelectorScreen.class);
				startActivityForResult(intent, 1);
			}
		});

		mScanButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				callResourceAllocationService(ResourcesScreen.this);
			}
		});

		try {
			OutboundSingleton.getInstance().headersTask(this, mParentOrder, 
					ResourcesScreen.class.getMethod("onSelectedResourceFetched", String.class), true);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public void onSelectedResourceFetched(String result) {
		if(!isResourceAllocated)
			mVoiceMessage.speak(getString(R.string.now_scan_resource));
		final String resource = OutboundParser.parseInProcessResponse(result);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(!Validation.isNullString(resource)){
					//setDatatoResourceField(resource);
				}
			}
		});
	}

	private boolean isResourceAllocated = false;
	private void setDataToResourceField() 
	{
		if(null==mSelectedResource){
			slotCapacity=0;
			mButton5.setTitleAndText(getString(R.string.resource), "");
			isResourceAllocated = false;

			mCurrentFocusedButton = null;
			mNextFocusedButton = mButton5;
			changeValidationCustomButtonFocus();
		}
		else{
			slotCapacity = mSelectedResource.getResourceCapacity();
			mButton5.setTitleAndText(getString(R.string.resource), mSelectedResource.getResourceName());
			mButton5.setAsVerified();
			isResourceAllocated=true;

			mScanButton.setEnabled(true);
			mScanButton.setBackgroundColor(getResources().getColor(R.color.green));
			mScanButton.setText(getString(R.string.go_to_tasks_list));
		}

		//Toast.makeText(getApplicationContext(), mResourceName + " --&&-- " + slotCapacity, Toast.LENGTH_SHORT).show();
	}

	public void changeValidationCustomButtonFocus() {
		if(mCurrentFocusedButton != null){
			mCurrentFocusedButton.setBackground(mButtonDrawable);
			mCurrentFocusedButton.setAsVerified();
			mCurrentFocusedButton.clearAnimation();
		}

		if(mNextFocusedButton != null){
			mButtonDrawable = mNextFocusedButton.getBackground();
			mNextFocusedButton.setBackgroundResource(R.drawable.header);
			mNextFocusedButton.startAnimation(Utility.getInstance().blinkAnimation());
			mCurrentFocusedButton = mNextFocusedButton;
			mCurrentFocusedButton.removeVerificationForBlink();
			mTextViewManual.setText(getString(R.string.validate_resource_manually));
		}
		else{
			mCurrentFocusedButton = null;
		}
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		onResourceScanned(scanValue);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if(requestCode == 1){
			if (resultCode == RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				if(isResourceAllocated){
					if(mSelectedResource.getResourceName().equalsIgnoreCase(result)){
						onResourceScanned(result);
					}
					else{
						Util.showMessageDialog(this, getString(R.string.please_release_resource_allocate_new_resource));
					}
				}
				else{
					onResourceScanned(result);
				}
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.manual_entry_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
		else if(requestCode == 2)
		{
			if (resultCode == RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				slotCapacity = Integer.parseInt(result);
				//if(slotCapacity < mPSHUCount){
				//		Util.showMessageDialog(ResourcesScreen.this, "Current resource is insufficient for WHOrder, " +
				//		"please use another Resource");
				//		isResourceAllocated = false;
				//		return;
				//}
				mSelectedResource.setResourceCapacity(slotCapacity);
				callResourceAllocationService(this);
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.manual_entry_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void onResourceScanned(String selectedResourceName) {
		if (Utility.getInstance().getResources().contains(selectedResourceName)) {
			if(isResourceAllocated){
				callResourceAllocationService(this);
			}
			else{
				mButton5.setTitleAndText(getString(R.string.resource), selectedResourceName);
				mSelectedResource = Util.getSelectedObject(AppConstants.getResources(), selectedResourceName);

				Intent intent  = new Intent(ResourcesScreen.this, NumericReaderScreen.class);
				intent.putExtra(AppConstants.NUMERIC_KEYPAD_ISFORRESOURCE,true);
				intent.putExtra(AppConstants.NUMERIC_KEYPAD_INPUT,selectedResourceName);
				startActivityForResult(intent, 2);

				//				if(mSelectedResource.getResourceType() == Resource.RPT_NO_PARING){
				//					callResourceAllocationService();
				//				}
				//				else{
				//					if(isResourceAllocated){
				//						callResourceAllocationService();
				//					}
				//					else{
				//						Intent intent  = new Intent(ResourcesScreen.this, NumericReaderScreen.class);
				//						intent.putExtra(AppConstants.NUMERIC_KEYPAD_ISFORRESOURCE,true);
				//						intent.putExtra(AppConstants.NUMERIC_KEYPAD_INPUT,selectedResourceName);
				//						startActivityForResult(intent, 2);
				//					}
				//				}
			}
		}
		else{
			mVoiceMessage.speak(getString(R.string.wrong_resource_please_repeat));
			mCurrentFocusedButton.setAsWrongVerified();
			StaticScanner.getInstance().scannerRead(this, 1);
			return;
		}
	}

	private void callResourceAllocationService(Context context) {
		if(mSelectedResource != null){
			Resource.selectResource(context, mSelectedResource);
			//			Resource.setSelectedResource(ResourcesScreen.this, mSelectedResource);
		}
		try {
			OutboundSingleton.getInstance().allocateResource(
					this, ResourcesScreen.class.getMethod("onResourceAllocated", ConfirmResponse.class), mParentOrder);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public void onResourceAllocated(ConfirmResponse confirmResponse) {
		int respCode = confirmResponse.getResponseCode();
		if (respCode >= 200 && respCode < 300) {
			changeValidationCustomButtonFocus();
			openTasksListActivity();
		}
		//else if(respCode == 401 || Validation.isNullString(confirmResponse.getResponseLine())) {
		else if(respCode == 401) {
			Toast.makeText(getApplicationContext(), "Session Expired", Toast.LENGTH_SHORT).show();
			Util.userLogout(this);
		}
		else{
			StaticScanner.getInstance().scannerRead(this, 1);
			Util.showMessageDialog(this, confirmResponse.getResponseLine());
		}
	}

	private void openTasksListActivity() {
		Intent intent = new Intent(ResourcesScreen.this, TaskScreen.class);
		intent.putExtra(AppConstants.OUTBOUND_ORDERS, mParentOrder);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;

		case R.id.action_settings_item:
			Utility.getInstance().showSettingsDialog(ResourcesScreen.this);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mVoiceMessage.destroySpeechEngine();
	}
	
	public void onResourceTasksResponse(JSONObject jsonObject, VolleyError volleyError) 
	{
		System.out.println("Tasks Fetched"+ jsonObject.toString());
		if(volleyError == null){
			List<ConsolidationItem> consolidationItemsList = OutboundParser.parsePackingItemsResult(jsonObject);
			if(consolidationItemsList.size() == 0){
				return;
			}
			int logicalPosition = 0;
			for(ConsolidationItem consolidationItem: consolidationItemsList){
				int lastSlotNumber=Resource.getSelectedResource().getLastOccupiedSlotNumber();
				Resource.getSelectedResource().addTask(consolidationItem);
				if(!Validation.isNullString(consolidationItem.getSlot()))
					logicalPosition = Integer.parseInt(consolidationItem.getSlot());

				if(lastSlotNumber>logicalPosition)
					lastSlotNumber = logicalPosition;
			}
			Resource.getSelectedResource().setLastOccupiedSlotNumber(logicalPosition);
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
		}
	}
}