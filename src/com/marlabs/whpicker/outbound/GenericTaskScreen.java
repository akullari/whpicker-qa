package com.marlabs.whpicker.outbound;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.models.ConfirmResponse;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.KeyPadReaderScreen;
import com.marlabs.whpicker.screens.NumericReaderScreen;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;

public class GenericTaskScreen extends Activity implements IScannerListener  {

	private CustomButton mButton1, mButton2, mButton3, mButton4, mButton5;
	private Button mButtonScan;
	public CustomButton mCurrentFocusedButton, mNextFocusedButton;
	public Drawable mButtonDrawable;
	private TextView mTextViewManual;
	private CustomActionBar actionBar;
	private boolean isHWUT = false;
	private int mTasksCount = 0, index = 0;
	private LinearLayout linearLayoutParent;
	private List<Task> mOutboundTasks, mResultTasks;
	private Task mCurrentTask;
	private Order mParentOrder;
	private Animation animation1, animation2;
	private List<CustomButton> mValidationButtonsList = new ArrayList<CustomButton>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom_validations);

		animation1 = AnimationUtils.loadAnimation(this, R.anim.fliptomiddle);
		animation2 = AnimationUtils.loadAnimation(this, R.anim.flipfrommiddle);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mResultTasks = new ArrayList<Task>();

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			mParentOrder = (Order) bundle.getSerializable(AppConstants.OUTBOUND_ORDERS);
			actionBar.changeHeader(mParentOrder.getWHOrder());
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		StaticScanner.getInstance().scannerRead(this, 1);
		mOutboundTasks = mParentOrder.getTasksList();
		switchViews();
	}

	private void switchViews(){
		mButton1 = (CustomButton) findViewById(R.id.validation_button1);
		mButton2 = (CustomButton) findViewById(R.id.validation_button2);
		mButton3 = (CustomButton) findViewById(R.id.validation_button3);
		mButton4 = (CustomButton) findViewById(R.id.validation_button4);
		mButton5 = (CustomButton) findViewById(R.id.validation_button5);
		linearLayoutParent = (LinearLayout) findViewById(R.id.linearlayout_validation);
		mTextViewManual = (TextView) findViewById(R.id.bottomlayout_textview_manual);
		mButtonScan = (Button) findViewById(R.id.bottomlayout_button_scan);

		animation1.setAnimationListener(animationListener);
		animation2.setAnimationListener(animationListener);

		mButton1.setOnClickListener(validationButonClickLisener);
		mButton2.setOnClickListener(validationButonClickLisener);
		mButton3.setOnClickListener(validationButonClickLisener);
		mButton4.setOnClickListener(validationButonClickLisener);
		mButton5.setOnClickListener(validationButonClickLisener);

		mTextViewManual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mCurrentFocusedButton == mButton3  && !isHWUT){
					Intent intent  = new Intent(GenericTaskScreen.this, NumericReaderScreen.class);
					intent.putExtra(AppConstants.NUMERIC_KEYPAD_INPUT, mCurrentTask.getTotalQuantity());
					startActivityForResult(intent, 1);
				}
				else{
					String input;
					if(mCurrentFocusedButton == mButton2 && !isHWUT){
						input = mCurrentTask.getBatch();
					}
					else{
						input = "";
					}
					Intent intent  = new Intent(GenericTaskScreen.this, KeyPadReaderScreen.class);
					intent.putExtra(AppConstants.ALPHA_KEYPAD_TITLE, mTextViewManual.getText().toString());
					intent.putExtra(AppConstants.ALPHA_KEYPAD_BATCHINPUT, input);
					startActivityForResult(intent, 1);
				}
			}
		});

		mButtonScan.setEnabled(false);
		mButtonScan.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				//callConfirmTaskService();
			}
		});

		setDataTovalidation(mTasksCount);
	}

	OnClickListener validationButonClickLisener = new OnClickListener() {
		@SuppressLint("NewApi")
		@Override
		public void onClick(View view) {
			CustomButton button = (CustomButton) view;
			if(mCurrentFocusedButton != null){
				mCurrentFocusedButton.setBackground(mButtonDrawable);
				mCurrentFocusedButton.clearAnimation();
				mCurrentFocusedButton.setAsNotVerified();
			}
			button.setAsNotVerified();
			mCurrentFocusedButton = null;
			mNextFocusedButton = button;
			changeValidationCustomButtonFocus();
			index--;
		}
	};

	AnimationListener animationListener = new AnimationListener() {
		@Override
		public void onAnimationStart(Animation animation) {}
		@Override
		public void onAnimationRepeat(Animation animation) {}
		@Override
		public void onAnimationEnd(Animation animation) {
			if (animation==animation1) {
				(linearLayoutParent).clearAnimation();
				(linearLayoutParent).setAnimation(animation2);
				(linearLayoutParent).startAnimation(animation2);
			} 
		}
	};

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
			if (resultCode== RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				onManualDialogClosed(result);
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.manual_entry_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void onManualDialogClosed(String result){
		if(mCurrentFocusedButton == mButton2 && !isHWUT)
			switchButtons();
		else if(mCurrentFocusedButton == mButton5 && !isHWUT){
			mButton5.setTitleAndText("Pick HU", result);
			//			mCurrentTask.setPickHU(result);
			//			try {
			//				OutboundSingleton.getInstance().createToat(GenericTaskScreen.this, 
			//						GenericTaskScreen.class.getMethod("onToatCreated", ConfirmResponse.class), mCurrentTask, mParentOrder);
			//			} catch (NoSuchMethodException e) {
			//				e.printStackTrace();
			//			}
		}
		else{
			if(mCurrentFocusedButton.getTag().toString().equalsIgnoreCase(result))
				switchButtons();
			else
				mCurrentFocusedButton.setAsWrongVerified();
		}
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	public void onToatCreated(ConfirmResponse confirmResponse){
		int respCode = confirmResponse.getResponseCode();
		if(respCode >= 200 && respCode < 300){
			switchButtons();
			System.out.println("Toat Created Successfully.!");
		}
		else{
			Util.showMessageDialog(this, confirmResponse.getResponseLine());
		}
	}

	public void changeValidationCustomButtonFocus(){
		if(mCurrentFocusedButton != null){
			mCurrentFocusedButton.setBackground(mButtonDrawable);
			mCurrentFocusedButton.setAsVerified();
			mCurrentFocusedButton.clearAnimation();
		}

		if(mNextFocusedButton != null){
			mButtonDrawable = mNextFocusedButton.getBackground();
			mNextFocusedButton.setBackgroundResource(R.drawable.header);
			mNextFocusedButton.startAnimation(Utility.getInstance().blinkAnimation());
			mCurrentFocusedButton = mNextFocusedButton;
			mCurrentFocusedButton.removeVerificationForBlink();
			mTextViewManual.setText("Enter "+ mCurrentFocusedButton.getTitle() +" Manually");
		}
		else{
			mCurrentFocusedButton = null;
		}
	}

	private void switchButtons() {
		index = index+1;
		if(index < mValidationButtonsList.size()){
			mNextFocusedButton = mValidationButtonsList.get(index);
			mValidationButtonsList.get(index).setEnabled(true);
		}
		else{
			mNextFocusedButton = null;
			mResultTasks.add(mCurrentTask);
			if(mTasksCount < mOutboundTasks.size()){
				CustomButton lastButton = mValidationButtonsList.get(mValidationButtonsList.size()-1);
				lastButton.setBackground(mButtonDrawable);
				lastButton.setAsVerified();
				lastButton.clearAnimation();
				setDataTovalidation(mTasksCount);
			}
			else{
				mParentOrder.setTasksList(mResultTasks);
				Intent intent = new Intent(GenericTaskScreen.this, ConsolidationScreen.class);
				intent.putExtra(AppConstants.OUTBOUND_ORDERS, mParentOrder);
				startActivity(intent);
				finish();
				//Utility.getInstance().showAlertDialog(this, "Task Confirmed Successfully.!");
			}
		}
		changeValidationCustomButtonFocus();
	}

	private void setDataTovalidation(int taskNumber) {

		actionBar.changeHeader("Task "+ (taskNumber+1) +" of " + mOutboundTasks.size());
		Task task = mOutboundTasks.get(taskNumber);
		mCurrentTask = task;

		if(task.isValidTask()){
			mTasksCount++;
			setDataTovalidation(mTasksCount);
			return;
		}
		else{
			Util.showMessageDialog(this, task.getErrorMessage());
		}

		if(taskNumber != 0){
			linearLayoutParent.clearAnimation();
			linearLayoutParent.setAnimation(animation1);
			linearLayoutParent.startAnimation(animation1);
		}

		mButton1.setAsVerified();
		mButton2.setAsNotVerified();
		mButton3.setAsNotVerified();
		mButton4.setAsNotVerified();
		mButton5.setAsNotVerified();

		mButton1.setEnabled(true);
		mButton2.setEnabled(false);
		mButton3.setEnabled(false);
		mButton4.setEnabled(false);
		mButton5.setEnabled(false);

		mButton1.setTitleAndText("Source Bin", task.getSourceBin());
		mButton1.setTag(task.getSourceBin());
		if(!task.getSourceBin().isEmpty())
			mValidationButtonsList.add(mButton1);

		if(task.getHandlingUnitWarehouseTask().equalsIgnoreCase("X")){
			isHWUT = true;

			mButton2.setTitleAndText("HU", Util.removeLeadingZeros(task.getHandlingUnit()));
			mButton2.setTag(task.getHandlingUnit());
			if(!task.getHandlingUnit().isEmpty())
				mValidationButtonsList.add(mButton2);

			//			mButton3.setTitleAndText("Pick HU", Util.removeLeadingZeros(task.getPickHU()));
			//			mButton3.setTag(task.getPickHU());
			//if(!inboundTask.getPickHU().isEmpty())
			mValidationButtonsList.add(mButton3);

			mButton4.setTitleAndText("Destination Bin", task.getDestinationBin());
			mButton4.setTag(task.getDestinationBin());
			if(!task.getDestinationBin().isEmpty())
				mValidationButtonsList.add(mButton4);

			mButton5.setVisibility(View.GONE);
		}
		else{
			isHWUT = false;
			mButton2.setTitleAndText("Product", task.getProduct());
			mButton2.setTag(task.getProduct());
			if(!task.getProduct().isEmpty())
				mValidationButtonsList.add(mButton2);

			mButton3.setTitleAndText("Qunatity", task.getTotalQuantity()+" "+task.getQuantityUoM());
			mButton3.setTag(task.getTotalQuantity());
			//if(!task.getTotalQuantity().isEmpty())
				mValidationButtonsList.add(mButton3);

			mButton4.setTitleAndText("Slot ID", "");
			//mValidationButtonsList.add(mButton4);

			//			mButton5.setTitleAndText("Pick HU ", task.getPickHU());
			//			mButton5.setTag(task.getPickHU());
			//if(!inboundTask.getPickHU().isEmpty())
			mValidationButtonsList.add(mButton5);
		}

		mCurrentFocusedButton = null;
		mNextFocusedButton = mValidationButtonsList.get(0);
		changeValidationCustomButtonFocus();
		mTasksCount++;
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		onManualDialogClosed(scanValue);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;

		case R.id.action_settings_item:
			Utility.getInstance().showSettingsDialog(GenericTaskScreen.this);
			break;
		}
		return super.onOptionsItemSelected(item);	
	}
}