package com.marlabs.whpicker.outbound;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.VoiceMessage;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.outbound.services.OutboundBussinessClass;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.HomeActivity;
import com.marlabs.whpicker.tparsers.OutboundParser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class ByWHOScreen extends Activity implements IScannerListener {

	private CustomActionBar actionBar;
	private EditText mEditText;
	private VoiceMessage mVoiceMessage;
	private List<Order> orderObjectList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mVoiceMessage = new VoiceMessage(this);
		init();
	}

	private void init(){
		setContentView(R.layout.activity_scanpo);
		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			if(bundle.get(AppConstants.SELECTED_MENU) != null){
				HomeMenuItem homeMenuItem = (HomeMenuItem) bundle.get(AppConstants.SELECTED_MENU);
				actionBar.changeHeader(homeMenuItem.getItemName());
			}
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
			finish();
		}
		HomeActivity.sSelectedQueue = null;
		HomeActivity.sSelectedParentID = 0;
		StaticScanner.getInstance().scannerRead(this, 1);
		Utility.getInstance().showKeyboard(this);
		final TextView textViewLabel = (TextView) findViewById(R.id.scanpo_textview_title);
		textViewLabel.setText(getString(R.string.please_scan_or_enter_wo));//
		Button buttonSubmit = (Button) findViewById(R.id.scanpo_button_submit);
		Button buttonReset = (Button) findViewById(R.id.scanpo_button_reset);
		mEditText = (EditText) findViewById(R.id.scanpo_edittext_code);

		buttonSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String value = mEditText.getText().toString();
				if(Validation.isNullString(value)){
					Toast.makeText(getApplicationContext(), getString(R.string.please_enter_wo), Toast.LENGTH_SHORT).show();
					return;
				}

				callOdrersVolleyRequest(value);
			}
		});

		buttonReset.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mEditText.setText("");
			}
		});

		Intent checkTTSIntent = new Intent();
		checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkTTSIntent, 0);
		Thread threadTimer = new Thread() {
			public void run() {
				try {
					sleep(2000);
					mVoiceMessage.speak(getString(R.string.scan_warehouse_order));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		threadTimer.start();
	}

	private void callOdrersVolleyRequest(String input) {
		String url;
		input = Util.removeLeadingZeros(input);
		input = Util.addLeadingZeros(input, AppConstants.ORDER_NUMBER_LENGTH);
		url = URLFactory.getOutboundOrdersByID_URL(this, input);
		OutboundBussinessClass.getInstance().callWHOorHUVolleyRequest(this, url);
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	public void onOrdersResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			//System.out.println("------ * response * -----" + jsonObject.toString());
			orderObjectList = OutboundParser.parseOutboundWHOResult(jsonObject);

			if(orderObjectList.size() == 0){
				StaticScanner.getInstance().scannerRead(this, 1);
				Toast.makeText(getApplicationContext(), getString(R.string.invalid_wo), Toast.LENGTH_SHORT).show();
				return;
			}
			OutboundBussinessClass.getInstance().callWHODetailsVolleyRequest(this, orderObjectList.get(0).getWHOrder());
		}
	}

	public void onOrdersDetailResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			System.out.println("------ * response * -----" + jsonObject.toString());
			List<Order> ordersDetailsList = OutboundParser.parseOutboundOrdersDetailResult(jsonObject);

			if(ordersDetailsList.size() == 0){
				StaticScanner.getInstance().scannerRead(this, 1);
				Toast.makeText(getApplicationContext(), getString(R.string.invalid_wo), Toast.LENGTH_SHORT).show();
				return;
			}
			Order order = orderObjectList.get(0);
			order.setPSHUCount(ordersDetailsList.get(0).getPSHUCount());
			order.setWeight(ordersDetailsList.get(0).getWeight());
			Intent intent = new Intent(ByWHOScreen.this, ResourcesScreen.class);
			intent.putExtra(AppConstants.OUTBOUND_ORDERS, order);
			startActivity(intent);
			finish();
		}
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		callOdrersVolleyRequest(scanValue);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Utility.getInstance().hideKeyboard(this, mEditText);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;

		case R.id.action_settings_item:
			Utility.getInstance().hideKeyboard(ByWHOScreen.this, mEditText);
			Utility.getInstance().showSettingsDialog(ByWHOScreen.this);
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mVoiceMessage.destroySpeechEngine();
	}
}