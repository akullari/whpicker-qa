package com.marlabs.whpicker.outbound;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.adapters.OrderAdapter;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.VoiceMessage;
import com.marlabs.whpicker.models.ConsolidationItem;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.outbound.services.OutboundBussinessClass;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.HomeActivity;
import com.marlabs.whpicker.screens.NumericReaderScreen;
import com.marlabs.whpicker.screens.ResourceSelectorScreen;
import com.marlabs.whpicker.tparsers.OutboundParser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class OrdersScreen extends Activity implements OnRefreshListener, IScannerListener{
	private CustomActionBar actionBar;
	public static String order;
	private ListView mListView;
	private LinearLayout mLinearLayout;
	private Button mButtonResource;
	private List<Order> mOrderList;
	private String mParentQueue, mResourceName;
	private SwipeRefreshLayout swipeLayout;
	boolean isResourceAllocated = false;
	//private SharedPreferences sharedPreferences;
	private VoiceMessage mVoiceMessage;
	//INBOUND_QUEUE_ID = 131
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		StaticScanner.getInstance().scannerRead(this, 1);
		mVoiceMessage = new VoiceMessage(this);
		//sharedPreferences = getSharedPreferences(AppConstants.RESOURCE_PREFERENCE, MODE_PRIVATE);

		actionBar = new CustomActionBar(getActionBar(), this, "");
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeLayout.setOnRefreshListener(this);
		swipeLayout.setColorScheme(android.R.color.holo_blue_bright, android.R.color.holo_green_light, 
				android.R.color.holo_orange_light, android.R.color.holo_red_light);
		mListView = (ListView) findViewById(R.id.pulltorefresh_listview);

		mButtonResource = (Button) findViewById(R.id.listactivity_button_scan);
		mButtonResource.setVisibility(View.VISIBLE);
		mButtonResource.setText(getString(R.string.select_scan_resource));
		mLinearLayout = (LinearLayout) findViewById(R.id.queueslist_headers);
		mLinearLayout.setVisibility(View.GONE);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			mParentQueue = bundle.getString(AppConstants.OUTBOUND_ORDER_INPUT);
		}
		else{
			if(Validation.isNullString(HomeActivity.sSelectedQueue)){
				finish();
				return;
			}
			mParentQueue = HomeActivity.sSelectedQueue;
		}
		actionBar.changeHeader(mParentQueue);

		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				{
					final Order order = mOrderList.get(position);
					int resourceCapacity = 0;
					//int lastOccupiedSlot = sharedPreferences.getInt(AppConstants.RESOURCE_LAST_OCCUPIED_SLOT, 0);
					int occupiedSlots = 0;
					if(Resource.getSelectedResource() != null){
						occupiedSlots = Resource.getSelectedResource().getAccomadatedTasks().size();
						resourceCapacity = Resource.getSelectedResource().getResourceCapacity();
					}
					//					int occupiedSlots=0;
					//					if(null!= selectedResource && null!=Resource.getAccomadatedTasks(OrdersScreen.this, selectedResource.getResourceName()))
					//						 occupiedSlots= Resource.getAccomadatedTasks(OrdersScreen.this, selectedResource.getResourceName()).size();
					int remainingEmptySlots = resourceCapacity - occupiedSlots;
					int orderPSHUCount = order.getPSHUCount();
					if(remainingEmptySlots > orderPSHUCount || (!isResourceAllocated))
					{
						Intent intent = new Intent(OrdersScreen.this, ResourcesScreen.class);
						intent.putExtra(AppConstants.OUTBOUND_ORDERS, order);
						startActivity(intent);
					}
					else{
						new AlertDialog.Builder(OrdersScreen.this).setTitle(getString(R.string.release_resource))
						.setMessage(getString(R.string.current_resource_is_insufficient_for_whorder_do_you_want_to_release_allocated_resource))
						.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
								Intent intent  = new Intent(OrdersScreen.this, ConsolidationScreen.class);
								startActivity(intent);
							}
						})
						.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
								Intent intent = new Intent(OrdersScreen.this, ResourcesScreen.class);
								intent.putExtra(AppConstants.OUTBOUND_ORDERS, order);
								startActivity(intent);
							}
						}).show();
					}
				}
			}
		});

		mButtonResource.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isResourceAllocated){
					Intent intent  = new Intent(OrdersScreen.this, ConsolidationScreen.class);
					startActivity(intent);
				}
				else{
					Intent intent  = new Intent(OrdersScreen.this, ResourceSelectorScreen.class);
					startActivityForResult(intent, 1);
				}
			}
		});
		callVolleyRequest();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(Resource.getSelectedResource() != null){
			isResourceAllocated = true;
			mButtonResource.setBackgroundColor(OrdersScreen.this.getResources().getColor(R.color.green));
			mButtonResource.setText(getString(R.string.go_to_packing) + "("+Resource.getSelectedResource().getResourceName()+")");
		}
		//callVolleyRequest();
	}

	@Override 
	public void onRefresh() {
		swipeLayout.setRefreshing(false);
		callVolleyRequest();
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
			if (resultCode == RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				onResourceScanned(result);
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.manual_entry_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
		else if(requestCode == 2){
			if (resultCode == RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				onSlotCountFetched(Integer.parseInt(result));
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.manual_entry_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void onResourceScanned(String resourceName) 
	{
		mResourceName = resourceName;
		Resource selectedResource = Util.getSelectedObject(AppConstants.getResources(), mResourceName);
		if(selectedResource == null){
			Toast.makeText(this, getString(R.string.wrong_resource_please_repeat),  Toast.LENGTH_SHORT).show();
			mVoiceMessage.speak(getString(R.string.wrong_resource_please_repeat));
			StaticScanner.getInstance().scannerRead(this, 1);
			return;
		}
		//		if(selectedResource.getResourceType() == Resource.RPT_NO_PARING){
		//			selectedResource.setSlotsCapacity(Integer.MAX_VALUE);
		//			Resource.setSelectedResource(OrdersScreen.this, selectedResource);
		//			isResourceAllocated = true;
		//			mButtonResource.setBackgroundColor(OrdersScreen.this.getResources().getColor(R.color.green));
		//			mButtonResource.setText("Go To Consolidation ("+selectedResource.getResourceName()+")");
		//		}
		//		else
		{
			Intent intent  = new Intent(OrdersScreen.this, NumericReaderScreen.class);
			intent.putExtra(AppConstants.NUMERIC_KEYPAD_ISFORRESOURCE,true);
			intent.putExtra(AppConstants.NUMERIC_KEYPAD_INPUT,selectedResource.getResourceName());
			startActivityForResult(intent, 2);
		}
	}

	private void onSlotCountFetched(int count){
		Resource selectedResource = Util.getSelectedObject(AppConstants.getResources(), mResourceName);
		selectedResource.setResourceCapacity(count);
		Resource.selectResource(this, selectedResource);
		//Resource.setSelectedResource(OrdersScreen.this, selectedResource);
		isResourceAllocated = true;
		mButtonResource.setBackgroundColor(OrdersScreen.this.getResources().getColor(R.color.green));
		mButtonResource.setText(getString(R.string.go_to_packing) + "("+selectedResource.getResourceName()+")");
	}

	private void callVolleyRequest() {
		OutboundBussinessClass.getInstance().callOutboundOrdersVolleyRequest(this, mParentQueue);
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			if(!isResourceAllocated){
				mVoiceMessage.speak(getString(R.string.now_scan_resource));
			}
			//System.out.println("------ * response * -----" + jsonObject.toString());
			mOrderList = OutboundParser.parseOutboundOrdersResult(jsonObject);

			/*List Validations*/
			if(mOrderList.size() == 0){//AppConstants.NO_ORDERS_IN_SELECTED_QUEUE
				Utility.getInstance().showAlertDialog(OrdersScreen.this, getString(R.string.no_orders_in_selected_queue));
			}
			else{
				order = mOrderList.get(0).getWHOrder();
				OutboundBussinessClass.getInstance().callOutboundOrdersDetailsVolleyRequest(this, mParentQueue);
			}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(OrdersScreen.this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	public void onDetailResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			//System.out.println("------ * response * -----" + jsonObject.toString());
			List<Order> ordersList = OutboundParser.parseOutboundOrdersDetailResult(jsonObject);

			if(mOrderList.size() == 0){
				Utility.getInstance().showAlertDialog(OrdersScreen.this, getString(R.string.no_orders_details_in_selected_queue));
				return;
			}
			else{
				mOrderList = Util.mergeOrders(mOrderList, ordersList);
				mOrderList = Util.sortOrders(mOrderList);
				mListView.setAdapter(new OrderAdapter(OrdersScreen.this, mOrderList));
			}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(OrdersScreen.this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;

		case R.id.action_settings_item:
			Utility.getInstance().showSettingsDialog(OrdersScreen.this);
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_right_out, R.anim.right_left_out);
		super.onBackPressed();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mVoiceMessage.destroySpeechEngine();
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		onResourceScanned(scanValue);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	public void onResourceTasksResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null && jsonObject !=null){
			System.out.println("Tasks Fetched"+ jsonObject.toString());
			List<ConsolidationItem> consolidationItemsList = OutboundParser.parsePackingItemsResult(jsonObject);
			if(consolidationItemsList.size() == 0){
				return;
			}
			int logicalPosition = 0;
			for(ConsolidationItem consolidationItem: consolidationItemsList){
				int lastSlotNumber=Resource.getSelectedResource().getLastOccupiedSlotNumber();
				Resource.getSelectedResource().addTask(consolidationItem);
				if(!Validation.isNullString(consolidationItem.getSlot()))
					logicalPosition = Integer.parseInt(consolidationItem.getSlot());

				if(lastSlotNumber>logicalPosition)
					lastSlotNumber = logicalPosition;
			}
			Resource.getSelectedResource().setLastOccupiedSlotNumber(logicalPosition);
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
		}
	}
}