package com.marlabs.whpicker.networking;

import java.lang.reflect.Method;
import java.util.Map;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.marlabs.whpickerqa.R;

public class VolleyRequestHandler {

	private static RequestQueue mRequestQueue;
	private Context context;
	private ProgressDialog mProgressDialog;

	public VolleyRequestHandler(Context context){
		this.context = context;
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setMessage(context.getString(R.string.please_wait));
		mProgressDialog.setCancelable(false);
	}

	public void callVolleyJSONRequest(final Context context, int methodType, String serverURL, JSONObject jsonObjectInput,
			final Map<String, String> headers, final Method functionName){
		mProgressDialog.show();
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(methodType, serverURL, null, new Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject jsonObject) {
				/* JSON Response on Successful Execution */
				mProgressDialog.dismiss();
				System.out.println("Progress dissmiss");
				try {
					functionName.invoke(context, jsonObject, null);
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				/* Volley Error, will be invoked in case of exceptions */
				mProgressDialog.dismiss();
				try {			
					functionName.invoke(context, null, volleyError);
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
		}){
			/* Parameters to be passed */
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				return headers;
			}

		};
		int socketTimeout = 90000;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		jsonObjectRequest.setRetryPolicy(policy);
		getRequestQueue().add(jsonObjectRequest);
	}
	
	private RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(context);
		}
		return mRequestQueue;
	}
}
