package com.marlabs.whpicker.networking;

import java.net.URLEncoder;

import android.content.Context;
import android.content.SharedPreferences;

import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.utility.AppConstants;

public class URLFactory {

	public static String getSAMLAuthURL(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		return sharedPreferences.getString("SAML_AUTHENTICATION_URL", "");
	}

	public static String getBaseHANAURL(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		return sharedPreferences.getString("BASE_URL_HANA", "");
	}

	public static String getBaseGateWayURL(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		return sharedPreferences.getString("BASE_URL_GATEWAY", "");
	}

	private static String userClient(Context context){
		return UserInfo.getInstance().getClient(context);
	}

	private static String userWareHouseNumber(Context context){
		return UserInfo.getInstance().getWareHouse(context);
	}

	public static String getUserLogin_URL(Context context, String userName) {
		String base = getBaseHANAURL(context) + "ewm/generic/odata/EWMGeneric.xsodata/UserParameterDesc?$format=json&$filter=";
		String URL = "MANDT eq '100' and PARID eq '/SCWM/LGN' and BNAME eq '"+ userName +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getResources_URL(Context context) {
		String userName = UserInfo.getInstance().getUserID(context);
		String base = getBaseHANAURL(context) + "ewm/generic/odata/EWMGenericHelp.xsodata/ResourceValueHelp?$format=json&$orderby=RSRC&$filter=";
		String URL = "MANDT eq '"+ userClient(context) +"' and LGNUM eq '"+ userWareHouseNumber(context) +"' and (UNAME eq '"+ userName +"' or UNAME eq '')";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	/*****************************UNLOADING URLs********************************/
	public static String getUnloadingByTU_URL(Context context, String transportationUnit){
		String base = getBaseHANAURL(context) + "ewm/inbound/odata/Inbound.xsodata/InboundUnloadingByTU?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq 'FR01' " +
				"and TransportationUnit eq '"+ transportationUnit +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getUnloadingByDelivery_URL(Context context, String delivery) {
		String base = getBaseHANAURL(context) + "ewm/inbound/odata/Inbound.xsodata/InboundUnloadingByDelivery?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+"' " +
				"and DeliveryNumber eq '"+ delivery +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getUnloadingByHU_URL(Context context, String handlingUnit) {
		String base = getBaseHANAURL(context) + "ewm/inbound/odata/Inbound.xsodata/InboundUnloadingByHU?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+"' " +
				"and HUIDENT eq '"+ handlingUnit +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getUnloadingByASN_URL(Context context, String asNumber) {
		String base = getBaseHANAURL(context) + "ewm/inbound/odata/Inbound.xsodata/InboundUnloadingByASN?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+"'";
		try{
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	/*****************************PUT AWAY URLs********************************/
	public static String getPutAwayQueues_URL(Context context) {
		String base = getBaseHANAURL(context) + "ewm/generic/odata/EWMGeneric.xsodata/QueueDetails?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+"' " +
				"and (Queue ne 'INB_UNLD' and Queue ne 'INB_STAG' and startswith(Queue,'INB'))";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getInboundOrders_URL(Context context, String queue) {
		String base = getBaseHANAURL(context) + "ewm/generic/odata/EWMGeneric.xsodata/QueueToWHO?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+
				"' and Queue eq '"+queue+"' and (WarehouseOrderStatus eq '"+ AppConstants.EMPTY_STRING 
				+"' or WarehouseOrderStatus eq '"+AppConstants.IN_PROCESS+"')";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	//	public static String getAllInboundOrders_URL(Context context) {
	//		String base = BASE_URL_HANA + "ewm/generic/odata/EWMGeneric.xsodata/QueueToWHO?$format=json&$filter=";
	//		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+
	//				"' and (WarehouseOrderStatus eq '"+ AppConstants.EMPTY_STRING 
	//				+"' or WarehouseOrderStatus eq '"+AppConstants.IN_PROCESS+"')";
	//		try {
	//			URL = base + URLEncoder.encode(URL, "UTF-8");
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//		return URL;
	//	}

	public static String getPutawayTasks_URL(Context context, String who) {
		String base = getBaseHANAURL(context) + "ewm/generic/odata/EWMGeneric.xsodata/InboundTasksOpen?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+"' and WHO eq '"+who
				+"' and (WarehouseTaskStatus eq '"+AppConstants.EMPTY_STRING+"' or WarehouseTaskStatus eq '"+AppConstants.IN_PROCESS+"')";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getPutawayByHU_URL(Context context, String handlingUnit) {
		String base = getBaseHANAURL(context) + "ewm/inbound/odata/Inbound.xsodata/InboundOpenTasksWHOrderHU?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+"' " +
				"and HandlingUnit eq '"+ handlingUnit +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getPutawayByWO_URL(Context context, String wareHosueOrder) {
		String base = getBaseHANAURL(context) + "ewm/generic/odata/EWMGeneric.xsodata/QueueToWHO?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+"' and " +
				"WHO eq '"+ wareHosueOrder +"' and (WarehouseOrderStatus eq '' or WarehouseOrderStatus eq 'D') and " +
				"(Queue ne 'INB_UNLD' and Queue ne 'INB_STAG' and startswith(Queue,'INB'))";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getProcessorOrders_URL(Context context) {
		String base = getBaseHANAURL(context) + "ewm/inbound/odata/Inbound.xsodata/InboundPutawayByQueue?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+
				userWareHouseNumber(context)+"' and  Processor eq '"+ UserInfo.getInstance().getUserID(context)+"' and " + 
				"(Queue ne 'INB_UNLD' and Queue ne 'INB_STAG' and startswith(Queue,'INB'))";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	/*****************************RECEIVE HU URLs********************************/
	public static String getReceiveHUDelivery_URL(Context context, String delivery) {
		String base = getBaseHANAURL(context) + "ewm/generic/odata/EWMGeneric.xsodata/ERPDocuments?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and RefDocCat eq 'ERP' and DocCat eq 'PDI' " +
				"and ReferenceDocNo eq '"+ delivery +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getReceiveHUASN_URL(Context context, String aSNumber) {
		//4400000134-001
		String base = getBaseHANAURL(context) + "ewm/generic/odata/EWMGeneric.xsodata/ERPDocuments?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and RefDocCat eq 'ASN' and DocCat eq 'PDI' " +
				"and ReferenceDocNo eq '"+ aSNumber +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();

		}
		return URL;
	}

	public static String getReceiveHUByTU_URL(Context context, String transportationUnit) {
		// 000000000000001511
		String base = getBaseHANAURL(context) + "ewm/generic/odata/EWMGeneric.xsodata/TransportationUnitDetails?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+"' and " +
				"DocCat eq 'PDI' and TU eq '"+ transportationUnit +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getReceiveHUDetails_URL(Context context, String documentID) {
		//00505682445D1ED488A60152ADA18376
		String base = getBaseHANAURL(context) + "ewm/inbound/odata/Inbound.xsodata/InboundDeliveryHU?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+"' and " +
				"DocumentID eq '"+ documentID +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	/*****************************PRODUCT STATING URLs********************************/
	public static String getPSAList_URL(Context context) {
		String base = getBaseHANAURL(context) + "ewm/ps/odata/ProductStaging.xsodata/ListOfPSA?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+userWareHouseNumber(context)+"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getPSATasks_URL(Context context, String mSelectedPSA) {
		String base = getBaseHANAURL(context) + "ewm/ps/odata/ProductStaging.xsodata/OpenTasksForPSA?$format=json&$filter=";
		String URL = "SAPClient eq '"+userClient(context)+"' and Warehouse eq '"+ userWareHouseNumber(context) +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	/*****************************OUTBOUND URLs********************************/
	public static String getOutboundQueues_URL(Context context) {
		//		String base = BASE_URL_HANA + "ewm/picker/odata/Pickers.xsodata/PickerQueue?$format=json&$filter=";
		//		String URL = "MANDT eq '"+userClient(context)+"' and LGNUM eq '"+userWareHouseNumber(context)+"' " +
		//				"and (QUEUE ne 'OUT_LOAD' and QUEUE ne 'OUT_STAG' and startswith(QUEUE,'OUT'))";
		//		try {
		//			URL = base + URLEncoder.encode(URL, "UTF-8");
		//		} catch (Exception e) {
		//			e.printStackTrace();
		//		}
		String base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerQueue?$filter=";
		String URL = "MANDT eq '"+userClient(context)+"' and LGNUM eq '"+userWareHouseNumber(context)+"' " +
				"and (QUEUE ne 'OUT_LOAD' and QUEUE ne 'OUT_STAG' and startswith(QUEUE,'OUT')) " +
				"and (WHO_STATUS_CA eq '' or WHO_STATUS_CA eq 'D' or WHO_STATUS_CA eq 'Null') " +
				"and (HDR_PROCTY_CA eq '2010' or HDR_PROCTY_CA eq '2011' or HDR_PROCTY_CA eq 'Null')";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8")+"&$format=json&$select=MANDT,LGNUM,QUEUE,TEXT,WHO_COUNT";
		} catch (Exception e) {
			e.printStackTrace();
		}

		return URL;
	}

	//	public static String getAllOutboundOrders_URL(Context context) {
	//		String base = BASE_URL_HANA + "ewm/picker/odata/Pickers.xsodata/PickerWHOrdersQuery?$format=json&$filter=";
	//		String URL = "MANDT eq '"+userClient(context)+"' and LGNUM eq '"+userWareHouseNumber(context)+
	//				"' and (STATUS eq '' or STATUS eq 'D') and (HDR_PROCTY eq '2010' or HDR_PROCTY eq '2011')";
	//		try {
	//			URL = base + URLEncoder.encode(URL, "UTF-8");
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//		return URL;
	//	}

	public static String getOutboundOrdersByQueue_URL(Context context, String queue) {
		String base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerWHOrdersQuery?$format=json&$filter=";
		String URL = "MANDT eq '"+userClient(context)+"' and LGNUM eq '"+userWareHouseNumber(context)+"' and " +
				"QUEUE eq '"+ queue +"' and (STATUS eq '' or STATUS eq 'D') and (HDR_PROCTY eq '2010' or HDR_PROCTY eq '2011')";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getOutboundOrderDetailsByQueue_URL(Context context, String queue) {
		String base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerWHOrdersDetails?$format=json&$filter=";
		String URL = "MANDT eq '"+userClient(context)+"' and LGNUM eq '"+userWareHouseNumber(context)+"' and " +
				"QUEUE eq '"+queue+"' and (STATUS eq '' or STATUS eq 'D') and (HDR_PROCTY eq '2010' or HDR_PROCTY eq '2011')";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8") +"&$select=MANDT,LGNUM,WHO,PSHU_CNT,N_WEIGHT";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getOutboundOrdersByID_URL(Context context, String who) {
		String base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerWHOrdersQuery?$format=json&$filter=";
		String URL = "MANDT eq '"+userClient(context)+"' and LGNUM eq '"+userWareHouseNumber(context)+"' " +
				"and WHO eq '"+ who +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getOutboundWHODetails_URL(Context context, String whOrder) {
		String base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerWHOrdersDetails?$format=json&$filter=";
		String URL = "MANDT eq '"+userClient(context)+"' and LGNUM eq '"+userWareHouseNumber(context)+"' and " +
				"WHO eq '"+whOrder+"' and (STATUS eq '' or STATUS eq 'D') and (HDR_PROCTY eq '2010' or HDR_PROCTY eq '2011')";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8") +"&$select=MANDT,LGNUM,WHO,PSHU_CNT,N_WEIGHT";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	//	public static String getOutboundOrdersByHU_URL(Context context, String handlingUnit) {
	//		String base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerWHOrdersDetails?$format=json&$filter=";
	//		String URL = "MANDT eq '"+userClient(context)+"' and LGNUM eq '"+userWareHouseNumber(context)+"' " +
	//				"and HUIDENT eq '"+ handlingUnit +"'";
	//		try {
	//			URL = base + URLEncoder.encode(URL, "UTF-8");
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//		return URL;
	//	}

	/*http://hanadse.sial.com:8000/sial/sapnext/ewm/picker/odata/Pickers.xsodata/PickerWHTasks?$format=json&$filter=MANDT eq 
			'100' and LGNUM eq 'W015' and WHO eq '0000006025' and FLGHUTO ne 'X'*/
	public static String getOutboundPickingTasks_URL(Context context, String who) {
		String base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerWHTasks?$format=json&$orderby=PATHSEQ&$filter=";
		String URL = "MANDT eq '"+ userClient(context) +"' and LGNUM eq '"+ userWareHouseNumber(context)+"' and WHO eq '"+ who 
				+"' and FLGHUTO ne 'X'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getOutboundConfirmTasks_URL(Context context, Resource resource) {
		String base;
//		base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerWHTasks?$format=json&$orderby=PATHSEQ&$filter=";
		if(resource.getResourceType() == Resource.RPT_NO_PARING)
			base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerWHTasks?$format=json&$orderby=VLENR&$filter=";
		else
			base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerWHTasks?$format=json&$orderby=LOGPOS&$filter=";
		
		String URL = "MANDT eq '"+ userClient(context) +"' and LGNUM eq '"+ userWareHouseNumber(context)
				+"' and FLGHUTO eq 'X' and SRSRC eq '"+ resource.getResourceName() +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}

	public static String getOutboundPickingConfirmTasks_URL(Context context, Task task) {
		String url = URLFactory.getBaseGateWayURL(context) + "odata/sap/ZEWM_CREATE_HU_AND_WTCONFIRM_SRV/WHOpenTaskConfirmSet(WHTask='"+
				task.getWareHouseTask()+"',Warehouse='"+ UserInfo.getInstance().getWareHouse(context) +"')";
		return url;
	}

	public static String getOutboundOrdersByProcessor_URL(Context context) {
		String base = getBaseHANAURL(context) + "ewm/picker/odata/Pickers.xsodata/PickerWHOrdersQuery?$format=json&$filter=";
		String URL = "MANDT eq '"+ userClient(context)+"' and LGNUM eq '"+ userWareHouseNumber(context) +
				"' and PROCESSOR eq '"+ UserInfo.getInstance().getUserID(context) +"'";
		try {
			URL = base + URLEncoder.encode(URL, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return URL;
	}
}