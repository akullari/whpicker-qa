package com.marlabs.whpicker.networking;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.marlabs.whpicker.utility.AppConstants;


public class UserInfo {
	
	private static UserInfo instance = new UserInfo();

	private UserInfo(){}

	public static UserInfo getInstance(){
		return instance;
	}
	
	public String getUserID(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		return sharedPreferences.getString(AppConstants.USER_ID, "");
	}
	
	public String getSelectedEnvironment(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		return sharedPreferences.getString(AppConstants.SELECTED_ENV, "");
	}

	public String getClient(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		return sharedPreferences.getString(AppConstants.USER_CLIENT, "100");
	}

	public String getWareHouse(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		return sharedPreferences.getString(AppConstants.USER_WAREHOUSE_NUMBER, "100");
	}
	
	public String getUserCookies(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		return sharedPreferences.getString("COOKIE", "");
	}

	public String getUserFullName(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		return sharedPreferences.getString(AppConstants.USER_NAME_CONSTANT, "");
	}
	
	public String getAuthorizationHeader(){
		String credentials = AppConstants.USERNAME + ":" + AppConstants.PASSWORD;
		String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
		return "Basic "+base64EncodedCredentials;
	}
}