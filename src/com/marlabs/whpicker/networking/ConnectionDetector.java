package com.marlabs.whpicker.networking;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.marlabs.whpickerqa.R;
 
public class ConnectionDetector {
 
    private static Context _context;
    private  static ConnectionDetector mInstance = new ConnectionDetector();
    private ConnectionDetector(){}
    public static ConnectionDetector getInstance(Context context){
    	_context = context;
		return mInstance;
    }
 
    public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
          if (connectivity != null)
          {
              NetworkInfo[] info = connectivity.getAllNetworkInfo();
              if (info != null)
                  for (int i = 0; i < info.length; i++)
                      if (info[i].getState() == NetworkInfo.State.CONNECTED){
                          return true;
                      }
 
          }
          return false;
    }
}
