package com.marlabs.whpicker.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.HomeMenuItem;

public class MenuDataSorce {

	private static MenuDataSorce sDataSorceInstance = new MenuDataSorce();

	private MenuDataSorce(){}

	public static MenuDataSorce getInstance(){
		System.out.println("Constructor");
		return sDataSorceInstance;
	}

	public List<HomeMenuItem> getMainMenuData(Context context){
		List<HomeMenuItem> menuList = new ArrayList<HomeMenuItem>();
		menuList.add(new HomeMenuItem("1", context.getString(R.string.inbound), R.drawable.inbound_selector));
		menuList.add(new HomeMenuItem("2", context.getString(R.string.outbound), R.drawable.outbound_selector));
		menuList.add(new HomeMenuItem("3", context.getString(R.string.staging_shipping), R.drawable.staging_selector));
		menuList.add(new HomeMenuItem("4", context.getString(R.string.physical_inventory), R.drawable.physicalinventryt_selector));

		return menuList;
	}

	public Map<String, List<HomeMenuItem>> getData(Context context){
		Map<String, List<HomeMenuItem>> menuMap =  new HashMap<String, List<HomeMenuItem>>();

		/*---------------------------- INBOUND ----------------------------*/
		List<HomeMenuItem> menuList1 = new ArrayList<HomeMenuItem>();
		menuList1.add(new HomeMenuItem("11", context.getString(R.string.unloading), R.drawable.unloading_selector));
		menuList1.add(new HomeMenuItem("12", context.getString(R.string.receive_by_hu), R.drawable.cart_selector));
		menuList1.add(new HomeMenuItem("13", context.getString(R.string.put_away), R.drawable.putaway_selector));
		menuList1.add(new HomeMenuItem("14", context.getString(R.string.by_processor), R.drawable.processor_selector));
		menuMap.put("1", menuList1);

		//----- INBOUND -> UNLOADING
		List<HomeMenuItem> menuList11 = new ArrayList<HomeMenuItem>();
		menuList11.add(new HomeMenuItem("111", context.getString(R.string.unloading_by_delivery), R.drawable.unloading_selector));
		menuList11.add(new HomeMenuItem("112", context.getString(R.string.unloading_by_tu), R.drawable.unloading_selector));
		menuList11.add(new HomeMenuItem("113", context.getString(R.string.unloading_by_hu), R.drawable.unloading_selector));
		menuList11.add(new HomeMenuItem("114", context.getString(R.string.unloading_by_asn), R.drawable.unloading_selector));
		menuMap.put("11", menuList11);

		//----- INBOUND -> RECEIVE BY HU
		List<HomeMenuItem> menuList12 = new ArrayList<HomeMenuItem>();
		//menuList12.add(new HomeMenuItem("121","Receive HU by Delivery", R.drawable.cart_selector));
		//menuList12.add(new HomeMenuItem("122","Receive HU by TU", R.drawable.cart_selector));
		//menuList12.add(new HomeMenuItem("123","Receive HU by ASN", R.drawable.cart_selector));
		menuList12.add(new HomeMenuItem("121", context.getString(R.string.receive_hu_by_delivery), R.drawable.cart_selector));
		menuList12.add(new HomeMenuItem("122", context.getString(R.string.receive_hu_by_tu), R.drawable.cart_selector));
		menuList12.add(new HomeMenuItem("123", context.getString(R.string.receive_hu_by_asn), R.drawable.cart_selector));
		menuMap.put("12", menuList12);

		//----- INBOUND -> PUT AWAY
		List<HomeMenuItem> menuList13 = new ArrayList<HomeMenuItem>();
		//menuList13.add(new HomeMenuItem("131","By Queue / Activity Area", R.drawable.putaway_selector));
		//menuList13.add(new HomeMenuItem("132","Put Away by HU", R.drawable.putaway_selector));
		//menuList13.add(new HomeMenuItem("133","Put Away by WO", R.drawable.putaway_selector));
		menuList13.add(new HomeMenuItem("131", context.getString(R.string.by_queue_activity_area), R.drawable.putaway_selector));
		menuList13.add(new HomeMenuItem("132", context.getString(R.string.put_away_by_hu), R.drawable.putaway_selector));
		menuList13.add(new HomeMenuItem("133", context.getString(R.string.put_away_by_wo), R.drawable.putaway_selector));
		menuMap.put("13", menuList13);

		/*---------------------------- OUTBOUND ----------------------------*/
		List<HomeMenuItem> menuList2 = new ArrayList<HomeMenuItem>();
		menuList2.add(new HomeMenuItem("21", context.getString(R.string.by_queue_activity_area), R.drawable.outbound_selector));
		menuList2.add(new HomeMenuItem("22", context.getString(R.string.by_warehouse_order), R.drawable.outbound_selector));
		//menuList2.add(new HomeMenuItem("23","By Handling Unit", R.drawable.cart_selector));
		menuList2.add(new HomeMenuItem("24", context.getString(R.string.by_processor), R.drawable.processor_selector));
		menuList2.add(new HomeMenuItem("25", context.getString(R.string.go_to_packing), R.drawable.processor_selector));
		menuMap.put("2", menuList2);

		/*---------------------------- STAGING / SHIPMENT ----------------------------*/
		List<HomeMenuItem> menuList3 = new ArrayList<HomeMenuItem>();
		//		menuList3.add(new HomeMenuItem("31","Product Staging", R.drawable.staging_selector));
		//		menuList3.add(new HomeMenuItem("32","Palletization", R.drawable.staging_selector));
		//		menuList3.add(new HomeMenuItem("33","Loading", R.drawable.staging_selector));
		//		menuList3.add(new HomeMenuItem("34","AdHoc WT Creation", R.drawable.staging_selector));
		menuList3.add(new HomeMenuItem("31", context.getString(R.string.product_staging), R.drawable.staging_selector));
		menuList3.add(new HomeMenuItem("32", context.getString(R.string.palletization), R.drawable.staging_selector));
		menuList3.add(new HomeMenuItem("33", context.getString(R.string.loading), R.drawable.staging_selector));
		menuList3.add(new HomeMenuItem("34", context.getString(R.string.adhoc_wt_creation), R.drawable.staging_selector));
		menuMap.put("3", menuList3);

		//----- STAGING / SHIPMENT -> PRODUCT STAGING
		List<HomeMenuItem> menuList31 = new ArrayList<HomeMenuItem>();
		//		menuList31.add(new HomeMenuItem("311","Staging by PSA", R.drawable.staging_selector));
		//		menuList31.add(new HomeMenuItem("312","Manual Staging", R.drawable.staging_selector));
		menuList31.add(new HomeMenuItem("311", context.getString(R.string.staging_by_psa), R.drawable.staging_selector));
		menuList31.add(new HomeMenuItem("312", context.getString(R.string.manual_staging), R.drawable.staging_selector));
		menuMap.put("31", menuList31);

		//----- STAGING / SHIPMENT -> PALLETIZATION
		List<HomeMenuItem> menuList32 = new ArrayList<HomeMenuItem>();
		//		menuList32.add(new HomeMenuItem("321","By TU", R.drawable.staging_selector));
		//		menuList32.add(new HomeMenuItem("322","By Customer", R.drawable.staging_selector));
		//		menuList32.add(new HomeMenuItem("323","HU Query", R.drawable.staging_selector));
		menuList32.add(new HomeMenuItem("321",context.getString(R.string.by_tu), R.drawable.staging_selector));
		menuList32.add(new HomeMenuItem("322",context.getString(R.string.by_customer), R.drawable.staging_selector));
		menuList32.add(new HomeMenuItem("323",context.getString(R.string.hu_query), R.drawable.staging_selector));
		menuMap.put("32", menuList32);

		//----- STAGING / SHIPMENT -> LOADING
		List<HomeMenuItem> menuList33 = new ArrayList<HomeMenuItem>();
		//		menuList33.add(new HomeMenuItem("331","Loading by HU", R.drawable.staging_selector));
		//		menuList33.add(new HomeMenuItem("332","Loading by TU", R.drawable.staging_selector));
		//		menuList33.add(new HomeMenuItem("333","Loading by Delivery", R.drawable.staging_selector));
		menuList33.add(new HomeMenuItem("331", context.getString(R.string.loading_by_hu), R.drawable.staging_selector));
		menuList33.add(new HomeMenuItem("332", context.getString(R.string.loading_by_tu), R.drawable.staging_selector));
		menuList33.add(new HomeMenuItem("333", context.getString(R.string.loading_by_delivery), R.drawable.staging_selector));
		menuMap.put("33", menuList33);

		//----- STAGING / SHIPMENT -> ADHOC WT CREATION
		List<HomeMenuItem> menuList34 = new ArrayList<HomeMenuItem>();
		//		menuList34.add(new HomeMenuItem("341","Adhoc HU WT", R.drawable.staging_selector));
		//		menuList34.add(new HomeMenuItem("342","Adhoc Product WT", R.drawable.staging_selector));
		menuList34.add(new HomeMenuItem("341", context.getString(R.string.adhoc_hu_wt), R.drawable.staging_selector));
		menuList34.add(new HomeMenuItem("342", context.getString(R.string.adhoc_product_wt), R.drawable.staging_selector));
		menuMap.put("34", menuList34);

		/*---------------------------- PHYSICAL INVENTORY ----------------------------*/
		List<HomeMenuItem> menuList4 = new ArrayList<HomeMenuItem>();
		//		menuList4.add(new HomeMenuItem("41","By Queue / Activity Area", R.drawable.physicalinventryt_selector));
		//		menuList4.add(new HomeMenuItem("42","By Processor", R.drawable.processor_selector));
		//		menuList4.add(new HomeMenuItem("43","AdHoc phy Inventory", R.drawable.physicalinventryt_selector));
		//		menuList4.add(new HomeMenuItem("44","Write Off/Scrap", R.drawable.physicalinventryt_selector));
		menuList4.add(new HomeMenuItem("41", context.getString(R.string.by_queue_activity_area), R.drawable.physicalinventryt_selector));
		menuList4.add(new HomeMenuItem("42", context.getString(R.string.by_processor), R.drawable.processor_selector));
		menuList4.add(new HomeMenuItem("43", context.getString(R.string.adhoc_phy_inventory), R.drawable.physicalinventryt_selector));
		menuList4.add(new HomeMenuItem("44", context.getString(R.string.write_off_scrap), R.drawable.physicalinventryt_selector));
		menuMap.put("4", menuList4);

		//----- PHYSICAL INVENTORY -> ADHOC PHY INVENTORY
		List<HomeMenuItem> menuList43 = new ArrayList<HomeMenuItem>();
		//		menuList43.add(new HomeMenuItem("431","By Storage Bin", R.drawable.physicalinventryt_selector));
		//		menuList43.add(new HomeMenuItem("432","By Product", R.drawable.physicalinventryt_selector));
		//		menuList43.add(new HomeMenuItem("433","Returnables", R.drawable.physicalinventryt_selector));
		menuList43.add(new HomeMenuItem("431", context.getString(R.string.by_storage_bin), R.drawable.physicalinventryt_selector));
		menuList43.add(new HomeMenuItem("432", context.getString(R.string.by_product), R.drawable.physicalinventryt_selector));
		menuList43.add(new HomeMenuItem("433", context.getString(R.string.returnables), R.drawable.physicalinventryt_selector));
		menuMap.put("43", menuList43);

		//----- PHYSICAL INVENTORY -> WRITE OFF/SCRAP
		List<HomeMenuItem> menuList44 = new ArrayList<HomeMenuItem>();
		//		menuList44.add(new HomeMenuItem("441","Storage Bin", R.drawable.physicalinventryt_selector));
		//		menuList44.add(new HomeMenuItem("442","By Product", R.drawable.physicalinventryt_selector));
		menuList44.add(new HomeMenuItem("441", context.getString(R.string.storage_bin), R.drawable.physicalinventryt_selector));
		menuList44.add(new HomeMenuItem("442", context.getString(R.string.by_product), R.drawable.physicalinventryt_selector));
		menuMap.put("44", menuList44);

		return menuMap;
	}

	public static final int INBOUND_UNLOADING_DELIVERY = 111;
	public static final int INBOUND_UNLOADING_TU = 112;
	public static final int INBOUND_UNLOADING_HU = 113;
	public static final int INBOUND_UNLOADING_ASN = 114;

	public static final int INBOUND_RECEIVEBYHU_DELIVERY = 121;
	public static final int INBOUND_RECEIVEBYHU_TU = 122;
	public static final int INBOUND_RECEIVEBYHU_ASN = 123;

	public static final int INBOUND_PUTAWAY_QUEUE = 131;
	public static final int INBOUND_PUTAWAY_HU = 132;
	public static final int INBOUND_PUTAWAY_WO = 133;

	public static final int INBOUND_PROCESSOR = 14;

	public static final int OUTBOUND_QUEUE = 21;
	public static final int OUTBOUND_WO = 22;
	public static final int OUTBOUND_PROCESSOR = 24;
}