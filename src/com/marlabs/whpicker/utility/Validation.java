package com.marlabs.whpicker.utility;

import com.marlabs.whpickerqa.R;


public class Validation {

	public static boolean isNullString(String string){
		// Returns true for string = null & string = ""
		// Returns false for valid String (string = "value");
		if(string != null && !string.isEmpty())
			return false;
		else
			return true;
	}
}
