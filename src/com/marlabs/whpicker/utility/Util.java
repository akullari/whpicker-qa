
package com.marlabs.whpicker.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Queue;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.networking.UserInfo;
import com.marlabs.whpicker.screens.HomeActivity;

public class Util {

	/*Building Params*/
	public static Map<String, String> getBasicHANAHeaders(Context context) {
		Map<String, String> authParams = new HashMap<String, String>();
		//authParams.put("Authorization", UserInfo.getInstance().getAuthorizationHeader());
		authParams.put("Cookie", UserInfo.getInstance().getUserCookies(context));
		authParams.put("Content-Type", "application/json");
		authParams.put("Accept-Language","en-US" );
		return authParams;
	}

	public static String ellipsize(String input, int maxLength) {
		String ellip = "...";
		if (input == null || input.length() <= maxLength 
				|| input.length() < ellip.length()) {
			return input;
		}
		return input.substring(0, maxLength - ellip.length()).concat(ellip);
	}

	public static void showMessageDialog(final Context context, String message){
		AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
		builder1.setTitle(context.getResources().getString(R.string.app_name));
		if(message.contains("RFC"))
			message = message.replaceAll("RFC", "SAP");
		builder1.setMessage(message);
		builder1.setCancelable(true);
		builder1.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		AlertDialog alert11 = builder1.create();
		alert11.setCancelable(false);
		alert11.show();
	}

	public static String removeLeadingZeros(String value){
		return value.replaceFirst("^0+(?!$)", "");
	}

	public static String addLeadingZeros(String string, int maxLength){
		while(string.length() < maxLength){
			string = "0"+string;
		}
		return string;
	}

	public static int getPatternIndex(String text){
		//index = 1 ---> Start; index = 2 ---> Body; index = 3 ---> End
		int index = 0;
		String startPattern = "--.*", endPattern = "--.*--";
		Pattern patternStart = Pattern.compile(startPattern);
		Pattern patternEnd = Pattern.compile(endPattern);

		if(patternEnd.matcher(text).matches())
			index = 3;
		else  if(patternStart.matcher(text).matches())
			index = 1;
		else
			index = 2;

		return index;
	}

	public static String getDataInBraces(String text){
		if(text.contains("(") && text.contains(")")){
			String splitedString = "";
			Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(text);
			while(m.find()) {
				splitedString = m.group(1);    
			}
			return splitedString;
		}
		else{
			return text;
		}
	}

	public static List<Order> sortOrders(List<Order> ordersList) {
		List<Order> openList = new ArrayList<Order>();
		List<Order> inProcessList = new ArrayList<Order>();
		for(Order order : ordersList){
			if(order.getStatus().equalsIgnoreCase("")){
				openList.add(order);
			}
			else {
				inProcessList.add(order);
			}
		}
		openList.addAll(inProcessList);
		return openList;
	}

	public static List<Queue> getWHOCountInQueue(List<Queue> queuesList, List<Order> orderList) {
		List<Queue> queuesListWithCount = new ArrayList<Queue>();
		for(Queue queue : queuesList){
			String queueName = queue.getQueueName();
			int count = 0;
			for(Order order : orderList){
				if(queueName.equalsIgnoreCase(order.getQueue())){
					count++;
				}
			}
			queue.setCount(count+"");
			queuesListWithCount.add(queue);
		}
		return queuesListWithCount;
	}

	public static List<Order> mergeOrders(List<Order> odrersList, List<Order> ordersDetailList) {
		List<Order> mergedOrders = new ArrayList<Order>();
		for(Order order : odrersList){
			String who = order.getWHOrder();
			for(Order orderDetail : ordersDetailList){
				if(who.equalsIgnoreCase(orderDetail.getWHOrder())){
					order.setPSHUCount(orderDetail.getPSHUCount());
					order.setWeight(orderDetail.getWeight());
				}
			}
			mergedOrders.add(order);
		}
		return mergedOrders;
	}

	public static Resource getSelectedObject(List<Resource> resources, String resourceName) {
		Resource resource = null;
		for(Resource tempResource : resources){
			if(tempResource.getResourceName().equalsIgnoreCase(resourceName)){
				resource = tempResource;
				break;
			}
		}
		return resource;
	}

	public static void userLogout(Context context){
		Intent intent = new Intent(context, HomeActivity.class);
		intent.putExtra(AppConstants.LOGOUT, AppConstants.LOGOUT);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
		((Activity) context).finish();
	}

	private static Drawable mButtonDrawable;
	@SuppressLint("NewApi")
	public static void clearAnimation(CustomButton mCurrentFocusedButton) {
		mCurrentFocusedButton.setBackground(mButtonDrawable);
		mCurrentFocusedButton.setDarkTextColor();
		mCurrentFocusedButton.clearAnimation();
	}

	public static void setAnimation(CustomButton mCurrentFocusedButton) {
		mButtonDrawable = mCurrentFocusedButton.getBackground();
		mCurrentFocusedButton.setAsNotVerified();
		mCurrentFocusedButton.setBackgroundResource(R.drawable.header);
		mCurrentFocusedButton.startAnimation(Utility.getInstance().blinkAnimation());
		mCurrentFocusedButton.removeVerificationForBlink();
	}

	public static void setUserlanguage(Context context, String languageCode) {
		String language = "English";
		if(languageCode.equalsIgnoreCase("E")){
			language = "English";
		}
		//		else if(languageCode.equalsIgnoreCase("GERMAN")){
		//			languageCode = "Deutsch";
		//		}
		Locale locale = new Locale(language);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
	}

	public static void setCurrentResource(Context context, List<Resource> resourcesList) {
		String userName = UserInfo.getInstance().getUserID(context);
		for(Resource resource : resourcesList){
			if(userName.equalsIgnoreCase(resource.getUserName())){
				resource.setResourceCapacity(99);
				Resource.selectResource(context, resource);
				break;
			}
		}
	}

	public static List<String> getToteNames(List<Task> packingTasks) {
		List<String> totes = new ArrayList<String>();
		for(Task task : packingTasks){
			if(Validation.isNullString(task.getModifiedDestinationBin()))
				totes.add(task.getHandlingUnit());
		}
		return totes;
	}
	public static boolean[] getCheckedTotes(List<Task> packingTasks, int index) {
		boolean[] itemsArray = new boolean[packingTasks.size()];
		int approvedItemIndex=0;
		for(Task task : packingTasks)
		{
			if(Validation.isNullString(task.getModifiedDestinationBin()))
			{
				if(packingTasks.indexOf(task) == index)
					itemsArray[packingTasks.indexOf(approvedItemIndex)] = true;
				else
					itemsArray[packingTasks.indexOf(approvedItemIndex)] = false;
				approvedItemIndex++;
			}
		}
		return itemsArray;
	}
}