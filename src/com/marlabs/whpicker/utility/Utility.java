package com.marlabs.whpicker.utility;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.screens.SettingsScreen;

public class Utility {

	private static Utility instance = new Utility();

	private Utility(){}

	public static Utility getInstance(){
		return instance;
	}

	public void hideKeyboard(Context context, EditText editText) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(),0); 
	}

	public void showKeyboard(Context context) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	public void showSettingsDialog(final Context context){
		context.startActivity(new Intent(context, SettingsScreen.class));
		//((Activity) context).overridePendingTransition(R.anim.push_up_in,R.anim.push_up_out);
	}

	//	public void showListSelectorDialog(final Context context, String title, final Method method, List<String> list){
	//		final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
	//		dialog.setContentView(R.layout.custom_selector_dialog_screen);
	//
	//		Button headerButton = (Button) dialog.findViewById(R.id.selectordialog_header);
	//		headerButton.setText(title);
	//		Button dialogButtonCancel = (Button) dialog.findViewById(R.id.selectordialog_button_cancel);
	//		ListView listview = (ListView) dialog.findViewById(R.id.selectordialog_listview); 
	//		listview.setAdapter(new SelectorAdapter(context, list));
	//
	//		listview.setOnItemClickListener(new OnItemClickListener() {
	//			@Override
	//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	//				TextView textView = (TextView) view.findViewById(R.id.customtextview);
	//				try {
	//					method.invoke(context, textView.getText().toString().trim());
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//				dialog.dismiss();
	//			}
	//		});
	//
	//		dialogButtonCancel.setOnClickListener(new OnClickListener(){
	//			@Override
	//			public void onClick(View v){
	//				dialog.dismiss();
	//			}
	//		});
	//		dialog.show();
	//	}

	public List<String> getResources() {
		List<String> list = new ArrayList<String>();
		for(Resource resource : AppConstants.getResources()){
			list.add(resource.getResourceName());
		}
		return list;
	}

	public void removeVerification(Context context, Button button) {
		button.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
		button.setTextColor(context.getResources().getColor(R.color.textcolor_disable));
	}

	public void setAsVerified(Context context, Button button){
		button.setCompoundDrawablesWithIntrinsicBounds(null, null, 
				context.getResources().getDrawable(R.drawable.verified ), null);
	}

	public void setAsNotVerified(Context context, Button button){
		button.setCompoundDrawablesWithIntrinsicBounds(null, null, 
				context.getResources().getDrawable(R.drawable.not_verified), null);
	}

	public void setAsWrongVerified(Context context, Button button){
		button.setCompoundDrawablesWithIntrinsicBounds(null, null, 
				context.getResources().getDrawable(R.drawable.wrong), null);
	}

	public void showAlertDialog(final Context context, String text){
		AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
		builder1.setTitle("Alert");
		builder1.setMessage(text);
		builder1.setCancelable(true);
		builder1.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				((Activity) context).finish();
			}
		});
		AlertDialog alert11 = builder1.create();
		alert11.show();
	}

	public Animation blinkAnimation() {
		Animation mBlinkAnimation = new AlphaAnimation(1, 0); 
		mBlinkAnimation.setDuration(500);
		mBlinkAnimation.setInterpolator(new LinearInterpolator());
		mBlinkAnimation.setRepeatCount(Animation.INFINITE);
		mBlinkAnimation.setRepeatMode(Animation.REVERSE);
		return mBlinkAnimation;
	}
}