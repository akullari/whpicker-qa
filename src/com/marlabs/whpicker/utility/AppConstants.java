package com.marlabs.whpicker.utility;

import java.util.ArrayList;
import java.util.List;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.Resource;



public class AppConstants {

	/*--------- List Constants---------*/
	private static List<Resource> sResources = new ArrayList<Resource>();
	public static List<Resource> getResources() {
		return sResources;
	}
	public static void setResources(List<Resource> resources) {
		sResources = resources;
	}

	/*--------- ENUM Constants---------*/
	public enum RESOURCE_TYPE {
		RT_NO_POSITION(0),
		RT_MANUAL_POSITION(1),
		RT_AUTO_POSITION(2);
		private int type;
		RESOURCE_TYPE(int p) {
			type = p;
		}
		public int getType() {
			return type;
		} 
	} 

	/*--------- String Constants---------*/
	public static final String USERNAME="FIREBALL";

	public static final String PASSWORD="Sigma123";

	public static final String SELECTED_MENU = "selected_menu";

	public static final String HARDWARE_BACK_PRESSED = "hardware_back_pressed";

	public static final String LOGOUT = "Logout";

	public static final String DELIVERYCODE = "deliverycode";

	public static final String DELIVERY_HU_CODE = "delivery_hu_code";

	public static final String TU_CODE = "tucode";

	public static final String HU_CODE_BY_TU = "hucodebytu";

	public static final String PO_CODE = "poCode";

	public static final String HU_CODE = "huCode";

	public static final String WO_CODE = "wo_Code";

	public static final String INBOUND_QUEUE = "Inbound_Queue";

	public static final String INBOUND_ORDER = "Inbound_Order";

	public static final String USER_CLIENT = "client";

	public static final String USER_WAREHOUSE_NUMBER = "warehouseNumber";

	public static final String USER_ID = "userID";

	public static final String USER_NAME_CONSTANT = "userName";

	public static final String USER_LANGUAGE = "language";

	public static final String TASK_DETAILS = "task_details";

	public static final String PARENTID = "parentID";

	public static final String EMPTY_STRING = "";

	public static final String IN_PROCESS = "D";

	public static final String STATUS_WAITING_STRING = "B";

	public static final String STATUS_CONFIRM_STRING = "C";

	public static final String STATUS_CLIENTSIDE_CONFIRM_STRING = "Z";

	public static final String STATUS_BATCH_ERROR = "Y";

	public static final String OUTBOUND_ORDER_INPUT = "outbound_order_input";

	public static final String OUTBOUND_ORDERS = "outbound_orders";

	public static final String SELECTED_ENV = "selected_environment";

	//	public static final String USER = "user";

	public static final String NUMERIC_KEYPAD_INPUT = "numeric_keypad_input";
	public static final String NUMERIC_KEYPAD_INPUT_UNITS = "numeric_keypad_input_units";
	public static final String NUMERIC_KEYPAD_SCANVALUE= "numeric_keypad_scanValue";
	public static final String NUMERIC_KEYPAD_ISFORRESOURCE= "numeric_keypad_isForResource";
	public static final String NUMERIC_KEYPAD_ENTERED_VALUE= "numeric_keypad_entered_value";
	public static final String NUMERIC_KEYPAD_REMAINING_INPUT = "RemainingQuantity";

	public static final String ALPHA_KEYPAD_BATCHINPUT = "alpha_keypad_batchinput";
	public static final String ALPHA_KEYPAD_TITLE = "alpha_keypad_title";
	public static final String ALPHA_KEYPAD_ISNUMERIC = "alpha_keypad_is_numeric";

	//	public static final String LOADING_PLEASEWAIT = "Loading, Please wait...";

	//	public static final String PLEASE_TRY_AGAIN = "Please try again..";

	//	public static final String PLEASEWAIT = "Please wait...";

	//	public static final String ARE_YOU_SURE = "Are you sure?";
	//	public static final String YES = "Yes";
	//	public static final String NO = "No";

	public static final String SETTINGS = "Settings";

	//	public static final String GO_TO_PACKING = "Go To Packing";

	//	public static final String NO_CONNECTIVITY = "No Internet Connectivity.!";

	//	public static final String SERVER_CALL_BACK_ISSUE = "Unable to connect Server, Please try again.";

	//	public static final String NO_ORDERS_IN_SELECTED_QUEUE = "No Orders in selected Queue..!";

	/*--------- Resource Constants---------*/
	//public static final String RESOURCE_PREFERENCE = "resource_preference";
	//	public static final String RESOURCE_NAME = "resourceName";
	//	public static final String RESOURCE_TYPE = "resourceType";
	//	public static final String RESOURCE_TEXT = "resourceText";
	//	public static final String RESOURCE_CAPACITY = "resource_count";
	//	public static final String RESOURCE_LAST_OCCUPIED_SLOT = "resource_last_occupied_slot";

	/*--------- Task Details Constants---------*/
	//	public static final String SOURCE_BIN = "Source Bin";
	//	public static final String HU = "HU";
	//	public static final String PICK_HU = "Pick HU";
	//	public static final String DESTINATION_BIN = "Destination Bin";
	//	public static final String PRODUCT = "Product";
	//	public static final String QUANTITY = "Quantity";
	//	public static final String SLOT_ID = "Slot ID";

	/*--------- Text to Speech Constants---------*/
	public static final String TTS = "Text-To-Speech";
	public static final String TTS_FAILED = "Text-To-Speech Initilization Failed";
	public static final String LANGUAGE_UN_SUPPORTED = "Text-To-Speech Language not supported";
	public static final String TTS_PREFERENCE = "TTS_PreferencE";
	public static final String TTS_ENABLE = "TTS_Enable";

	/*--------- Integer Constants---------*/
	public static final int DIALOG_LISTITEM_ELIPSE_LENGTH = 19;

	public static final int VALIDATION_ELIPSE_LENGTH = 38;

	public static final int SCANNER_REQUEST_CODE = 2;

	public static final int MANUAL_REQUEST_CODE = 3;

	public static final int NUMERIC_INPUT = 1;

	public static final int ALPHA_INPUT = 2;

	public static final int ORDER_NUMBER_LENGTH = 10;

	public static final int HU_LENGTH = 20;

	public static final int TU_LENGTH = 18;

	public static final int PO_LENGTH = 10;

	public static final int DELIVERY_LENGTH = 35;
}