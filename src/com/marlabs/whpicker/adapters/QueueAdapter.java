package com.marlabs.whpicker.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.Queue;

public class QueueAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private List<Queue> mQueuesList;
	private Context context;
	public QueueAdapter(Context context, List<Queue> queuesList) {
		this.mQueuesList = queuesList;
		this.context = context;
		mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mQueuesList.size();
	}

	@Override
	public Queue getItem(int position) {
		return mQueuesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder; 
		if(convertView == null){
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(R.layout.list_row_queues, parent, false);

			viewHolder.textViewQueue = (TextView) convertView.findViewById(R.id.queue);
			viewHolder.textViewQueueText = (TextView) convertView.findViewById(R.id.queueTitle);
			viewHolder.textViewQueueCount = (TextView) convertView.findViewById(R.id.queuecount);
			convertView.setTag(viewHolder);
		}
		else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
		
		if (position %2 ==0) {
			convertView.setBackgroundColor(context.getResources().getColor(R.color.grey_light));
		}
		else{
			convertView.setBackgroundColor(context.getResources().getColor(R.color.grey_dark));
		}
		
		Queue queue = mQueuesList.get(position);
		viewHolder.textViewQueue.setText(queue.getQueueName());
		viewHolder.textViewQueueText.setText(queue.getQueueText());
		viewHolder.textViewQueueCount.setText(queue.getCount());

		return convertView;
	}

	class ViewHolder{
		TextView textViewQueue;
		TextView textViewQueueText;
		TextView textViewQueueCount;
	}
}