package com.marlabs.whpicker.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.HomeMenuItem;

public class HomeMenuAdapter extends BaseAdapter{
	private LayoutInflater mLayoutInflater;
	private List<HomeMenuItem> mMenuList;

	public HomeMenuAdapter(Context context, List<HomeMenuItem> menuList) {
		mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mMenuList = menuList;
	}

	@Override
	public int getCount() {
		return mMenuList.size();
	}

	@Override
	public HomeMenuItem getItem(int position) {
		return mMenuList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder; 
		if(convertView == null){
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(R.layout.listitem_home_menu, parent, false);

			viewHolder.textView = (TextView) convertView.findViewById(R.id.listitem_text);
			viewHolder.imageView = (ImageView) convertView.findViewById(R.id.listitem_menuicon);

			convertView.setTag(viewHolder);
		}
		else{
			viewHolder = (ViewHolder)convertView.getTag();
		}

		viewHolder.textView.setText(mMenuList.get(position).getItemName());
		viewHolder.imageView.setBackgroundResource(mMenuList.get(position).getItemImageId());

		return convertView;
	}

	class ViewHolder{
		TextView textView;
		ImageView imageView;
	}
}
