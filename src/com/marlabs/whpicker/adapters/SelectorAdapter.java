package com.marlabs.whpicker.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.marlabs.whpickerqa.R;

public class SelectorAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private List<String> list;
	private Context context;
	public SelectorAdapter(Context context, List<String> list) {
		this.list = list;
		mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public String getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder; 
		if(convertView == null){
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(R.layout.custom_textview, parent, false);
			viewHolder.textView = (TextView) convertView.findViewById(R.id.customtextview);
			convertView.setTag(viewHolder);
		}
		else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
		
		if (position %2 ==0) {
			convertView.setBackgroundColor(context.getResources().getColor(R.color.grey_light));
		}
		else{
			convertView.setBackgroundColor(context.getResources().getColor(R.color.grey_dark));
		}
		
		viewHolder.textView.setText(list.get(position));
		return convertView;
	}

	class ViewHolder{
		TextView textView;
	}
}