package com.marlabs.whpicker.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Validation;

public class OrderAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private List<Order> mOrdersList;	
	private Context context;

	public OrderAdapter(Context context, List<Order> ordersList) {
		this.mOrdersList = ordersList;
		this.context = context;
		mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mOrdersList.size();
	}

	@Override
	public Object getItem(int position) {
		return mOrdersList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder; 
		if(convertView == null){
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(R.layout.list_row_orders, parent, false);

			viewHolder.textViewOrder = (TextView) convertView.findViewById(R.id.order);
			viewHolder.textViewPSHUCount = (TextView) convertView.findViewById(R.id.order_pshucount);
			viewHolder.imageViewStatus = (ImageView) convertView.findViewById(R.id.statusImageView);
			convertView.setTag(viewHolder);
		}
		else{
			viewHolder = (ViewHolder)convertView.getTag();
		}

		if (position%2 == 0) {
			convertView.setBackgroundColor(context.getResources().getColor(R.color.grey_light));
		}
		else{
			convertView.setBackgroundColor(context.getResources().getColor(R.color.grey_dark));
		}

		Order order = mOrdersList.get(position);
		viewHolder.textViewOrder.setText(Util.removeLeadingZeros(order.getWHOrder()));
		
		if(!Validation.isNullString(order.getOrderUser()))
			viewHolder.textViewPSHUCount.setText("(" + order.getOrderUser() +") " + order.getPSHUCount());
		else
			if(order.getPSHUCount() == 0)
				viewHolder.textViewPSHUCount.setText("");
			else
				viewHolder.textViewPSHUCount.setText(order.getPSHUCount()+"");
		
		if (order.getStatus() !=null) {
			String status = order.getStatus();
			if(status.equalsIgnoreCase("")){
				viewHolder.imageViewStatus.setImageResource(R.drawable.open);
				if(order.getPSHUCount() == 0)
					viewHolder.textViewPSHUCount.setText("");
				else
					viewHolder.textViewPSHUCount.setText(order.getPSHUCount()+"");
			}
			else if(status.equalsIgnoreCase("D")){
				viewHolder.imageViewStatus.setImageResource(R.drawable.in_process);
			}
			else if(status.equalsIgnoreCase("C")){//Confirmed
				viewHolder.imageViewStatus.setImageResource(R.drawable.verified);
			}
			else if(status.equalsIgnoreCase("A")){//Not Yet Processed
				viewHolder.imageViewStatus.setImageResource(R.drawable.open);
			}
			else if(status.equalsIgnoreCase("B")){//Partially Processed
				viewHolder.imageViewStatus.setImageResource(R.drawable.in_process);
			}
			else{
				viewHolder.imageViewStatus.setImageResource(R.drawable.open);
			}
		}
		return convertView;
	}

	class ViewHolder{
		TextView textViewOrder;
		TextView textViewPSHUCount;
		ImageView imageViewStatus;
	}
}
