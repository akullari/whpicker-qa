package com.marlabs.whpicker.models;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.content.Context;

import com.android.volley.VolleyError;
import com.marlabs.whpicker.outbound.services.OutboundBussinessClass;
import com.marlabs.whpicker.tparsers.OutboundParser;
import com.marlabs.whpicker.utility.Validation;


public class Resource implements Comparable<Resource> {

	/*--------------------- ENUM Constants ----------------------*/
	public static final int RPT_NO_PARING=0;
	public static final int RPT_MANUAL_PARING=1;
	public static final int RPT_AUTO_PARING=2;

	private String resourceName;
	private String userName;
	private int resourceType;
	private String resourceText;
	private int resourceCapacity;
	private int lastOccupiedSlotNumber;

	List<ConsolidationItem> packingTasks;

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public int getResourceType() {
		return resourceType;
	}

	public void setResourceType(int resourceTypes) {
		this.resourceType = resourceTypes;
	}

	public String getResourceText() {
		return resourceText;
	}

	public void setResourceText(String resourceText) {
		this.resourceText = resourceText;
	}

	public int getResourceCapacity() {
		return resourceCapacity;
	}

	public void setResourceCapacity(int slotsCapacity) {
		this.resourceCapacity = slotsCapacity;
	}

	public int getLastOccupiedSlotNumber() {
		return lastOccupiedSlotNumber;
	}

	public void setLastOccupiedSlotNumber(int lastOccupiedSlotNumber) {
		this.lastOccupiedSlotNumber = lastOccupiedSlotNumber;
	}

	@Override
	public int compareTo(Resource another) {
		return resourceName.compareTo(another.resourceName);
	}

	private static Resource _selectedResource = null;
	public static Resource getSelectedResource()
	{
		return _selectedResource;
	}

	public static boolean selectResource(Context context, Resource selectedResource)
	{
		if(_selectedResource==null)
		{
			_selectedResource=selectedResource;
			_selectedResource.setResourceCapacity(selectedResource.getResourceCapacity());
			_selectedResource.setLastOccupiedSlotNumber(0);
			if(_selectedResource.packingTasks == null)
				_selectedResource.packingTasks = new ArrayList<ConsolidationItem>();
			_selectedResource.fetchPackingTasks(context);
			return true;
		}
		else
		{
			return false; //Release before select...
		}

	}

	public static void releaseSelectedResource() 
	{
		if(_selectedResource != null){
			_selectedResource.setLastOccupiedSlotNumber(0);
			_selectedResource.packingTasks.clear();
		}
		_selectedResource = null;
	}

	public List<ConsolidationItem> getAccomadatedTasks() {
		return packingTasks;
	}

	private void fetchPackingTasks(Context context){
		try 
		{
			OutboundBussinessClass.getInstance().callOutboundConsolidationTasksVolleyRequest(context,
					context.getClass().getMethod("onResourceTasksResponse", JSONObject.class, VolleyError.class));
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public void onResourceTasksResponse(JSONObject jsonObject, VolleyError volleyError) 
	{
		System.out.println("Tasks Fetched"+ jsonObject.toString());
		if(volleyError == null){
			int lastSlotNumber=_selectedResource.getLastOccupiedSlotNumber();
			List<Task> packingTasks = OutboundParser.parseOutboundTasksResult(jsonObject);
			if(packingTasks.size() == 0){
				return;
			}
			for(Task task: packingTasks){
				_selectedResource.addTask(task);
				int logicalPosition = 0;
				if(!Validation.isNullString(task.getLogicalPosition()))
					logicalPosition = Integer.parseInt(task.getLogicalPosition());

				if(lastSlotNumber>logicalPosition)
					lastSlotNumber = logicalPosition;
			}
			_selectedResource.setLastOccupiedSlotNumber(lastSlotNumber);
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
		}
	}

	public void addTask(Task task) {
		List<ConsolidationItem> splits=ConsolidationItem.ConsolidationItemsFromTask(task, this);
		for(ConsolidationItem item: splits)
		{
			packingTasks.add(item);
			//Derive and Update last occupied slot..
			if(this.getResourceType()==Resource.RPT_AUTO_PARING)
			{
				int currentSlot;
				if(Validation.isNullString(item.getSlot()))
					currentSlot=Integer.parseInt("0");
				else
					currentSlot=Integer.parseInt(item.getSlot());
				if(this.getLastOccupiedSlotNumber()<currentSlot)
				{
					this.setLastOccupiedSlotNumber(currentSlot);
				}
			}
		}
	}

	public void addTask(ConsolidationItem consolidationItem) {
		packingTasks.add(consolidationItem);
		//Derive and Update last occupied slot..
		if(this.getResourceType()==Resource.RPT_AUTO_PARING)
		{
			int currentSlot;
			if(Validation.isNullString(consolidationItem.getSlot()))
				currentSlot=Integer.parseInt("0");
			else
				currentSlot=Integer.parseInt(consolidationItem.getSlot());
			if(this.getLastOccupiedSlotNumber()<currentSlot)
			{
				this.setLastOccupiedSlotNumber(currentSlot);
			}
		}
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
