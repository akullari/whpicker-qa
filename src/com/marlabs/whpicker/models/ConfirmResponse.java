package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;
import java.util.ArrayList;
import java.util.List;

public class ConfirmResponse {
	private int responseCode;
	private String responseLine;
	private boolean isBatchError;
	private String batchErrorMessage;
	private List<ConfirmResponse> batchResponses = new ArrayList<ConfirmResponse>();
	private ErrorResponse errorResponse;
	private int taskIndex;
	
	private String strTag;
	
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseLine() {
		return responseLine;
	}
	public void setResponseLine(String responseLine) {
		this.responseLine = responseLine;
	}
	public List<ConfirmResponse> getBatchResponses() {
		return batchResponses;
	}
	public void setBatchResponses(List<ConfirmResponse> batchResponses) {
		this.batchResponses = batchResponses;
	}
	public boolean isBatchError() {
		return isBatchError;
	}
	public void setBatchError(boolean isBatchError) {
		this.isBatchError = isBatchError;
	}
	public String getBatchErrorMessage() {
		return batchErrorMessage;
	}
	public void setBatchErrorMessage(String batchErrorMessage) {
		this.batchErrorMessage = batchErrorMessage;
	}
	public String getStrTag() {
		return strTag;
	}
	public void setStrTag(String strTag) {
		this.strTag = strTag;
	}
	public ErrorResponse getErrorResponse() {
		return errorResponse;
	}
	public void setErrorResponse(ErrorResponse errorResponse) {
		this.errorResponse = errorResponse;
	}
	public int getTaskIndex() {
		return taskIndex;
	}
	public void setTaskIndex(int taskIndex) {
		this.taskIndex = taskIndex;
	}
}
