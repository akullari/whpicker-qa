package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;


public class ErrorDetails{
	private String errorDetailsCode;
	private String errorDetailsMessage;
	public String getErrorDetailsCode() {
		return errorDetailsCode;
	}
	public void setErrorDetailsCode(String errorDetailsCode) {
		this.errorDetailsCode = errorDetailsCode;
	}
	public String getErrorDetailsMessage() {
		return errorDetailsMessage;
	}
	public void setErrorDetailsMessage(String errorDetailsMessage) {
		this.errorDetailsMessage = errorDetailsMessage;
	}
}
