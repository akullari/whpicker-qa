package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;
import java.util.ArrayList;
import java.util.List;

public class ErrorResponse {
	
	private String errorCode;
	private String errorMessage;
	private List<ErrorDetails> errorDetailsList = new ArrayList<ErrorDetails>();
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public List<ErrorDetails> getErrorDetailsList() {
		return errorDetailsList;
	}

	public void setErrorDetailsList(List<ErrorDetails> errorDetailsList) {
		this.errorDetailsList = errorDetailsList;
	}
}
