package com.marlabs.whpicker.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.utility.Validation;

public class Task  implements Serializable {

	private static final long serialVersionUID = 4768120440647388445L;
	private String whOrder;
	private String status;
	private String queue;
	private String sourceBin;
	private String sourceHU;
	private String destinationBin;
	private String modifiedDestinationBin;
	private String product;
	private String product_Scanned;
	private String serial;
	private String batch;
	private String zone;
	private String handlingUnit;
	private int totalQuantity;
	private String quantityUoM;
	private String handlingUnitWarehouseTask;
	private String wareHouseTask;
	private String pickHU_Suggested;
	private List<SplitTask> mSplitTasksList = new ArrayList<SplitTask>();
	private String WarehouseOrderStatus;
	private boolean isValidTask;
	private String errorMessage;
	private String consolidationGroup;
	private String logicalPosition;
	
	public Task() {
		SplitTask splitTask = new SplitTask();
		getSplitTasksList().add(splitTask);
	}
	
	public String getDestinationBinValue(){
		if(Validation.isNullString(modifiedDestinationBin))
			return destinationBin;
		else
			return modifiedDestinationBin;
	}
	
	public String getModifiedDestinationBin() {
		return modifiedDestinationBin;
	}

	public void setModifiedDestinationBin(String modifiedDestinationBin) {
		this.modifiedDestinationBin = modifiedDestinationBin;
	}
	
	public int getRemainingQuantity(){
		int totalTempQuantity = (totalQuantity);
		System.out.println(totalTempQuantity);
		int suppliedQuantity = getSuppliedQuantity();
		return totalTempQuantity-suppliedQuantity;
	}

	public int getSuppliedQuantity(){
		int suppliedQuantity = 0;
		for(SplitTask splitTask : getSplitTasksList()){
			suppliedQuantity += (splitTask.getQuantity());
		}
		return suppliedQuantity;
	}

	
	public String getDestinationBin() {
		return destinationBin;
	}
	public void setDestinationBin(String destinationBin) {
		this.destinationBin = destinationBin;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getWHOrder() {
		return whOrder;
	}
	public void setWHOrder(String order) {
		this.whOrder = order;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSourceBin() {
		return sourceBin;
	}
	public void setSourceBin(String sourceBin) {
		this.sourceBin = sourceBin;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getQuantityUoM() {
		return quantityUoM;
	}
	public void setQuantityUoM(String quantityUoM) {
		this.quantityUoM = quantityUoM;
	}
	public String getHandlingUnitWarehouseTask() {
		return handlingUnitWarehouseTask;
	}
	public void setHandlingUnitWarehouseTask(String handlingUnitWarehouseTask) {
		this.handlingUnitWarehouseTask = handlingUnitWarehouseTask;
	}
	public String getHandlingUnit() {
		return handlingUnit;
	}
	public void setHandlingUnit(String handlingUnit) {
		this.handlingUnit = handlingUnit;
	}
	public String getWareHouseTask() {
		return wareHouseTask;
	}
	public void setWareHouseTask(String wareHouseTask) {
		this.wareHouseTask = wareHouseTask;
	}
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	public String getWarehouseOrderStatus() {
		return WarehouseOrderStatus;
	}
	public void setWarehouseOrderStatus(String warehouseOrderStatus) {
		WarehouseOrderStatus = warehouseOrderStatus;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public boolean isValidTask() {
		return isValidTask;
	}
	public void setValidTask(boolean isValidTask) {
		this.isValidTask = isValidTask;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getConsolidationGroup() {
		return consolidationGroup;
	}
	public void setConsolidationGroup(String consolidationGroup) {
		this.consolidationGroup = consolidationGroup;
	}
	public String getPickHU_Suggested() {
		return pickHU_Suggested;
	}
	public void setPickHU_Suggested(String pickHU_Suggested) {
		this.pickHU_Suggested = pickHU_Suggested;
	}
	public String getProduct_Scanned() {
		return product_Scanned;
	}
	public void setProduct_Scanned(String product_Scanned) {
		this.product_Scanned = product_Scanned;
	}
	public List<SplitTask> getSplitTasksList() {
		return mSplitTasksList;
	}
	public void setSplitTasksList(List<SplitTask> mSplitTasksList) {
		this.mSplitTasksList = mSplitTasksList;
	}
	public int getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getLogicalPosition() {
		return logicalPosition;
	}

	public void setLogicalPosition(String logicalPosition) {
		this.logicalPosition = logicalPosition;
	}

	public String getSourceHU() {
		return sourceHU;
	}

	public void setSourceHU(String sourceHU) {
		this.sourceHU = sourceHU;
	}
}