package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;


public class TaskObject{
	private String WHTask;
	private String Warehouse;
	private String Queue;
	
	private String Huident;
	private String Pmat;
	private String Rsrc;
	private String Logpos;
	
	private String SrcHU;
	private String Quantity;
	private String Unit;
	private String Split;
	public String getWHTask() {
		return WHTask;
	}
	public void setWHTask(String wHTask) {
		WHTask = wHTask;
	}
	public String getWarehouse() {
		return Warehouse;
	}
	public void setWarehouse(String warehouse) {
		Warehouse = warehouse;
	}
	public String getQueue() {
		return Queue;
	}
	public void setQueue(String queue) {
		Queue = queue;
	}
	public String getHuident() {
		return Huident;
	}
	public void setHuident(String huident) {
		Huident = huident;
	}
	public String getPmat() {
		return Pmat;
	}
	public void setPmat(String pmat) {
		Pmat = pmat;
	}
	public String getRsrc() {
		return Rsrc;
	}
	public void setRsrc(String rsrc) {
		Rsrc = rsrc;
	}
	public String getLogpos() {
		return Logpos;
	}
	public void setLogpos(int logpos) {
		Logpos = logpos+"";
	}
	public String getSrcHU() {
		return SrcHU;
	}
	public void setSrcHU(String srcHU) {
		SrcHU = srcHU;
	}
	public String getQuantity() {
		return Quantity;
	}
	public void setQuantity(String quantity) {
		Quantity = quantity;
	}
	public String getUnit() {
		return Unit;
	}
	public void setUnit(String unit) {
		Unit = unit;
	}
	public String getSplit() {
		return Split;
	}
	public void setSplit(String split) {
		Split = split;
	}
}