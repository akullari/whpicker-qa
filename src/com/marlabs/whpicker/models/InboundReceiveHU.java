package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;
import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class InboundReceiveHU implements Serializable {

	private String partnerNumber;
	private String partnerName;
	private String door;
	private String HUCount;
	private String handlingUnit;
	private String documentID;
	private String product;
	private String batch;
	private String qunatity;
	private String UOM;
	private String destinationStorageBin;
	private String sourceStorageBin;
	private String destinationHU;
	private String sourceHU;
	private String warehouseTask;
	private String 	warehouseProcessType;
	private List<InboundReceiveHU> receiveHUsList;
	
	public String getPartnerNumber() {
		return partnerNumber;
	}
	public void setPartnerNumber(String partnerNumber) {
		this.partnerNumber = partnerNumber;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getDoor() {
		return door;
	}
	public void setDoor(String door) {
		this.door = door;
	}
	public String getHUCount() {
		return HUCount;
	}
	public void setHUCount(String hUCount) {
		HUCount = hUCount;
	}
	public String getHandlingUnit() {
		return handlingUnit;
	}
	public void setHandlingUnit(String handlingUnit) {
		this.handlingUnit = handlingUnit;
	}
	public String getDocumentID() {
		return documentID;
	}
	public void setDocumentID(String documentID) {
		this.documentID = documentID;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getQunatity() {
		return qunatity;
	}
	public void setQunatity(String qunatity) {
		this.qunatity = qunatity;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public List<InboundReceiveHU> getReceiveHUsList() {
		return receiveHUsList;
	}
	public void setReceiveHUsList(List<InboundReceiveHU> receiveHUsList) {
		this.receiveHUsList = receiveHUsList;
	}
	public String getDestinationStorageBin() {
		return destinationStorageBin;
	}
	public void setDestinationStorageBin(String destinationStorageBin) {
		this.destinationStorageBin = destinationStorageBin;
	}
	public String getDestinationHU() {
		return destinationHU;
	}
	public void setDestinationHU(String destinationHU) {
		this.destinationHU = destinationHU;
	}
	public String getSourceHU() {
		return sourceHU;
	}
	public void setSourceHU(String sourceHU) {
		this.sourceHU = sourceHU;
	}
	public String getSourceStorageBin() {
		return sourceStorageBin;
	}
	public void setSourceStorageBin(String sourceStorageBin) {
		this.sourceStorageBin = sourceStorageBin;
	}
	public String getWarehouseTask() {
		return warehouseTask;
	}
	public void setWarehouseTask(String warehouseTask) {
		this.warehouseTask = warehouseTask;
	}
	public String getWarehouseProcessType() {
		return warehouseProcessType;
	}
	public void setWarehouseProcessType(String warehouseProcessType) {
		this.warehouseProcessType = warehouseProcessType;
	}
}
