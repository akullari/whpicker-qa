package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;
import java.util.ArrayList;
import java.util.List;

public class ConsolidationItem extends SplitTask
{
	public static List<ConsolidationItem> ConsolidationItemsFromTask(Task fromTask, Resource associatedResource)
	{
		List<ConsolidationItem> returnObject=new ArrayList<ConsolidationItem>();
		for(SplitTask sTask: fromTask.getSplitTasksList())
		{
			ConsolidationItem item=new ConsolidationItem();
			item.setConsolidationGroup(fromTask.getConsolidationGroup());
			item.setDestBin(fromTask.getDestinationBin());
			item.setOrderID(fromTask.getWHOrder());
			item.setResource(associatedResource.getResourceName());
			item.setConfirmSuccessful(fromTask.isValidTask());
			item.setTaskID(fromTask.getWareHouseTask());

			item.setTote(sTask.getTote());
			item.setQuantity(sTask.getQuantity());
			item.setSlot(sTask.getSlot());
			
			returnObject.add(item);
		}
		return returnObject;
	}
	
	

	private String client;
	private String consolidationGroup;
	private String destBin;
	private int itemNumber;
	private String orderID;
	private String pathSequence;
	private String resource;
	private boolean isConfirmSuccessful;
	private String taskID;
	private String userName;
	private String whNo;
	
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getConsolidationGroup() {
		return consolidationGroup;
	}
	public void setConsolidationGroup(String consolidationGroup) {
		this.consolidationGroup = consolidationGroup;
	}
	public String getDestBin() {
		return destBin;
	}
	public void setDestBin(String destBin) {
		this.destBin = destBin;
	}
	public int getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(int itemNumber) {
		this.itemNumber = itemNumber;
	}
	public String getOrderID() {
		return orderID;
	}
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public String getPathSequence() {
		return pathSequence;
	}
	public void setPathSequence(String pathSequence) {
		this.pathSequence = pathSequence;
	}
	public String getTaskID() {
		return taskID;
	}
	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getWhNo() {
		return whNo;
	}
	public void setWhNo(String whNo) {
		this.whNo = whNo;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public boolean isConfirmSuccessful() {
		return isConfirmSuccessful;
	}

	public void setConfirmSuccessful(boolean isConfirmSuccessful) {
		this.isConfirmSuccessful = isConfirmSuccessful;
	}
}
