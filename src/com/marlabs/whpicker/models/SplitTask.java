package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;


public class SplitTask {
	private String tote;
	private int quantity;
	private String slot;
	public String getTote() {
		return tote;
	}
	public void setTote(String tote) {
		this.tote = tote;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getSlot() {
		return slot;
	}
	public void setSlot(String slot) {
		this.slot = slot;
	}
}
