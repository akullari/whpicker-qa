package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;
import java.io.Serializable;


public class HomeMenuItem implements Serializable {

	private static final long serialVersionUID = -8237543289050036162L;
	private String itemId;
	private String itemName;
	private int itemImageId;

	public HomeMenuItem(){
	}

	public HomeMenuItem(String id, String name, int itemImageId){
		setItemId(id);
		setItemName(name);
		setItemImageId(itemImageId);
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public int getItemImageId() {
		return itemImageId;
	}

	public void setItemImageId(int itemImageId) {
		this.itemImageId = itemImageId;
	}
}
