package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;



public class Queue {

	private String queueName;
	private String queueText;
	private String count;
	public String getQueueText() {
		return queueText;
	}
	public void setQueueText(String queueText) {
		this.queueText = queueText;
	}
	public String getQueueName() {
		return queueName;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
}
