package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;
import java.io.Serializable;

public class PutAway implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sourceBin;
	private String sourceHU;
	private String product;
	private String batch;
	private String quantity;
	private String quantityUoM;
	private String destinationBin;
	private String destinationHU;
	private String sNumber;
	private String actualHU;
	private String handlingUnit;
	private String serial;
	private String wareHouseTask;
	
	public String getSourceBin() {
		return sourceBin;
	}
	public void setSourceBin(String sourceBin) {
		this.sourceBin = sourceBin;
	}
	public String getSourceHU() {
		return sourceHU;
	}
	public void setSourceHU(String sourceHU) {
		this.sourceHU = sourceHU;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getDestinationBin() {
		return destinationBin;
	}
	public void setDestinationBin(String destinationBin) {
		this.destinationBin = destinationBin;
	}
	public String getDestinationHU() {
		return destinationHU;
	}
	public void setDestinationHU(String desyinationHU) {
		this.destinationHU = desyinationHU;
	}
	public String getsNumber() {
		return sNumber;
	}
	public void setsNumber(String sNumber) {
		this.sNumber = sNumber;
	}
	public String getActualHU() {
		return actualHU;
	}
	public void setActualHU(String actualHU) {
		this.actualHU = actualHU;
	}
	public String getHandlingUnit() {
		return handlingUnit;
	}
	public void setHandlingUnit(String handlingUnit) {
		this.handlingUnit = handlingUnit;
	}
	public String getQuantityUoM() {
		return quantityUoM;
	}
	public void setQuantityUoM(String quantityUoM) {
		this.quantityUoM = quantityUoM;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getWareHouseTask() {
		return wareHouseTask;
	}
	public void setWareHouseTask(String wareHouseTask) {
		this.wareHouseTask = wareHouseTask;
	}
}
