package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;
import java.io.Serializable;
import java.util.List;

public class Order  implements Serializable {

	private static final long serialVersionUID = 4768120440647388445L;
	private String whOrder;
	private String status;
	private String queue;
	private String sourceBin;
	private String destinationBin;
	private String product;
	private String serial;
	private String batch;
	private String zone;
	private String handlingUnit;
	private String quantity;
	private String quantityUoM;
	private String handlingUnitWarehouseTask;
	private String wareHouseTask;
	private String resource;
	private String pickHU;
	private String WarehouseOrderStatus;
	private int PSHUCount;
	private String weight;
	private String orderUser;
	private List<Task> tasksList; 
	public String getDestinationBin() {
		return destinationBin;
	}
	public void setDestinationBin(String destinationBin) {
		this.destinationBin = destinationBin;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getWHOrder() {
		return whOrder;
	}
	public void setWHOrder(String order) {
		this.whOrder = order;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSourceBin() {
		return sourceBin;
	}
	public void setSourceBin(String sourceBin) {
		this.sourceBin = sourceBin;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getQuantityUoM() {
		return quantityUoM;
	}
	public void setQuantityUoM(String quantityUoM) {
		this.quantityUoM = quantityUoM;
	}
	public String getHandlingUnitWarehouseTask() {
		return handlingUnitWarehouseTask;
	}
	public void setHandlingUnitWarehouseTask(String handlingUnitWarehouseTask) {
		this.handlingUnitWarehouseTask = handlingUnitWarehouseTask;
	}
	public String getHandlingUnit() {
		return handlingUnit;
	}
	public void setHandlingUnit(String handlingUnit) {
		this.handlingUnit = handlingUnit;
	}
	public String getWareHouseTask() {
		return wareHouseTask;
	}
	public void setWareHouseTask(String wareHouseTask) {
		this.wareHouseTask = wareHouseTask;
	}
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
	public String getWarehouseOrderStatus() {
		return WarehouseOrderStatus;
	}
	public void setWarehouseOrderStatus(String warehouseOrderStatus) {
		WarehouseOrderStatus = warehouseOrderStatus;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getPickHU() {
		return pickHU;
	}
	public void setPickHU(String pickHU) {
		this.pickHU = pickHU;
	}
	public List<Task> getTasksList() {
		return tasksList;
	}
	public void setTasksList(List<Task> tasksList) {
		this.tasksList = tasksList;
	}
	public int getPSHUCount() {
		return PSHUCount;
	}
	public void setPSHUCount(int pSHUCount) {
		this.PSHUCount = pSHUCount;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		if(weight.equalsIgnoreCase("null"))
			this.weight = "";
		else
			this.weight = weight;
	}
	public String getOrderUser() {
		return orderUser;
	}
	public void setOrderUser(String orderUser) {
		this.orderUser = orderUser;
	}
}
