package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;


public class Delivery {
	private String deliveryCode;
	private String HU;
	private String product;
	private String ITNumber;
	private String SQuantity;
	private String packageMaterial;
	
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getHU() {
		return HU;
	}
	public void setHU(String hU) {
		HU = hU;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getITNumber() {
		return ITNumber;
	}
	public void setITNumber(String iTNumber) {
		ITNumber = iTNumber;
	}
	public String getSQuantity() {
		return SQuantity;
	}
	public void setSQuantity(String sQuantity) {
		SQuantity = sQuantity;
	}
	public String getPackageMaterial() {
		return packageMaterial;
	}
	public void setPackageMaterial(String packageMaterial) {
		this.packageMaterial = packageMaterial;
	}
}
