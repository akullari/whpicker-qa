package com.marlabs.whpicker.models;

import com.marlabs.whpickerqa.R;
import java.io.Serializable;

public class Unloading implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String HU;
	private String TU;
	private String ASN;
	private String vendor;
	private String totalHUCount;
	private String processedHUCount;
	private String DestinationBin;
	private String product;
	private String qunatity;
	private String queue;
	private String wareHouseTask;
	private String batch;
	private String deliveryNumber;
	
	public String getTU() {
		return TU;
	}
	public void setTU(String tU) {
		TU = tU;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getHU() {
		return HU;
	}
	public void setHU(String hU) {
		HU = hU;
	}
	public String getDestinationBin() {
		return DestinationBin;
	}
	public void setDestinationBin(String destinationBin) {
		DestinationBin = destinationBin;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getQunatity() {
		return qunatity;
	}
	public void setQunatity(String qunatity) {
		this.qunatity = qunatity;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getTotalHUCount() {
		return totalHUCount;
	}
	public void setTotalHUCount(String totalHUCount) {
		this.totalHUCount = totalHUCount;
	}
	public String getProcessedHUCount() {
		return processedHUCount;
	}
	public void setProcessedHUCount(String processedHUCount) {
		this.processedHUCount = processedHUCount;
	}
	public String getDeliveryNumber() {
		return deliveryNumber;
	}
	public void setDeliveryNumber(String deliveryNumber) {
		this.deliveryNumber = deliveryNumber;
	}
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	public String getWareHouseTask() {
		return wareHouseTask;
	}
	public void setWareHouseTask(String wareHouseTask) {
		this.wareHouseTask = wareHouseTask;
	}
	public String getASN() {
		return ASN;
	}
	public void setASN(String aSN) {
		ASN = aSN;
	}
}
