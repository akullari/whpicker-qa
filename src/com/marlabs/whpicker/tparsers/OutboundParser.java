package com.marlabs.whpicker.tparsers;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.marlabs.whpicker.models.ConsolidationItem;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Queue;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.utility.Validation;

public class OutboundParser {

	/*--------------------- 'OUTBOUND -> PUTAWAY' PARSERS ----------------------*/
	public static List<Queue> parseOutboundQueuesResult(JSONObject jsonObject) {
		List<Queue> queuesList = new ArrayList<Queue>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Queue queue = new Queue();
				queue.setQueueName(jObject.getString("QUEUE"));
				queue.setQueueText(jObject.getString("TEXT"));
				queue.setCount(jObject.getInt("WHO_COUNT")+"");
				queuesList.add(queue);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return queuesList;
	}

	public static List<Order> parseOutboundOrdersResult(JSONObject jsonObject) {
		List<Order> ordersList = new ArrayList<Order>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Order order = new Order();
				order.setWHOrder(jObject.getString("WHO"));
				order.setStatus(jObject.getString("STATUS"));
				order.setQueue(jObject.getString("QUEUE"));
				order.setOrderUser(jObject.getString("CHANGED_BY"));
				ordersList.add(order);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ordersList;
	}

	public static List<Order> parseOutboundOrdersDetailResult(JSONObject jsonObject) {
		List<Order> ordersList = new ArrayList<Order>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Order order = new Order();
				order.setWHOrder(jObject.getString("WHO"));
				try{
					int count = Integer.parseInt(jObject.getString("PSHU_CNT"));
					order.setPSHUCount(count);
				}
				catch(Exception e){
					order.setPSHUCount(0);
				}
				order.setWeight(jObject.getString("N_WEIGHT"));
				ordersList.add(order);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ordersList;
	}

	public static List<Task> parseOutboundTasksResult(JSONObject jsonObject) {
		List<Task> tasksList = new ArrayList<Task>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Task task = new Task();
				task.setWHOrder(jObject.getString("WHO"));
				task.setSourceBin(jObject.getString("VLPLA"));
				task.setDestinationBin(jObject.getString("NLPLA"));
				task.setProduct(jObject.getString("PRODUCTNO"));
				if(!Validation.isNullString(jObject.getString("VSOLM")))
					task.setTotalQuantity(Integer.parseInt(jObject.getString("VSOLM")));
				else
					task.setTotalQuantity(0);
					
				task.setQuantityUoM(jObject.getString("MEINS"));
				task.setBatch(jObject.getString("CHARG"));
				task.setHandlingUnitWarehouseTask(jObject.getString("FLGHUTO"));
				task.setHandlingUnit(jObject.getString("VLENR"));
				task.setWareHouseTask(jObject.getString("TANUM"));
				task.setPickHU_Suggested(jObject.getString("NLENR"));
				task.setQueue(jObject.getString("QUEUE"));
				task.setConsolidationGroup(jObject.getString("DSTGRP"));
				task.setLogicalPosition(jObject.getString("LOGPOS"));
				task.setValidTask(false);
				tasksList.add(task);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return tasksList;
	}


	public static List<ConsolidationItem> parsePackingItemsResult(JSONObject jsonObject) {
		List<ConsolidationItem> tasksList = new ArrayList<ConsolidationItem>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				ConsolidationItem consolidationItem = new ConsolidationItem();
				consolidationItem.setOrderID(jObject.getString("WHO"));
				consolidationItem.setDestBin(jObject.getString("NLPLA"));
				if(!Validation.isNullString(jObject.getString("VSOLM")))
					consolidationItem.setQuantity(Integer.parseInt(jObject.getString("VSOLM")));
				else
					consolidationItem.setQuantity(0);
				consolidationItem.setTote(jObject.getString("VLENR"));
				consolidationItem.setTaskID(jObject.getString("TANUM"));
				consolidationItem.setConsolidationGroup(jObject.getString("DSTGRP"));
				consolidationItem.setSlot(jObject.getString("LOGPOS"));
				tasksList.add(consolidationItem);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return tasksList;
	}

	public static List<Order> parseOutboundWHOResult(JSONObject jsonObject) {
		List<Order> ordersList = new ArrayList<Order>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Order order = new Order();
				order.setWHOrder(jObject.getString("WHO"));
				order.setStatus(jObject.getString("STATUS"));
				order.setQueue(jObject.getString("QUEUE"));
				ordersList.add(order);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ordersList;
	}

	public static String parseInProcessResponse(String result) {
		String resourceName = "";
		try {
			//System.out.println("*******resource response********" + result);
			JSONObject jsonObject = new JSONObject(result);
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			resourceName = jObjectMain.getString("Rsrc");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return resourceName;
	}
}
