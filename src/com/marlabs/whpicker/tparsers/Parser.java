package com.marlabs.whpicker.tparsers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.ErrorDetails;
import com.marlabs.whpicker.models.ErrorResponse;
import com.marlabs.whpicker.models.InboundReceiveHU;
import com.marlabs.whpicker.models.Order;
import com.marlabs.whpicker.models.Queue;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.models.Unloading;
import com.marlabs.whpicker.models.User;
import com.marlabs.whpicker.utility.Validation;

public class Parser {

	public static User ParseBasicUserData(String data) {
		User user = new User();
		try {
			JSONObject jsonObject = new JSONObject(data);
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			user.setUserID(jObjectMain.getString("Username"));
			user.setFullName(jObjectMain.getString("FullName"));
			user.setLanguage(jObjectMain.getString("LANGU"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return user;
	}

	public static List<User> ParseLoggedInUserData(JSONObject jsonObject) {
		List<User> users = new ArrayList<User>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				User user = new User();
				user.setUserID(jObject.getString("BNAME"));
				user.setClient(jObject.getString("MANDT"));
				user.setWarehouseNumber(jObject.getString("PARVA"));
				user.setLanguage(jObject.getString("SPRACHE"));
				users.add(user);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return users;
	}

	public static List<Resource> parseResourcesResponse(JSONObject jsonObject) {
		List<Resource> resourcesList = new ArrayList<Resource>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Resource resource = new Resource();
				resource.setResourceName(jObject.getString("RSRC"));
				resource.setUserName(jObject.getString("UNAME"));
				String strPostinMangt=jObject.getString("POSTN_MNGMNT");
				if(Validation.isNullString(strPostinMangt))
					resource.setResourceType(0);
				else
					resource.setResourceType(Integer.parseInt(strPostinMangt));
				resource.setResourceText(jObject.getString("TEXT"));
				resourcesList.add(resource);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Collections.sort(resourcesList);
		return resourcesList;
	}

	/*--------------------- 'INBOUND -> PUTAWAY' PARSERS ----------------------*/
	public static List<Queue> parseInboundQueuesResult(JSONObject jsonObject) {
		List<Queue> inboundQueuesList = new ArrayList<Queue>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Queue inboundQueue = new Queue();
				inboundQueue.setQueueName(jObject.getString("Queue"));
				inboundQueue.setQueueText(jObject.getString("QueueName"));
				inboundQueuesList.add(inboundQueue);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return inboundQueuesList;
	}

	public static List<Order> parseInboundOrdersResult(JSONObject jsonObject) {
		List<Order> inboundOrdersList = new ArrayList<Order>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Order inboundOrder = new Order();
				inboundOrder.setWHOrder(jObject.getString("WHO"));
				inboundOrder.setStatus(jObject.getString("WarehouseOrderStatus"));
				inboundOrder.setQueue(jObject.getString("Queue"));
				inboundOrder.setResource(jObject.getString("Resource"));
				inboundOrdersList.add(inboundOrder);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return inboundOrdersList;
	}

	public static List<Task> parseInboundTasksResult(JSONObject jsonObject) {
		List<Task> inboundTasksList = new ArrayList<Task>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Task inboundTask = new Task();
				inboundTask.setWHOrder(jObject.getString("WHO"));
				inboundTask.setSourceBin(jObject.getString("SourceStorageBin"));
				inboundTask.setDestinationBin(jObject.getString("DestinationStorageBin"));
				inboundTask.setProduct(jObject.getString("ProductNo"));
				inboundTask.setTotalQuantity(jObject.getInt("Quantity"));
				inboundTask.setQuantityUoM(jObject.getString("UOM"));
				inboundTask.setBatch(jObject.getString("Batch"));
				inboundTask.setSerial("");
				inboundTask.setHandlingUnitWarehouseTask(jObject.getString("HUWT"));
				inboundTask.setWareHouseTask(jObject.getString("WarehouseTask"));
				inboundTask.setQueue(jObject.getString("Queue"));
				inboundTasksList.add(inboundTask);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return inboundTasksList;
	}


	/*--------------------- 'INBOUND -> UNLOADING' PARSERS ----------------------*/
	public static List<Unloading> parseUnloadingByDeliveryResult(JSONObject jsonObject) {
		List<Unloading> unloadingList = new ArrayList<Unloading>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObjectDelivery = jsonArray.getJSONObject(i);
				Unloading unloading = new Unloading();
				unloading.setDeliveryNumber(jObjectDelivery.getString("DeliveryNumber"));
				unloading.setVendor(jObjectDelivery.getString("Vendor"));
				unloading.setHU(jObjectDelivery.getString("HUIDENT"));
				unloading.setTotalHUCount(jsonArray.length()+"");
				unloading.setProcessedHUCount(jObjectDelivery.getString("ProcessedHUCount"));
				unloading.setQueue(jObjectDelivery.getString("Queue"));
				unloading.setWareHouseTask(jObjectDelivery.getString("WarehouseTask"));
				unloadingList.add(unloading);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return unloadingList;
	}

	public static List<Unloading> parseUnloadingByTUResult(JSONObject jsonObject){
		List<Unloading> unloadingList = new ArrayList<Unloading>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObjectTU = jsonArray.getJSONObject(i);
				Unloading unloading = new Unloading();
				unloading.setTU(jObjectTU.getString("TransportationUnit"));
				unloading.setVendor(jObjectTU.getString("Vendor"));
				unloading.setHU(jObjectTU.getString("HUIDENT"));
				unloading.setTotalHUCount(jsonArray.length()+"");
				unloading.setProcessedHUCount(jObjectTU.getString("CountOfProcessedHU"));
				//				unloading.setQueue(jObjectTU.getString("Queue"));
				//				unloading.setWareHouseTask(jObjectTU.getString("WarehouseTask"));
				unloadingList.add(unloading);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return unloadingList;
	}

	public static List<Unloading> parseUnloadingByHUResult(JSONObject jsonObject) {
		List<Unloading> unloadingList = new ArrayList<Unloading>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Unloading unloading = new Unloading();
				unloading.setVendor(jObject.getString("Vendor"));
				unloading.setHU(jObject.getString("HUIDENT"));
				unloading.setTotalHUCount(jObject.getString("HUCount"));
				unloading.setProcessedHUCount(jObject.getString("ProcessedHUCount"));
				//				unloading.setQueue(jObject.getString("Queue"));
				//				unloading.setWareHouseTask(jObject.getString("WarehouseTask"));
				unloadingList.add(unloading);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return unloadingList;
	}

	public static String parseError(String string){
		System.out.println("Error ----" + string);
		String errorMessage = "";
		try {
			JSONObject jObjectMain = new JSONObject(string);
			JSONObject jObjectError = jObjectMain.getJSONObject("error");
			JSONObject jObjectMessage = jObjectError.getJSONObject("message");
			errorMessage =  jObjectMessage.getString("value");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return errorMessage;
	}
	
	public static ErrorResponse parseErrorDetails(String string){
		//System.out.println("Error ----" + string);
		ErrorResponse errorResponse = new ErrorResponse();
		try {
			JSONObject jObjectMain = new JSONObject(string);
			JSONObject jObjectError = jObjectMain.getJSONObject("error");
			errorResponse.setErrorCode(jObjectError.getString("code"));
			
			JSONObject jObjectMessage = jObjectError.getJSONObject("message");
			errorResponse.setErrorMessage(jObjectMessage.getString("value"));

			JSONObject jObjectInnerError = jObjectError.getJSONObject("innererror");
			JSONArray jsonArraydetails = jObjectInnerError.getJSONArray("errordetails");

			List<ErrorDetails> errorDetailsList = new ArrayList<ErrorDetails>();
			for(int i=0; i<jsonArraydetails.length(); i++){
				JSONObject jObjectErrorDetails = jsonArraydetails.getJSONObject(i);
				ErrorDetails errorDetails = new ErrorDetails();
				errorDetails.setErrorDetailsCode(jObjectErrorDetails.getString("code"));
				errorDetails.setErrorDetailsMessage(jObjectErrorDetails.getString("message"));
				errorDetailsList.add(errorDetails);
			}
			errorResponse.setErrorDetailsList(errorDetailsList);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		System.out.println(errorResponse.getErrorCode());
		return errorResponse;
	}

	public static List<InboundReceiveHU> parseReceiveByHUTasks(JSONObject jsonObject) {
		List<InboundReceiveHU> inboundReceiveHUsList = new ArrayList<InboundReceiveHU>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);

				InboundReceiveHU inboundReceiveHU = new InboundReceiveHU();
				inboundReceiveHU.setPartnerNumber(jObject.getString("Partner"));
				inboundReceiveHU.setPartnerName(jObject.getString("PartnerName"));
				inboundReceiveHU.setDoor(jObject.getString("Door"));
				inboundReceiveHU.setHUCount(jObject.getString("HUCount"));
				inboundReceiveHU.setHandlingUnit(jObject.getString("HUIDENT"));
				inboundReceiveHU.setDocumentID(jObject.getString("DocumentID"));
				inboundReceiveHU.setProduct(jObject.getString("ProductNo"));
				inboundReceiveHU.setBatch(jObject.getString("BatchNo"));
				inboundReceiveHU.setQunatity(jObject.getString("Quantity"));
				inboundReceiveHU.setUOM(jObject.getString("UOM"));

				inboundReceiveHU.setDestinationStorageBin(jObject.getString("DestinationStorageBin"));
				inboundReceiveHU.setSourceStorageBin(jObject.getString("SourceStorageBin"));
				inboundReceiveHU.setDestinationHU(jObject.getString("DestinationHU"));
				inboundReceiveHU.setSourceHU(jObject.getString("SourceHU"));
				inboundReceiveHU.setWarehouseTask(jObject.getString("WarehouseTask"));
				inboundReceiveHU.setWarehouseProcessType(jObject.getString("WarehouseProcessType"));


				inboundReceiveHUsList.add(inboundReceiveHU);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return inboundReceiveHUsList;
	}

	public static String parseReceiveByHUDocimentID(JSONObject jsonObject) {
		String documentID = null;
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				documentID = jObject.getString("DocumentID");
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return documentID;
		}
		return documentID;
	}

	public static List<Queue> parsePSAResult(JSONObject jsonObject) {
		List<Queue> psaList = new ArrayList<Queue>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Queue inboundOrder = new Queue();
				inboundOrder.setQueueName(jObject.getString("PSA"));
				inboundOrder.setQueueText(jObject.getString("PSAName"));
				psaList.add(inboundOrder);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return psaList;
	}

	public static List<Task> parseStagingTasksResult(JSONObject jsonObject) {
		List<Task> psaTasksList = new ArrayList<Task>();
		try {
			JSONObject jObjectMain = new JSONObject(jsonObject.getString("d"));
			JSONArray jsonArray = jObjectMain.getJSONArray("results");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jObject = jsonArray.getJSONObject(i);
				Task inboundTask = new Task();
				inboundTask.setHandlingUnitWarehouseTask(jObject.getString("HUWT"));
				inboundTask.setSourceBin(jObject.getString("SourceStorageBin"));
				inboundTask.setProduct(jObject.getString("ProductNo"));
				inboundTask.setTotalQuantity(jObject.getInt("HUWT"));
				inboundTask.setQuantityUoM(jObject.getString("HUWT"));
				inboundTask.setWHOrder(jObject.getString("WarehouseTask"));
				inboundTask.setSerial("");
				inboundTask.setDestinationBin(jObject.getString("DestinationStorageBin"));
				inboundTask.setHandlingUnit("");
				psaTasksList.add(inboundTask);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return psaTasksList;
	}

}
