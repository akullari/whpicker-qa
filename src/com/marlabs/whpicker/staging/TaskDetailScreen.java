package com.marlabs.whpicker.staging;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.CustomButton;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.screens.KeyPadReaderScreen;
import com.marlabs.whpicker.screens.NumericReaderScreen;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;

public class TaskDetailScreen extends Activity implements IScannerListener{

	private CustomButton mButton1, mButton2, mButton3, mButton4, mButton5;
	private TextView mTextViewManual;
	private CustomActionBar actionBar;
	private boolean isHWUT = false;;
	private Task mParentTask;
	private CustomButton mCurrentFocusedButton, mNextFocusedButton;
	private Drawable mButtonDrawable;
	//private Animation animation1, animation2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom_validations);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		//animation1 = AnimationUtils.loadAnimation(this, R.anim.fliptomiddle);
		//animation2 = AnimationUtils.loadAnimation(this, R.anim.flipfrommiddle);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			mParentTask = (Task) bundle.getSerializable(AppConstants.INBOUND_ORDER);
			actionBar.changeHeader(mParentTask.getWHOrder());
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}
		switchViews();
	}

	private void switchViews(){
		setContentView(R.layout.activity_custom_validations);
		mButton1 = (CustomButton) findViewById(R.id.validation_button1);
		mButton2 = (CustomButton) findViewById(R.id.validation_button2);
		mButton3 = (CustomButton) findViewById(R.id.validation_button3);
		mButton4 = (CustomButton) findViewById(R.id.validation_button4);
		mButton5 = (CustomButton) findViewById(R.id.validation_button5);
		mTextViewManual = (TextView) findViewById(R.id.bottomlayout_textview_manual);

		mButton1.setOnClickListener(validationButonClickLisener);
		mButton2.setOnClickListener(validationButonClickLisener);
		mButton3.setOnClickListener(validationButonClickLisener);
		mButton4.setOnClickListener(validationButonClickLisener);
		mButton5.setOnClickListener(validationButonClickLisener);

		mButton1.setEnabled(true);
		mButton2.setEnabled(false);
		mButton3.setEnabled(false);
		mButton4.setEnabled(false);
		mButton5.setEnabled(false);

		mTextViewManual.setText("Enter manually");
		mButton1.setAsVerified();

		mTextViewManual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mCurrentFocusedButton == mButton3){
					Intent intent  = new Intent(TaskDetailScreen.this, NumericReaderScreen.class);
					intent.putExtra(AppConstants.NUMERIC_KEYPAD_ISFORRESOURCE,false);
					intent.putExtra(AppConstants.NUMERIC_KEYPAD_INPUT, mParentTask.getTotalQuantity());
					startActivityForResult(intent, 1);
				}
				else{
					String input;
					if(mCurrentFocusedButton == mButton2 && !isHWUT){
						input = mParentTask.getProduct();
					}
					else{
						input = "";
					}
					Intent intent  = new Intent(TaskDetailScreen.this, KeyPadReaderScreen.class);
					intent.putExtra(AppConstants.ALPHA_KEYPAD_TITLE, mTextViewManual.getText().toString());
					intent.putExtra(AppConstants.ALPHA_KEYPAD_BATCHINPUT, input);
					startActivityForResult(intent, 1);
				}
			}
		});
		setDataTovalidation();
	}

	OnClickListener validationButonClickLisener = new OnClickListener() {
		@SuppressLint("NewApi")
		@Override
		public void onClick(View view) {
			CustomButton button = (CustomButton) view;
			if(mCurrentFocusedButton != null){
				mCurrentFocusedButton.setBackground(mButtonDrawable);
				mCurrentFocusedButton.clearAnimation();
				mCurrentFocusedButton.setAsNotVerified();
			}
			button.setAsNotVerified();
			mCurrentFocusedButton = null;
			mNextFocusedButton = button;
			changeValidationCustomButtonFocus();
			index--;
		}
	};
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
			if (resultCode== RESULT_OK) {
				String result = data.getStringExtra("INPUT_RESULT");
				onManualDialogClosed(result);
			}
			else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.manual_entry_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void onManualDialogClosed(String result){
		if(mCurrentFocusedButton == mButton2 && !isHWUT)
			switchButtons();
		else{
			if(mCurrentFocusedButton.getTag().toString().equalsIgnoreCase(result))
				switchButtons();
			else
				mCurrentFocusedButton.setAsWrongVerified();
		}
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	int index = 0;
	private void switchButtons() {
		index = index+1;
		if(index < mValidationButtonsList.size()){
			mNextFocusedButton = mValidationButtonsList.get(index);
			mValidationButtonsList.get(index).setEnabled(true);
		}
		else{
			mNextFocusedButton = null;
		}
		changeValidationCustomButtonFocus();
	}

	@SuppressLint("NewApi")
	public void changeValidationCustomButtonFocus(){
		if(mCurrentFocusedButton != null){
			mCurrentFocusedButton.setBackground(mButtonDrawable);
			mCurrentFocusedButton.setAsVerified();
			mCurrentFocusedButton.clearAnimation();
		}

		if(mNextFocusedButton != null){
			mButtonDrawable = mNextFocusedButton.getBackground();
			mNextFocusedButton.setBackgroundResource(R.drawable.header);
			mNextFocusedButton.startAnimation(Utility.getInstance().blinkAnimation());
			mCurrentFocusedButton = mNextFocusedButton;
			mCurrentFocusedButton.removeVerificationForBlink();
			mTextViewManual.setText("Validate "+ mCurrentFocusedButton.getTitle() +" Manually");
		}
		else{
			mCurrentFocusedButton = null;
		}
	}

	List<CustomButton> mValidationButtonsList = new ArrayList<CustomButton>();
	private void setDataTovalidation() {

		mButton1.setTitleAndText("Source Bin", mParentTask.getSourceBin());
		mButton1.setTag(mParentTask.getSourceBin());
		if(!mParentTask.getSourceBin().isEmpty())
			mValidationButtonsList.add(mButton1);

		if(mParentTask.getHandlingUnitWarehouseTask().equalsIgnoreCase("X")){
			isHWUT = true;

			mButton2.setTitleAndText("HU", Util.removeLeadingZeros(mParentTask.getHandlingUnit()));
			mButton2.setTag(mParentTask.getHandlingUnit());
			if(!mParentTask.getHandlingUnit().isEmpty())
				mValidationButtonsList.add(mButton2);

			mButton3.setTitleAndText("Destination Bin", mParentTask.getDestinationBin());
			mButton3.setTag(mParentTask.getDestinationBin());
			if(!mParentTask.getDestinationBin().isEmpty())
				mValidationButtonsList.add(mButton3);

			mButton4.setVisibility(View.GONE);
			mButton5.setVisibility(View.GONE);
		}
		else{
			isHWUT = false;
			mButton2.setTitleAndText("Product", mParentTask.getProduct());
			mButton2.setTag(mParentTask.getProduct());
			if(!mParentTask.getProduct().isEmpty())
				mValidationButtonsList.add(mButton2);

			mButton3.setTitleAndText("Qunatity", mParentTask.getTotalQuantity()+" "+mParentTask.getQuantityUoM());
			mButton3.setTag(mParentTask.getTotalQuantity());
			//if(!mParentTask.getTotalQuantity().isEmpty())
				mValidationButtonsList.add(mButton3);

			mButton4.setTitleAndText("Serial", mParentTask.getSerial());
			mButton4.setTag(mParentTask.getSerial());
			if(!mParentTask.getSerial().isEmpty())
				mValidationButtonsList.add(mButton4);

			mButton5.setTitleAndText("Destination Bin", mParentTask.getDestinationBin());
			mButton5.setTag(mParentTask.getDestinationBin());
			if(!mParentTask.getDestinationBin().isEmpty())
				mValidationButtonsList.add(mButton5);
		}

		mCurrentFocusedButton = null;
		mNextFocusedButton = mButton1;
		changeValidationCustomButtonFocus();
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main_picker_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		onManualDialogClosed(scanValue);
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}
}