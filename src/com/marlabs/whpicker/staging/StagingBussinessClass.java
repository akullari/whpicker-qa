package com.marlabs.whpicker.staging;

import org.json.JSONObject;

import android.content.Context;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.networking.ConnectionDetector;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.networking.VolleyRequestHandler;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;

public class StagingBussinessClass {

	public void callPSAListVolleyRequest(Context context) {
		if(ConnectionDetector.getInstance(context).isConnectingToInternet()){
			try {
				String url = URLFactory.getPSAList_URL(context);
				new VolleyRequestHandler(context).callVolleyJSONRequest(context, Method.GET, url, null, Util.getBasicHANAHeaders(context),
						ProductsListScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(context, context.getString(R.string.no_internet_connectivity));
		}
	}
}
