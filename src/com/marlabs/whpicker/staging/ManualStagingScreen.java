package com.marlabs.whpicker.staging;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.networking.ConnectionDetector;
import com.marlabs.whpicker.networking.VolleyRequestHandler;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;

public class ManualStagingScreen extends Activity{
	private CustomActionBar actionBar;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manualstaging);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			if(bundle.get(AppConstants.SELECTED_MENU) != null){
				HomeMenuItem homeMenuItem = (HomeMenuItem) bundle.get(AppConstants.SELECTED_MENU);
				actionBar.changeHeader(homeMenuItem.getItemName());
			}
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		findViewById(R.id.manual_staging_product).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

			}
		});
		
		findViewById(R.id.manual_staging_hu).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

			}
		});

		//callVolleyRequest(URLFactory.getInBoundQueues_URL(this));
	}

	@SuppressWarnings("unused")
	private void callVolleyRequest(String url) {
		if(ConnectionDetector.getInstance(this).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(this).callVolleyJSONRequest(this, Method.GET, url, null, Util.getBasicHANAHeaders(this),
						ManualStagingScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(ManualStagingScreen.this, getString(R.string.no_internet_connectivity));
		}
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){

		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(ManualStagingScreen.this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_right_out, R.anim.right_left_out);
		super.onBackPressed();
	}
}