package com.marlabs.whpicker.staging;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.adapters.QueueAdapter;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.models.Queue;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Utility;

public class ProductsListScreen extends Activity{
	private CustomActionBar actionBar;
	private ListView mListView;
	private StagingBussinessClass stagingBussinessClass;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.selected_delivery));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		stagingBussinessClass = new StagingBussinessClass();

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			if(bundle.get(AppConstants.SELECTED_MENU) != null){
				HomeMenuItem homeMenuItem = (HomeMenuItem) bundle.get(AppConstants.SELECTED_MENU);
				actionBar.changeHeader(homeMenuItem.getItemName());
			}
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		LinearLayout mLinearLayout = (LinearLayout) findViewById(R.id.queueslist_headers);
		mLinearLayout.setVisibility(View.GONE);
		mListView = (ListView) findViewById(R.id.pulltorefresh_listview);

		stagingBussinessClass.callPSAListVolleyRequest(this);

		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					TextView textView = (TextView) view.findViewById(R.id.queue);
					String queue = textView.getText().toString();
					Intent intent = new Intent(ProductsListScreen.this, ProductTasksListScreen.class);
					System.out.println(queue);
					intent.putExtra(AppConstants.INBOUND_QUEUE, queue);
					startActivity(intent);
			}
		});
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
				List<Queue> mStagingProductsList= Parser.parsePSAResult(jsonObject);
				if(mStagingProductsList.size() == 0){
					Utility.getInstance().showAlertDialog(ProductsListScreen.this, "No Queues in current WareHouse..!");
				}
				else{
					mListView.setAdapter(new QueueAdapter(this, mStagingProductsList));
				}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(ProductsListScreen.this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);	
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_right_out, R.anim.right_left_out);
		super.onBackPressed();
	}
}