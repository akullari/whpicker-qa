package com.marlabs.whpicker.staging;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.models.Task;
import com.marlabs.whpicker.networking.ConnectionDetector;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.networking.VolleyRequestHandler;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;

public class ProductTasksListScreen extends Activity{
	private CustomActionBar actionBar;
	private ListView mListView;
	private LinearLayout mLinearLayout;
	private List<Task> mInboundOrdersList;
	private String mSelectedPSA;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			mSelectedPSA = bundle.getString(AppConstants.INBOUND_QUEUE);
		}
		else{
			actionBar.setUpTitle(getString(R.string.home));
		}

		actionBar = new CustomActionBar(getActionBar(), this, mSelectedPSA);
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle(getString(R.string.back));
		getActionBar().setDisplayHomeAsUpEnabled(true);

		mLinearLayout = (LinearLayout) findViewById(R.id.queueslist_headers);
		mLinearLayout.setVisibility(View.GONE);
		mListView = (ListView) findViewById(R.id.pulltorefresh_listview);
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(ProductTasksListScreen.this, TaskDetailScreen.class);
				intent.putExtra(AppConstants.INBOUND_ORDER, mInboundOrdersList.get(position));
				startActivity(intent);
			}
		});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		callVolleyRequest(URLFactory.getPSATasks_URL(this, mSelectedPSA));
	}

	private void callVolleyRequest(String url) {
		if(ConnectionDetector.getInstance(this).isConnectingToInternet()){
			try {
				new VolleyRequestHandler(this).callVolleyJSONRequest(this, Method.GET, url, null, Util.getBasicHANAHeaders(this),
						ProductTasksListScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Utility.getInstance().showAlertDialog(ProductTasksListScreen.this, getString(R.string.no_internet_connectivity));
		}
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			System.out.println("------ * response * -----" + jsonObject.toString());
			mInboundOrdersList = Parser.parseStagingTasksResult(jsonObject);
			if(mInboundOrdersList.size() == 0){
				Utility.getInstance().showAlertDialog(ProductTasksListScreen.this, getString(R.string.no_orders_in_selected_queue));
			}
			else{
				//mListView.setAdapter(new InboundOrdersListAdapter(ProductTasksListScreen.this, mInboundOrdersList));
			}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Utility.getInstance().showAlertDialog(ProductTasksListScreen.this, getString(R.string.unable_to_connect_server_please_try_again));
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main_picker_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);	
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_right_out, R.anim.right_left_out);
		super.onBackPressed();
	}

}