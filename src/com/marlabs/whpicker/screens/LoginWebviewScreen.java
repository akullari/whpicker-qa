package com.marlabs.whpicker.screens;

import java.util.List;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.marlabs.whpicker.customviews.VoiceMessage;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.models.User;
import com.marlabs.whpicker.networking.ConnectionDetector;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.networking.VolleyRequestHandler;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpickerqa.R;

public class LoginWebviewScreen extends Activity  {

	private WebView webView;
	private ProgressDialog mProgressDialog;
	private SharedPreferences.Editor mEditor;
	private final String LOGIN_SUCCESSFUL = "LOGIN_SUCCESSFUL";
	private String SAML_AUTHENTICATION_URL;
	private CookieManager cookieManager;
	private VoiceMessage mVoiceMessage;
	private Button mButtonRefresh;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_loginwebview);
		getActionBar().hide();
		mVoiceMessage = new VoiceMessage(this);
		SAML_AUTHENTICATION_URL = URLFactory.getSAMLAuthURL(this);
		System.out.println("SAML_AUTHENTICATION_URL-----------" + SAML_AUTHENTICATION_URL);
		
		Resource.releaseSelectedResource();
		
		SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
		mEditor = sharedPreferences.edit();

		cookieManager = CookieManager.getInstance();
		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			cookieManager.removeAllCookie();
			cookieManager.removeSessionCookie();
			startActivity(new Intent(LoginWebviewScreen.this, LoginScreen.class));
			finish();
		}
		else{
			init();
		}
	}

	boolean isInitLoad=true;
	@SuppressLint("SetJavaScriptEnabled")
	private void init() {
		mProgressDialog = ProgressDialog.show(LoginWebviewScreen.this, null, getString(R.string.loading_please_wait));
		mProgressDialog.setCancelable(false);
		webView = (WebView) findViewById(R.id.webView1);
		mButtonRefresh = (Button) findViewById(R.id.button_refresh_webview);
		
		mButtonRefresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				isInitLoad=true;
			    CookieManager cookieManager = CookieManager.getInstance();
			    cookieManager.removeAllCookie();

			    webView.loadUrl(SAML_AUTHENTICATION_URL);
				mButtonRefresh.setVisibility(View.GONE);
				webView.setVisibility(View.VISIBLE);
			}
		});

		isInitLoad=true;
		CookieSyncManager.createInstance(this);
	    CookieManager cookieManager = CookieManager.getInstance();
	    cookieManager.removeAllCookie();
		    
		webView.loadUrl(SAML_AUTHENTICATION_URL);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new SSLTolerentWebViewClient());
		webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
		webView.setWebChromeClient(new WebChromeClient());
	}


	@Override
	protected void onPause() {
		super.onPause();
		mProgressDialog.dismiss();
	}

	private class SSLTolerentWebViewClient extends WebViewClient  implements IScannerListener{
		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
			handler.proceed();
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			mProgressDialog.show();
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			super.onReceivedError(view, errorCode, description, failingUrl);
			mButtonRefresh.setVisibility(View.VISIBLE);
			webView.setVisibility(View.GONE);
			Toast.makeText(getApplicationContext(), getString(R.string.unable_to_connect_server_please_try_again), Toast.LENGTH_SHORT).show();
		}

		private boolean isVocalMessageDone = false;
		@Override
		public void onPageFinished(WebView view, String url) {
			
			super.onPageFinished(view, url);
			if(url.contains("/adfs/ls/?")){
				if(!isVocalMessageDone){
					isVocalMessageDone = true;
					mVoiceMessage.speak(getString(R.string.scan_your_user_name));
				}
				StaticScanner.getInstance().scannerRead(this, 3);
			}

			
			if(mProgressDialog != null)
				mProgressDialog.dismiss();
			String cookies = CookieManager.getInstance().getCookie(url);
			if(!isInitLoad && cookies != null){
				if(SAML_AUTHENTICATION_URL.equalsIgnoreCase(url)){
					System.out.println("Success");
					mEditor.putString("COOKIE", cookies);
					mEditor.commit();
					//System.out.println(cookies);
					webView.setVisibility(View.INVISIBLE);
					webView.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
				}
			}
			isInitLoad=false;
		}

		@Override
		public void ScanSuccessful(String scanValue, int sourceIndex) {
			String javaScript="javascript: if('email'==document.activeElement.type)\r\ndocument.activeElement.value='"+ scanValue +"';\r\n document.getElementById('passwordInput').focus();";
			webView.loadUrl(javaScript);
		}

		@Override
		public void ScanFailed() {
			// TODO Auto-generated method stub
		}
	}

	class MyJavaScriptInterface{
		@JavascriptInterface
		public void processHTML(String html){
			openLoginActivity(Jsoup.parse(html).text());
			System.out.println(Jsoup.parse(html).text());
		}
	}

	private void openLoginActivity(String text) {
		User user = Parser.ParseBasicUserData(text);
		if(user != null){
			mEditor.putString(AppConstants.USER_NAME_CONSTANT, user.getFullName());
			callLoginVolleyRequest(user.getUserID());
		}
	}

	void callLoginVolleyRequest(String userName) {
		if(ConnectionDetector.getInstance(this).isConnectingToInternet()){
			try {
				String url = URLFactory.getUserLogin_URL(this, userName);
				new VolleyRequestHandler(this).callVolleyJSONRequest(this, Method.GET, url, null, Util.getBasicHANAHeaders(this),
						LoginWebviewScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Util.showMessageDialog(this, getString(R.string.no_internet_connectivity));
		}
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			System.out.println(jsonObject);
			List<User> userData = Parser.ParseLoggedInUserData(jsonObject);
			if(userData.size() == 0){

				AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginWebviewScreen.this);
				builder1.setTitle(getResources().getString(R.string.app_name));
				builder1.setMessage(getString(R.string.you_are_not_associated_with_any_warehouse_press_ok_to_logout));
				builder1.setCancelable(true);
				builder1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						Intent intent = new Intent(LoginWebviewScreen.this, LoginWebviewScreen.class);
						intent.putExtra(AppConstants.LOGOUT, AppConstants.LOGOUT);
						startActivity(intent);
						finish();
					}
				});

				AlertDialog alert11 = builder1.create();
				alert11.setCancelable(false);
				alert11.show();
				//Toast.makeText(getApplicationContext(), "Invalid User Credentials. Please try again..!", Toast.LENGTH_SHORT).show();
			}
			else{
				User user = userData.get(0);
				mEditor.putBoolean(LOGIN_SUCCESSFUL, true);
				mEditor.putString(AppConstants.USER_CLIENT, user.getClient());
				mEditor.putString(AppConstants.USER_ID, user.getUserID());
				mEditor.putString(AppConstants.USER_LANGUAGE, user.getLanguage());
				mEditor.putString(AppConstants.USER_WAREHOUSE_NUMBER, user.getWarehouseNumber());
				mEditor.commit();
				
				System.out.println("Language------" + user.getLanguage());
				Util.setUserlanguage(getBaseContext(), user.getLanguage());
				
				Intent intent = new Intent(this, HomeActivity.class);
				startActivity(intent);
				finish();
			}
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(LoginWebviewScreen.this, LoginWebviewScreen.class);
			intent.putExtra(AppConstants.LOGOUT, AppConstants.LOGOUT);
			startActivity(intent);
			finish();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mVoiceMessage.destroySpeechEngine();
	}
}