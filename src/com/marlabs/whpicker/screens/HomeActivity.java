package com.marlabs.whpicker.screens;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.adapters.HomeMenuAdapter;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.inbound.InboundProcessorScreen;
import com.marlabs.whpicker.inbound.PutAwayByWOScreen;
import com.marlabs.whpicker.inbound.ReceiveHUScreen;
import com.marlabs.whpicker.inbound.UnloadingScreen;
import com.marlabs.whpicker.models.ConsolidationItem;
import com.marlabs.whpicker.models.HomeMenuItem;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.networking.ConnectionDetector;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.networking.VolleyRequestHandler;
import com.marlabs.whpicker.outbound.ByProcessorScreen;
import com.marlabs.whpicker.outbound.ByQueuesScreen;
import com.marlabs.whpicker.outbound.ByWHOScreen;
import com.marlabs.whpicker.outbound.ConsolidationScreen;
import com.marlabs.whpicker.staging.ManualStagingScreen;
import com.marlabs.whpicker.staging.ProductsListScreen;
import com.marlabs.whpicker.tparsers.OutboundParser;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.MenuDataSorce;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Utility;
import com.marlabs.whpicker.utility.Validation;

public class HomeActivity extends Activity {
	private String mParentName = "";
	public static String sSelectedQueue = null;
	public static int sSelectedParentID = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		CustomActionBar actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.home));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle("");

		getActionBar().setDisplayHomeAsUpEnabled(true);

		ListView listView = (ListView) findViewById(R.id.listview_menu);

		List<HomeMenuItem> menuList = new ArrayList<HomeMenuItem>();

		Bundle bundle = getIntent().getExtras();
		/* Handling received intents and logout functionality */
		if((bundle != null) && (bundle.get("menuitem")!=null)){
			HomeMenuItem menuitem = (HomeMenuItem) bundle.get("menuitem");
			menuList = MenuDataSorce.getInstance().getData(this).get(menuitem.getItemId());
			actionBar.setUpTitle(bundle.getString("parentText"));
			actionBar.changeHeader(menuitem.getItemName());
			mParentName = menuitem.getItemName();
		}
		else if(bundle != null && (bundle.getString(AppConstants.LOGOUT)!=null)){
			userLogout();
		}
		else{
			menuList = MenuDataSorce.getInstance().getMainMenuData(this);
			actionBar.changeHeader("WareHouse Picker");
			mParentName = getString(R.string.home);
			getActionBar().setDisplayHomeAsUpEnabled(false);
			if (AppConstants.getResources().size() == 0) {
				callVolleyRequest();
			}
		}

		listView.setAdapter(new HomeMenuAdapter(HomeActivity.this, menuList));
		listView.setOnItemClickListener(listItemListener);
	}

	OnItemClickListener listItemListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			HomeMenuItem menuItemClicked = (HomeMenuItem) parent.getItemAtPosition(position);

			if(MenuDataSorce.getInstance().getData(HomeActivity.this).containsKey(menuItemClicked.getItemId())){
				Intent intent = new Intent(HomeActivity.this, HomeActivity.class);

				intent.putExtra("menuitem", menuItemClicked);
				intent.putExtra("parentText", mParentName);

				startActivity(intent);
				overridePendingTransition(R.anim.left_right_in, R.anim.right_left_in);
			}
			else{
				openChildActivity(menuItemClicked);
			}
		}
	};

	private void callVolleyRequest() {
		if(ConnectionDetector.getInstance(this).isConnectingToInternet()){
			try {
				String url = URLFactory.getResources_URL(this);
				new VolleyRequestHandler(this).callVolleyJSONRequest(this, Method.GET, url, null, Util.getBasicHANAHeaders(this),
						HomeActivity.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Util.showMessageDialog(this, getString(R.string.no_internet_connectivity));
		}
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			//System.out.println(jsonObject);
			List<Resource> resourcesList = Parser.parseResourcesResponse(jsonObject);
			if(resourcesList.size() == 0){
				Toast.makeText(getApplicationContext(), "No Resources..!", Toast.LENGTH_SHORT).show();
				return;
			}
			AppConstants.setResources(resourcesList);
			Util.setCurrentResource(HomeActivity.this, resourcesList);
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Toast.makeText(getApplicationContext(), "Unable to fetch Resources..!", Toast.LENGTH_SHORT).show();
		}
	}
	
	private void openChildActivity(HomeMenuItem homeMenuItem) {
		int id = Integer.parseInt(homeMenuItem.getItemId().trim());
		Intent intent = null;
		switch(id){

		case MenuDataSorce.INBOUND_UNLOADING_DELIVERY: 
			intent = new Intent(HomeActivity.this, UnloadingScreen.class);
			break;

		case MenuDataSorce.INBOUND_UNLOADING_TU: 
			intent = new Intent(HomeActivity.this, UnloadingScreen.class);
			break;

		case MenuDataSorce.INBOUND_UNLOADING_HU: 
			intent = new Intent(HomeActivity.this, UnloadingScreen.class);
			break;

		case MenuDataSorce.INBOUND_UNLOADING_ASN: 
			intent = new Intent(HomeActivity.this, UnloadingScreen.class);
			break;

		case MenuDataSorce.INBOUND_RECEIVEBYHU_DELIVERY: 
			intent = new Intent(HomeActivity.this, ReceiveHUScreen.class);
			break;

		case MenuDataSorce.INBOUND_RECEIVEBYHU_TU: 
			intent = new Intent(HomeActivity.this, ReceiveHUScreen.class);
			break;

		case MenuDataSorce.INBOUND_RECEIVEBYHU_ASN: 
			intent = new Intent(HomeActivity.this, ReceiveHUScreen.class);
			break;

		case MenuDataSorce.INBOUND_PUTAWAY_QUEUE: 
			intent = new Intent(HomeActivity.this, ByQueuesScreen.class);
			break;

		case MenuDataSorce.INBOUND_PUTAWAY_HU: 
			intent = new Intent(HomeActivity.this, PutAwayByWOScreen.class);
			break;

		case MenuDataSorce.INBOUND_PUTAWAY_WO: 
			intent = new Intent(HomeActivity.this, PutAwayByWOScreen.class);
			break;

		case MenuDataSorce.INBOUND_PROCESSOR: 
			intent = new Intent(HomeActivity.this, InboundProcessorScreen.class);
			break;

		case MenuDataSorce.OUTBOUND_QUEUE: 
			intent = new Intent(HomeActivity.this, ByQueuesScreen.class);
			break;

		case MenuDataSorce.OUTBOUND_WO: 
			intent = new Intent(HomeActivity.this, ByWHOScreen.class);
			break;

		case MenuDataSorce.OUTBOUND_PROCESSOR:
			intent = new Intent(HomeActivity.this, ByProcessorScreen.class);
			break;

		case 311:
			intent = new Intent(HomeActivity.this, ProductsListScreen.class);
			break;

		case 312:
			intent = new Intent(HomeActivity.this, ManualStagingScreen.class);
			break;

		case 25:
			if(Resource.getSelectedResource() != null)
				intent = new Intent(HomeActivity.this, ConsolidationScreen.class);
			else
				Toast.makeText(getApplicationContext(), "Please select a Resource.", Toast.LENGTH_SHORT).show();
			break;


		default : 
			Toast.makeText(getApplicationContext(), "Comming Soon...", Toast.LENGTH_SHORT).show();
			break;
		}

		if(intent != null){
			if(id != 25)
				intent.putExtra(AppConstants.SELECTED_MENU, homeMenuItem);
			startActivity(intent);
		}
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_right_out, R.anim.right_left_out);
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		//		inflater.inflate(R.menu.activity_main_picker_screen, menu);
		inflater.inflate(R.menu.activity_order_details_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;

			//		case R.id.action_scanner_item:
			//			logoutPromptDialog();
			//			break;

		case R.id.action_settings_item:
			Utility.getInstance().showSettingsDialog(HomeActivity.this);
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	private void userLogout(){
		//SharedPreferences sharedPreferencesResource = getSharedPreferences(AppConstants.RESOURCE_PREFERENCE, MODE_PRIVATE);
		//String resource = sharedPreferencesResource.getString(AppConstants.RESOURCE_NAME, "");
		if(Resource.getSelectedResource()== null){
			logout();
		}
		else if(Resource.getSelectedResource().getAccomadatedTasks().size() == 0){
			logout();
		}
		else{
			releaseResourcePromptDialog();
		}
	}

	private void releaseResourcePromptDialog() {
		new AlertDialog.Builder(this).setTitle("Release Resource")
		.setMessage("Do you want to release allocated Resource?")
		.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				Intent intent = new Intent(HomeActivity.this, ConsolidationScreen.class);
				intent.putExtra(AppConstants.LOGOUT, AppConstants.LOGOUT);
				startActivity(intent);
				finish();
			}
		})
		.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				logout();
			}
		}).show();
	}

	private void logout(){
		SharedPreferences.Editor editor = getSharedPreferences(getPackageName(), MODE_PRIVATE).edit();
		editor.remove("LOGIN_SUCCESSFUL");
		editor.clear();
		editor.commit();

		Intent intent = new Intent(HomeActivity.this, LoginWebviewScreen.class);
		intent.putExtra(AppConstants.LOGOUT, AppConstants.LOGOUT);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}
	
	public void onResourceTasksResponse(JSONObject jsonObject, VolleyError volleyError){
		System.out.println("Tasks Fetched"+ jsonObject.toString());
		if(volleyError == null){
			List<ConsolidationItem> consolidationItemsList = OutboundParser.parsePackingItemsResult(jsonObject);
			if(consolidationItemsList.size() == 0){
				return;
			}
			int logicalPosition = 0;
			for(ConsolidationItem consolidationItem: consolidationItemsList){
				int lastSlotNumber=Resource.getSelectedResource().getLastOccupiedSlotNumber();
				Resource.getSelectedResource().addTask(consolidationItem);
				if(!Validation.isNullString(consolidationItem.getSlot()))
					logicalPosition = Integer.parseInt(consolidationItem.getSlot());

				if(lastSlotNumber>logicalPosition)
					lastSlotNumber = logicalPosition;
			}
			Resource.getSelectedResource().setLastOccupiedSlotNumber(logicalPosition);
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
		}
	}
}