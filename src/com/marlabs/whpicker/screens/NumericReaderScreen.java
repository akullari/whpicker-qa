package com.marlabs.whpicker.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.customviews.VoiceMessage;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;
import com.marlabs.whpicker.utility.Validation;

public class NumericReaderScreen extends Activity implements IScannerListener {
	private TextView numericInputText, suggestionText;
	private int output, targetQuantity, mSuppliedQuantity;
	private String scanIncrementorText, mQuantityUoM, result = "";
	private boolean isForResource;
	private VoiceMessage mVoiceMessage;
	private boolean isClearable = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_numeric_reader_screen);

		CustomActionBar actionBar = new CustomActionBar(getActionBar(), this, getString(R.string.enter_quantity));
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle("");
		getActionBar().setDisplayHomeAsUpEnabled(false);

		mVoiceMessage = new VoiceMessage(this);

		numericInputText = (TextView) findViewById(R.id.screenText);
		suggestionText = (TextView) findViewById(R.id.expectedQuantityText);

		Bundle bundle = getIntent().getExtras();
		isForResource =bundle.getBoolean(AppConstants.NUMERIC_KEYPAD_ISFORRESOURCE);
		if(!isForResource)
		{
			isForResource = false;
			targetQuantity = bundle.getInt(AppConstants.NUMERIC_KEYPAD_INPUT);
			mQuantityUoM = bundle.getString(AppConstants.NUMERIC_KEYPAD_INPUT_UNITS);
			scanIncrementorText=bundle.getString(AppConstants.NUMERIC_KEYPAD_SCANVALUE);
			mSuppliedQuantity  = bundle.getInt(AppConstants.NUMERIC_KEYPAD_REMAINING_INPUT);
			int defaultInput = bundle.getInt(AppConstants.NUMERIC_KEYPAD_ENTERED_VALUE);
			if((defaultInput)>0)
			{
				numericInputText.setText(defaultInput+"");
				result = defaultInput+"";
				mSuppliedQuantity -= defaultInput;
			}
			if(targetQuantity == 1 && mSuppliedQuantity == 1)
			{
				changeScreenText(targetQuantity+"");
				setReturnData();
			}
			else if(!Validation.isNullString(result)){
				isClearable = true;
				changeScreenText(result);
			}
			else{
				//				if(mSuppliedQuantity == 0)
				//					changeScreenText("1");
				//				else
				changeScreenText("0");
			}
		}
		else{
			isForResource = true;
			suggestionText.setText(getString(R.string.please_enter_resource_capacity));
			String passedResource = bundle.getString(AppConstants.NUMERIC_KEYPAD_INPUT);
			actionBar.changeHeader(passedResource + " "+getString(R.string.capacity));
		}

		Button numericButton0 = (Button) findViewById(R.id.numericButton0);
		Button numericButton1 = (Button) findViewById(R.id.numericButton1);
		Button numericButton2 = (Button) findViewById(R.id.numericButton2);
		Button numericButton3 = (Button) findViewById(R.id.numericButton3);
		Button numericButton4 = (Button) findViewById(R.id.numericButton4);
		Button numericButton5 = (Button) findViewById(R.id.numericButton5);
		Button numericButton6 = (Button) findViewById(R.id.numericButton6);
		Button numericButton7 = (Button) findViewById(R.id.numericButton7);
		Button numericButton8 = (Button) findViewById(R.id.numericButton8);
		Button numericButton9 = (Button) findViewById(R.id.numericButton9);

		Button numericButtonC = (Button) findViewById(R.id.numericButtonC);
		Button numericButtonDot = (Button) findViewById(R.id.numericButtonDot);
		Button numericButtonOk = (Button) findViewById(R.id.numericButtonOk);
		Button numericButtonDelete = (Button) findViewById(R.id.numericButtonDelete);

		numericButton0.setOnClickListener(buttonClickListener);
		numericButton1.setOnClickListener(buttonClickListener);
		numericButton2.setOnClickListener(buttonClickListener);
		numericButton3.setOnClickListener(buttonClickListener);
		numericButton4.setOnClickListener(buttonClickListener);
		numericButton5.setOnClickListener(buttonClickListener);
		numericButton6.setOnClickListener(buttonClickListener);
		numericButton7.setOnClickListener(buttonClickListener);
		numericButton8.setOnClickListener(buttonClickListener);
		numericButton9.setOnClickListener(buttonClickListener);

		numericButtonC.setOnClickListener(buttonClickListener);
		numericButtonDot.setOnClickListener(buttonClickListener);
		numericButtonOk.setOnClickListener(buttonClickListener);
		numericButtonDelete.setOnClickListener(buttonClickListener);

		StaticScanner.getInstance().scannerRead(this, 1);
	}

	public void setReturnData(){
		String valueEntered = numericInputText.getText().toString().trim();
		if(Validation.isNullString(valueEntered)){
			Toast.makeText(getApplicationContext(), getString(R.string.please_enter_valid_quantity), Toast.LENGTH_SHORT).show();
			return;
		}

		int count = Integer.parseInt(valueEntered);
		if(count <= 0){
			Toast.makeText(getApplicationContext(), getString(R.string.please_enter_valid_quantity), Toast.LENGTH_SHORT).show();
		}

		//else if(!isForResource && !Validation.isNullString(mRemainingQuantity) && count > Integer.parseInt(mRemainingQuantity)){
		else if(!isForResource && (count + mSuppliedQuantity)> targetQuantity)
		{
			Toast.makeText(getApplicationContext(), getString(R.string.please_enter_valid_quantity), Toast.LENGTH_SHORT).show();
		}
		else if(isForResource && mSuppliedQuantity >= 100){
			Util.showMessageDialog(this, "Resource Capacity should be less than 100");
		}
		else{
			Intent returnIntent = new Intent();
			returnIntent.putExtra("INPUT_RESULT", count+"");
			setResult(RESULT_OK, returnIntent);
			finish();
		}
	}

	OnClickListener buttonClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			int tag = Integer.parseInt( (String) v.getTag() );
			evaluateValue(tag);
		}
	};

	private void evaluateValue(int tag) {
		switch (tag) {
		case 0:
			if (result.length()>0) {
				if(isClearable){
					result = "";
					isClearable = false;
				}
				result+="0";
			}
			break;

		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			if(isClearable){
				result = "";
				isClearable = false;
			}
			result += tag;
			break;

		case 11: // Backspace
			if (result.length()>0) {
				result = result.substring(0, result.length()-1);
			}
			//screenText.setText(result);
			changeScreenText(result);
			break;

		case 12: // OK
			setReturnData();
			break;

		case 13: // CLEAR
			result = "";
			//screenText.setText(result);
			changeScreenText(result);
			output = 0;
			break;

		case 14: // DOT.
			if(isClearable){
				result = "";
				isClearable = false;
			}
			if (!result.contains(".")) {
				if (result.length()==0) {
					result = "0.";
				}else{
					result += '.';
				}
			}
			break;

		case 15: // ++ or Hidden Incrementor button
			try{
				double readValue=0;
				if(!Validation.isNullString(result))
					readValue=Double.parseDouble(result);
				readValue++;
				int i = (int)readValue;
				result=i+"";
			}
			catch (NumberFormatException e){
			}
			break;

		default:
			break;
		}

		if (result.length()>0) {
			changeScreenText(result);
			output = Integer.parseInt(result);
		} else{
			changeScreenText("0");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_key_pad_reader_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.closeButton) {
			Intent returnIntent = new Intent();
			setResult(RESULT_CANCELED, returnIntent);
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	private void changeScreenText(String input){
		numericInputText.setText(input);
		int enteredQunatity;
		if(!isForResource){
			if(Validation.isNullString(input)){
				enteredQunatity = 0;
			}
			else{
				enteredQunatity = Integer.parseInt(input);
			}

			if((enteredQunatity + mSuppliedQuantity)<= targetQuantity)
			{
				suggestionText.setText(getString(R.string.expected_quantity)+" "+ (int)(enteredQunatity+mSuppliedQuantity) + "/" + (int)targetQuantity +" "+ mQuantityUoM);
				if((enteredQunatity + mSuppliedQuantity) == targetQuantity && !isClearable){
					setReturnData();
				}
			}
			else
			{
				String errorText = getString(R.string.exceeding_the_expected_quantity);
				suggestionText.setText(errorText);
				mVoiceMessage.speak(errorText);
			}
		}
	}

	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		if(1==sourceIndex && !Validation.isNullString(scanValue) && scanValue.equalsIgnoreCase(scanIncrementorText)){
			evaluateValue(15); //Incrementor tag
		}
		else{
			String errorMessage = getString(R.string.wrong)+ " " + getString(R.string.product) + ", "+ getString(R.string.please_repeat);
			mVoiceMessage.speak(errorMessage);
			Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
		}

		if((output+mSuppliedQuantity) < targetQuantity){
			StaticScanner.getInstance().scannerRead(this, 1);
		}
	}

	@Override
	public void ScanFailed() {
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mVoiceMessage.destroySpeechEngine();
	}
}
