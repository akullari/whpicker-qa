package com.marlabs.whpicker.screens;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.utility.AppConstants;

public class SplashScreen extends Activity {
	private final String LOGIN_SUCCESSFUL = "LOGIN_SUCCESSFUL";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_splash_screen);
		init();
	}
	
	private void init() {
		final SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
		SharedPreferences sharedPreferencesTTS = getSharedPreferences(AppConstants.TTS_PREFERENCE, MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferencesTTS.edit();
		editor.putBoolean(AppConstants.TTS_ENABLE, sharedPreferencesTTS.getBoolean(AppConstants.TTS_ENABLE, true));
		editor.commit();
		
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (sharedPreferences.getBoolean(LOGIN_SUCCESSFUL, false) == true) {
					Intent intent = new Intent(SplashScreen.this, LoginWebviewScreen.class);
					startActivity(intent);
					finish();
				}
				else{
					Intent intent = new Intent(SplashScreen.this, LoginScreen.class);
					startActivity(intent);
					finish();
				}
			}
		},3000L);
	}
}