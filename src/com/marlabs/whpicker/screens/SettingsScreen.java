package com.marlabs.whpicker.screens;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import android.widget.TextView;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.networking.UserInfo;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;

public class SettingsScreen extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custom_settings_dialog);

		CustomActionBar actionBar = new CustomActionBar(getActionBar(), this, AppConstants.SETTINGS);
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle("");
		getActionBar().setDisplayHomeAsUpEnabled(false);

		TextView textViewUserID = (TextView) findViewById(R.id.settings_textview_userid);
		TextView textViewUserClient = (TextView) findViewById(R.id.settings_textview_client);
		TextView textViewUserWHNo = (TextView) findViewById(R.id.settings_textview_whno);
		TextView textViewUserName = (TextView) findViewById(R.id.settings_textview_username);
		TextView textViewUserEnvironment = (TextView) findViewById(R.id.settings_textview_environment);
		TextView textViewUserVersion = (TextView) findViewById(R.id.settings_textview_version);
		SeekBar volumeSeekbar = (SeekBar) findViewById(R.id.settings_seekbar_ttsvolume);
		TextView textViewLogout = (TextView) findViewById(R.id.settings_textview_logout);
		Switch switchTTS = (Switch) findViewById(R.id.settings_switch_ttsenable);

		textViewUserID.setText(UserInfo.getInstance().getUserID(this));
		textViewUserClient.setText(UserInfo.getInstance().getClient(this));
		textViewUserWHNo.setText(UserInfo.getInstance().getWareHouse(this));
		textViewUserName.setText(UserInfo.getInstance().getUserFullName(this));
		textViewUserEnvironment.setText(UserInfo.getInstance().getSelectedEnvironment(this));
		try {
			PackageInfo packInfo= getPackageManager().getPackageInfo(getPackageName(), 0);
			textViewUserVersion.setText(packInfo.versionName);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

//		textViewUserVersion.setText(getResources().getString(R.string.app_version));
		final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		volumeSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
		volumeSeekbar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));   
		volumeSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,	progress, 0);
			}
		});

		SharedPreferences sharedPreferencesTTS = getSharedPreferences(AppConstants.TTS_PREFERENCE, Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = sharedPreferencesTTS.edit();
		boolean isTTSEnabled = sharedPreferencesTTS.getBoolean(AppConstants.TTS_ENABLE, true);
		switchTTS.setChecked(isTTSEnabled);
		switchTTS.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					editor.putBoolean(AppConstants.TTS_ENABLE, true);
				}
				else{
					editor.putBoolean(AppConstants.TTS_ENABLE, false);
				}
				editor.commit();
			}
		});

		textViewLogout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				logoutPromptDialog();
			}
		});
	}

	private void logoutPromptDialog() {
		new AlertDialog.Builder(this).setTitle(AppConstants.LOGOUT)
		.setMessage(getString(R.string.are_you_sure))
		.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//userLogout();
				dialog.dismiss();
				Util.userLogout(SettingsScreen.this);
			}
		})
		.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		}).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_settings_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.action_settings_close:
			onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);	
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		//overridePendingTransition(R.anim.push_down_in,R.anim.push_down_out);
	}
}