package com.marlabs.whpicker.screens;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.scanner.IScannerListener;
import com.marlabs.whpicker.scanner.StaticScanner;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Utility;

public class KeyPadReaderScreen extends Activity implements IScannerListener {

	private static final String TAG = KeyPadReaderScreen.class.getSimpleName();
	private EditText inputText;
	private String sourceName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_key_pad_reader_screen);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);

		inputText = (EditText) findViewById(R.id.inputText);
		TextView titleView = (TextView) findViewById(R.id.title);
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			titleView.setText(bundle.getString(AppConstants.ALPHA_KEYPAD_TITLE));
			if(bundle.get(AppConstants.ALPHA_KEYPAD_BATCHINPUT)!=null)
				inputText.setText(bundle.getString(AppConstants.ALPHA_KEYPAD_BATCHINPUT));
			if(bundle.get(AppConstants.ALPHA_KEYPAD_ISNUMERIC)!=null)
			{
				boolean isNumeric=bundle.getBoolean(AppConstants.ALPHA_KEYPAD_ISNUMERIC);
				if(isNumeric)
					inputText.setInputType(InputType.TYPE_CLASS_NUMBER);
			}
		}

		Button submitButton = (Button) findViewById(R.id.submitButton);
		Button resetButton = (Button) findViewById(R.id.resetButton);
		submitButton.setOnClickListener(submitButtonClickListener);
		resetButton.setOnClickListener(resetButtonClickListener);
		
		StaticScanner.getInstance().scannerRead(this, 1);
	}

	OnClickListener submitButtonClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (inputText==null||inputText.getText().length()<1) {
				Toast.makeText(KeyPadReaderScreen.this, "Please provide an input", Toast.LENGTH_LONG).show();
			}
			else{
				if (sourceName!=null && sourceName.length()>0) {
					if (sourceName.equalsIgnoreCase("warehouse")) {}
					else if (sourceName.equalsIgnoreCase("handling unit")) {
						Log.e(TAG, "Moving to task screen based on handling unit");
					}
				}
				else{
					Intent returnIntent = new Intent();
					returnIntent.putExtra("INPUT_RESULT", inputText.getText().toString().trim());
					setResult(RESULT_OK, returnIntent);
					inputText.setText("");
					finish();
				}
			}
			Utility.getInstance().hideKeyboard(KeyPadReaderScreen.this, inputText);
		}
	};

	OnClickListener resetButtonClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			inputText.setText("");
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_key_pad_reader_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.closeButton) {
			setResult(RESULT_CANCELED);
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void ScanSuccessful(String scanValue, int sourceIndex) {
		Intent returnIntent = new Intent();
		returnIntent.putExtra("INPUT_RESULT", scanValue);
		setResult(RESULT_OK, returnIntent);
		finish();
	}

	@Override
	public void ScanFailed() {
		StaticScanner.getInstance().scannerRead(this, 1);
	}
}