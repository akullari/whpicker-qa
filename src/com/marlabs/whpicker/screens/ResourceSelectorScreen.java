package com.marlabs.whpicker.screens;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.models.Resource;
import com.marlabs.whpicker.networking.ConnectionDetector;
import com.marlabs.whpicker.networking.URLFactory;
import com.marlabs.whpicker.networking.VolleyRequestHandler;
import com.marlabs.whpicker.tparsers.Parser;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpicker.utility.Util;

public class ResourceSelectorScreen extends Activity implements OnRefreshListener {

	private ListView mListView;
	private SwipeRefreshLayout swipeLayout;
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custom_selector_dialog_screen);

		getActionBar().hide();

		swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeLayout.setOnRefreshListener(this);
		swipeLayout.setColorScheme(android.R.color.holo_blue_bright, 
				android.R.color.holo_green_light, 
				android.R.color.holo_orange_light, 
				android.R.color.holo_red_light);

		Button cancelButton = (Button) findViewById(R.id.selectordialog_button_cancel);
		mListView = (ListView) findViewById(R.id.pulltorefresh_listview);
		if(AppConstants.getResources().size() > 0)
			mListView.setAdapter(new ResourceAdapter(this, AppConstants.getResources()));
		else
			callVolleyRequest();

		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				TextView textView = (TextView) view.findViewById(R.id.textview_resources_name);
				onItemSelected(textView.getText().toString().trim());
			}
		});

		cancelButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				setResult(RESULT_CANCELED);
				finish();
			}
		});
	}

	@Override 
	public void onRefresh() {
		swipeLayout.setRefreshing(false);
		callVolleyRequest();
	}

	private void onItemSelected(String selectedResource){
		Intent returnIntent = new Intent();
		returnIntent.putExtra("INPUT_RESULT", selectedResource);
		setResult(RESULT_OK, returnIntent);
		finish();
	}

	private void callVolleyRequest() {
		if(ConnectionDetector.getInstance(this).isConnectingToInternet()){
			try {
				String url = URLFactory.getResources_URL(this);
				new VolleyRequestHandler(this).callVolleyJSONRequest(this, Method.GET, url, null, Util.getBasicHANAHeaders(this),
						ResourceSelectorScreen.class.getMethod("onResponse", JSONObject.class, VolleyError.class));
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		else{
			Util.showMessageDialog(this, getString(R.string.no_internet_connectivity));
		}
	}

	public void onResponse(JSONObject jsonObject, VolleyError volleyError){
		if(volleyError == null){
			//System.out.println(jsonObject);
			List<Resource> resourcesList = Parser.parseResourcesResponse(jsonObject);
			if(resourcesList.size() == 0){
				Toast.makeText(getApplicationContext(), "No Resources..!", Toast.LENGTH_SHORT).show();
				return;
			}
			AppConstants.setResources(resourcesList);
			mListView.setAdapter(new ResourceAdapter(this, AppConstants.getResources()));
		}
		else{
			System.out.println("------ * callback on Failure * -----" + volleyError.toString());
			Toast.makeText(getApplicationContext(), "Unable to fetch Resources..!", Toast.LENGTH_SHORT).show();
		}
	}

	private class ResourceAdapter extends BaseAdapter {

		private LayoutInflater mLayoutInflater;
		private List<Resource> list;
		private Context context;
		public ResourceAdapter(Context context, List<Resource> list) {
			this.list = list;
			mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.context = context;
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Resource getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder; 
			if(convertView == null){
				viewHolder = new ViewHolder();
				convertView = mLayoutInflater.inflate(R.layout.listitem_resources, parent, false);
				viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textview_resources_name);
				viewHolder.textViewText = (TextView) convertView.findViewById(R.id.textview_resources_text);
				viewHolder.textViewType = (TextView) convertView.findViewById(R.id.textview_resources_type);
				convertView.setTag(viewHolder);
			}
			else{
				viewHolder = (ViewHolder)convertView.getTag();
			}

			if (position %2 ==0) {
				convertView.setBackgroundColor(context.getResources().getColor(R.color.grey_light));
			}
			else{
				convertView.setBackgroundColor(context.getResources().getColor(R.color.grey_dark));
			}

			Resource resource = list.get(position);
			viewHolder.textViewName.setText(resource.getResourceName());
			viewHolder.textViewText.setText(resource.getResourceText());
			if(Resource.RPT_MANUAL_PARING==resource.getResourceType())
				viewHolder.textViewType.setText("Manual");
			else if(Resource.RPT_AUTO_PARING==resource.getResourceType())
				viewHolder.textViewType.setText("Auto");
			else if(Resource.RPT_NO_PARING==resource.getResourceType())
				viewHolder.textViewType.setText("No Pairing");
			return convertView;
		}

		class ViewHolder{
			TextView textViewName;
			TextView textViewText;
			TextView textViewType;
		}
	}
}