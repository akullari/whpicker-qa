package com.marlabs.whpicker.screens;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.marlabs.whpicker.customviews.CustomActionBar;
import com.marlabs.whpicker.utility.AppConstants;
import com.marlabs.whpickerqa.R;

public class LoginScreen extends Activity{
	private String SAML_AUTHENTICATION_URL, BASE_URL_HANA, BASE_URL_GATEWAY;
	private Dialog mDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_screen);
		//		getActionBar().hide();
		showListSelectorDialog();
		CustomActionBar actionBar = new CustomActionBar(getActionBar(), this, "Login");
		actionBar.setCustomActionBarData();
		actionBar.setUpTitle("");

		TextView textView = (TextView) findViewById(R.id.login_textview_version);

		try {
			PackageInfo packInfo= getPackageManager().getPackageInfo(getPackageName(), 0);
			textView.setText(getString(R.string.whpicker_version)+": " + packInfo.versionName);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void showListSelectorDialog(){
		mDialog = new Dialog(this);
		mDialog.setContentView(R.layout.custom_env_dialog);
		mDialog.setTitle(R.string.please_select_environment);
		onEnvironmentSelected(1);

		TextView textViewDev = (TextView) mDialog.findViewById(R.id.env_textview_dev);
		TextView textViewQA = (TextView) mDialog.findViewById(R.id.env_textview_qa);
		textViewDev.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				onEnvironmentSelected(0);
			}
		});

		textViewQA.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				onEnvironmentSelected(1);
			}
		});

		mDialog.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				finish();
			}
		});

		//mDialog.show();
	}

	private void onEnvironmentSelected(int index){
		SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		String items[] = new String[] {getString(R.string.development_dev), getString(R.string.quality_qa)};
		switch(index){
		case 1 :
			SAML_AUTHENTICATION_URL = "https://sapqweb.sial.com:44371/sap/opu/odata/sap/ZGATEWAY_LOGIN_SAML_SRV/GWUserDefaultsSet('X')?sap-client=100&$format=json";
			BASE_URL_HANA="http://sapfw.sial.com/qse/sial/sapnext/";
			BASE_URL_GATEWAY="http://sapfw.sial.com/sapqgh/sap/opu/";
			editor.putString(AppConstants.SELECTED_ENV, items[1]);
			break;
		}

		editor.putString("SAML_AUTHENTICATION_URL", SAML_AUTHENTICATION_URL);
		editor.putString("BASE_URL_HANA", BASE_URL_HANA);
		editor.putString("BASE_URL_GATEWAY", BASE_URL_GATEWAY);
		editor.commit();

		startActivity(new Intent(LoginScreen.this, LoginWebviewScreen.class));
		if(mDialog.isShowing())
			mDialog.dismiss();
		finish();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}
