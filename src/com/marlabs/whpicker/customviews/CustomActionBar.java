package com.marlabs.whpicker.customviews;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.utility.Util;

public class CustomActionBar {

	private ActionBar actionBar;
	private Context context; 
	private  String headerTitle;
	private LayoutParams params;
	private  TextView headerTitleText;
	final private int MAX_HEADER_LENGTH = 15;
	
	public  CustomActionBar(ActionBar actionBar, Context context, String headerTitle) {
		this.actionBar = actionBar;
		this.context = context;
		this.headerTitle = Util.ellipsize(headerTitle, MAX_HEADER_LENGTH);
		params = new ActionBar.LayoutParams(
			     ActionBar.LayoutParams.WRAP_CONTENT,
			     ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER );
	}

	
	@SuppressLint("InflateParams")
	public  void setCustomActionBarData(){
		LayoutInflater inflater = LayoutInflater.from(context);
		View customView = inflater.inflate(R.layout.action_bar, null);
		
		headerTitleText = (TextView) customView.findViewById(R.id.headerTitleText);
		headerTitleText.setText(headerTitle);
		
		actionBar.setCustomView(customView ,params);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
	}
	
	public  void changeHeader(String header){
		headerTitle =  Util.ellipsize(header, MAX_HEADER_LENGTH);;
		headerTitleText.setText(headerTitle);
	}
	
	public void setUpTitle(String upTitle){
		String title =  upTitle.length()>9 ? upTitle.substring(0, 8)+("...") : upTitle;
		actionBar.setTitle(title);
	}
}