package com.marlabs.whpicker.customviews;

import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;

import com.marlabs.whpickerqa.R;
import com.marlabs.whpicker.utility.AppConstants;

public class VoiceMessage implements OnInitListener {

	private TextToSpeech mTextToSpeech;
	private Context mContext;

	public VoiceMessage(Context context){
		mTextToSpeech = new TextToSpeech(context, this); 
		this.mContext = context;
	}

	@Override
	public void onInit(int status) {
		int result = mTextToSpeech.setLanguage(Locale.UK);
		mTextToSpeech.setPitch(1);
		// mTextToSpeech.setSpeechRate(5); 
		if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
			Log.e(AppConstants.TTS, AppConstants.LANGUAGE_UN_SUPPORTED);
		} 
		else {
			Log.e(AppConstants.TTS, AppConstants.TTS_FAILED);
		}		
	}   

	public void speak(String text) {
		SharedPreferences sharedPreferencesTTS = mContext.getSharedPreferences(AppConstants.TTS_PREFERENCE, 
				Context.MODE_PRIVATE);
		boolean isTTSEnabled = sharedPreferencesTTS.getBoolean(AppConstants.TTS_ENABLE, true);
		if(isTTSEnabled)
			mTextToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);		
	}

	public void destroySpeechEngine(){
		if (mTextToSpeech != null) {
			mTextToSpeech.stop();
			mTextToSpeech.shutdown();
		}
	}
}