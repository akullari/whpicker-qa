package com.marlabs.whpicker.customviews;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.marlabs.whpickerqa.R;

public class CustomButton extends RelativeLayout{
	private LayoutInflater mInflater;
	private View mView;
	private TextView mTextViewTitle, mTextView, mTextViewIndex;
	private ImageView imageview;
	private RelativeLayout mRelativeLayout;
	private int buttonTagValue, positionIndex;
	private boolean isButtonValidated;
	
	public CustomButton(Context context) {
		super(context);
		mInflater = LayoutInflater.from(context);
		init(); 
	}

	public CustomButton(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
		mInflater = LayoutInflater.from(context);
		init(); 
	}

	public CustomButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		mInflater = LayoutInflater.from(context);
		init(); 
	}

	public void init(){
		mView = mInflater.inflate(R.layout.validation_layout, this, true);
		mRelativeLayout = (RelativeLayout)mView.findViewById(R.id.relativelayout_validation_layout);
		mTextViewTitle = (TextView)mView.findViewById(R.id.verification_textview_title);
		mTextViewIndex = (TextView)mView.findViewById(R.id.verification_textview_index);
		mTextView = (TextView)mView.findViewById(R.id.verification_textview);
		imageview = (ImageView)mView.findViewById(R.id.verification_imageview);
	}

	public void setTitleAndText(String key, String value){
		mTextViewTitle.setText(key);
		mTextView.setText(value);
	}

	public void changebackGround(){
		mView.setBackgroundColor(getResources().getColor(R.color.red));
	}

	public void setAsVerified() {
		imageview.setVisibility(VISIBLE);
		imageview.setImageResource(R.drawable.verified);
		setDarkTextColor();
	}
	
	public void setDarkTextColor() {
		mTextView.setTextColor(getResources().getColor(R.color.textcolor));
		mTextViewTitle.setTextColor(getResources().getColor(R.color.textcolor));
		mTextViewIndex.setTextColor(getResources().getColor(R.color.textcolor));
	}

	public void setAsNotVerified() {
		imageview.setVisibility(VISIBLE);
		imageview.setImageResource(R.drawable.not_verified);
		mTextView.setTextColor(getResources().getColor(R.color.textcolor));
		mTextViewTitle.setTextColor(getResources().getColor(R.color.textcolor));
		mTextViewIndex.setTextColor(getResources().getColor(R.color.textcolor));
	}

	public void removeVerification() {
		imageview.setVisibility(INVISIBLE);
		mTextView.setTextColor(getResources().getColor(R.color.textcolor_disable));
		mTextViewTitle.setTextColor(getResources().getColor(R.color.textcolor_disable));
		mTextViewIndex.setTextColor(getResources().getColor(R.color.textcolor_disable));
	}

	public void removeVerificationForBlink() {
		imageview.setVisibility(INVISIBLE);
		mTextView.setTextColor(getResources().getColor(R.color.white));
		mTextViewTitle.setTextColor(getResources().getColor(R.color.white));
		mTextViewIndex.setTextColor(getResources().getColor(R.color.white));
	}
	
	public void hideVerification() {
		imageview.setVisibility(INVISIBLE);
	}

	public void setAsWrongVerified() {
		imageview.setVisibility(VISIBLE);
		imageview.setImageResource(R.drawable.wrong);
	}

	public String getTitle() {
		return mTextViewTitle.getText().toString();
	}
	
	public String getTextValue() {
		return mTextView.getText().toString();
	}

	public void setDarkGreyBackground(CustomButton customButton) {
		customButton.setBackgroundColor(this.getResources().getColor(R.color.grey_dark));
	}

	public void setLightGreyBackground(CustomButton customButton) {
		customButton.setBackgroundColor(this.getResources().getColor(R.color.grey_light));
	}

	public void setInnerPadding() {
		mRelativeLayout.setPadding(10, 15, 10, 15);
	}

	public void setLightBackgroundStyle(CustomButton customButton) {
		customButton.setPadding(15, 15, 15, 15);
		customButton.setBackgroundColor(Color.parseColor("#F1F1F2"));
	}
	public void setDarkBackgroundStyle(CustomButton customButton) {
		customButton.setPadding(15, 15, 15, 15);
		customButton.setBackgroundColor(Color.parseColor("#E6E7E8"));
	}

	public int getButtonTagValue() {
		return buttonTagValue;
	}

	public void setButtonTagValue(int buttonTagValue) {
		this.buttonTagValue = buttonTagValue;
	}
	
	public void setIndex(int index) {
		mTextViewIndex.setText("(" + index + ")");
	}
	
	public void removeIndex() {
		mTextViewIndex.setText("");
	}

	public int getPositionIndex() {
		return positionIndex;
	}

	public void setPositionIndex(int positionIndex) {
		this.positionIndex = positionIndex;
	}
	
	public boolean isButtonValidated() {
		return isButtonValidated;
	}

	public void setButtonValidation(boolean isValidated) {
		this.isButtonValidated = isValidated;
	}
}